
rPrint.RemovePrintersOnDisconnect = true  --whether to remove printers on player disconnect
rPrint.OwnershipTransferEnabled = true  --whether to allow users to take ownership of someone else's printer
rPrint.UpdateDelay = 2  --the time in between updates to the datatable (in seconds)
rPrint.MaxPrinters = 8  --maximum number of printers a user can own (including those that have had their owner changed)

rPrint.DefaultPrinterParams = {
	AutoAddToDarkRP = true,  --automatically add printers to DarkRP
	Price = 1000,  --price of the printer in DarkRP (only if automatically added)

	Max = 2, -- default printer max per-type

	Color = Color( 255, 255, 255, 255 ),  --color of the printer
	SoundEffectVolume = 50,  --sound effects volume (between 0 and 100)
	
	TempMin = 28,  --minimum temperature (won't go below this)
	TempMax = 80,  --maximum temperature (will explode or shut down at this temperature)
	TempStart = 30,  --starting temperature

	PrintRate = 4,  --printing rate (in money per second) ($/s)
	HeatRate = 1 / 15,  --heat rate (in degrees per money printed) (*/$)
	CoolRate = 1 / 10,  --cool rate (in degrees per second) (*/s)

	CoolerCost = 40,  --cooler cost
	CoolerCoolRate = 3 / 2,  --cooler cooling rate (in degrees per second) (*/s)
	CoolerBreakEnabled = true,  --should the cooler sometimes break
	CoolerBreakInterval = {  --the time interval between the coolers breaking (in seconds, randomized) (s)
		10 * 60,  --minimum amount of seconds between breaks
		20 * 60   --maximum amount of seconds between breaks
	},

	PowerConsumptionRate = 1 / 20, --power consumption rate (in percent power per money printed) (%/$)
	PowerConsumptionRateCooler = 1 / 12,  --power consumption rate for the cooler (in percent per second) (%/s)

	RechargeCost = 10,  --cost to recharge
	RechargeMax = 30,  --the value at which the recharge button becomes pressable

	FadeDistance = 125,  --maximum distance that the printers render the GUI
	UseDistance = 100,  --maximum distance a player can use the buttons

	AlertOwnerOnDestroyed = true,  --alert the owner when someone blows up their printer
	AlertOwnerOnOverheated = true,  --alert the owner when the printer overheats and blows up

	PrinterHealth = 100,  --printer health (resistance to damage)
	ExplodeOnOverheat = true,  --explode when the printer overheats
	ExplodeInWater = true,  --explode when submerged in water

	CanBeDestroyed = true,  --can the printers be broken
	DestroyPayout = 25,  --amount paid to the person who destroyed the printer
	DestroyPayoutTeamsExclusive = false,  --whether to exclude or include the teams below in the payout
	DestroyPayoutTeams = {  --teams which can receive money from destroying a printer; also supports team names as strings
		TEAM_POLICE,
		TEAM_CHIEF,
		TEAM_SWAT
	},

	Custom = { -- add custom properties here (like categories, stuff for your custom F4, etc.)
		-- category = "Printers", -- make sure you create the category first!
	}
}


--[[ Examples:

--Printer with all default settings
rPrint.RegisterPrinterType( "Default", {} )


--Printer with default settings, except it uses less power
rPrint.RegisterPrinterType( "Eco", {
	Price = 1500,

	PowerConsumptionRate = 1 / 35,
	PowerConsumptionRateCooler = 1 / 25
} )


--Special VIP printer, you'll want to change this depending on the teams/user groups that you want to have access
--In this example, it is available to users who are both TEAM_CITIZEN or TEAM_AOD, and of the user group "vip"
rPrint.RegisterPrinterType( "VIP", {
	Price = 5000,
	AllowedTeams = { TEAM_CITIZEN, TEAM_AOD }, --make it so only citizens and AODs can buy the printer
	AllowedUserGroups = { "vip" }, --lock to user group vip
	SpawnCommand = "/buyvipprinter", --custom spawn commands are also supported

	PrintRate = 10, --$6,000 per minute!

	HeatRate = 1 / 100, --heats up very slowly
	CoolRate = 1 / 2, --cool down pretty fast, you shouldn't even need a cooler for this one
	CoolerCoolRate = 5, --super cooler for the hell of it
	CoolerBreakEnabled = false, --cooler never breaks

	PowerConsumptionRate = 1 / 500, --very efficient
	PowerConsumptionRateCooler = 1 / 300, --cooler is also very efficient

	PrinterHealth = 1000, --super strong
	ExplodeOnOverheat = false, --don't blow up
	DestroyPayout = 5000 --Police jackpot!
}, "rprint_vipprinter" ) --give it the entity name "rprint_vipprinter" (this is optional, see README for default names)

]]


--[[rPrint.RegisterPrinterType( "Bronze", {
	Price = 1000,

	PrintRate = 4, 
	DestroyPayout = 250, 
	Color = Color( 205, 135, 65, 255 ), 
	RechargeCost = 200, 
	CoolerCost = 300
} )]]--

rPrint.RegisterPrinterType( "Emerald", {
	Price = 2000,
	Max = 4,

	PrintRate = 6,
	DestroyPayout = 600, 
	Color = Color( 50, 255, 50, 255 ), 
	Material = "models/shiny",
	RechargeCost = 300,
	CoolerCost = 200,
	PowerConsumptionRate = 1 / 30,
	PowerConsumptionRateCooler = 1 / 30,
} )

rPrint.RegisterPrinterType( "Ruby", {
	Price = 3000,

	PrintRate = 8,
	DestroyPayout = 1200, 
	Color = Color( 255, 20, 20, 255 ), 
	Material = "phoenix_storms/fender_white",
	RechargeCost = 500, 
	CoolerCost = 500,
	PowerConsumptionRate = 1 / 40,
	PowerConsumptionRateCooler = 1 / 30,
} )

rPrint.RegisterPrinterType( "Platinum", {
	Price = 5000,

	PrintRate = 9,
	DestroyPayout = 1500, 
	Color = Color( 255, 50, 50, 255 ), 
	Material = "debug/env_cubemap_model",
	RechargeCost = 500, 
	CoolerCost = 500,
	PowerConsumptionRate = 1 / 40,
	PowerConsumptionRateCooler = 1 / 30,
} )

rPrint.RegisterPrinterType( "Crystal", {
	Price = 5000,

	PrintRate = 11,
	DestroyPayout = 2000, 
	Material = "models/props_lab/xencrystal_sheet",
	RechargeCost = 500, 
	CoolerCost = 500,
	PowerConsumptionRate = 1 / 70,
	PowerConsumptionRateCooler = 1 / 40,
	customCheck = function(ply)
		if (kShop.PlayerHasUpgrade(ply, "darkrp-printer_1")) then
			return true
		end

		return false
	end,
	CustomCheckFailMsg = "You must purchase this printer in the !shop"
} )

rPrint.RegisterPrinterType( "Unknown", {
	Price = 5000,

	PrintRate = 14,
	DestroyPayout = 3000, 
	Material = "models/xqm/lightlinesred_tool",
	RechargeCost = 500, 
	CoolerCost = 550,
	PowerConsumptionRate = 1 / 80,
	PowerConsumptionRateCooler = 1 / 50,
	customCheck = function(ply)
		if (kShop.PlayerHasUpgrade(ply, "darkrp-printer_2")) then
			return true
		end

		return false
	end,
	CustomCheckFailMsg = "You must purchase this printer in the !shop"
} )

rPrint.RegisterPrinterType( "Nuclear", {
	Price = 5000,

	PrintRate = 17,
	DestroyPayout = 5000,
	Material = "phoenix_storms/fender_white",
	Rainbow = true,
	RechargeCost = 550, 
	CoolerCost = 600,
	PowerConsumptionRate = 1 / 80,
	PowerConsumptionRateCooler = 1 / 50,
	customCheck = function(ply)
		if (kShop.PlayerHasUpgrade(ply, "darkrp-printer_3")) then
			return true
		end

		return false
	end,
	CustomCheckFailMsg = "You must purchase this printer in the !shop"
} )
