Inventory = {}

if SERVER then
    include('inventory/sv_mysql.lua')
    include('inventory/sv_inventory.lua')
    include('inventory/sv_player.lua')

    AddCSLuaFile()
    AddCSLuaFile('inventory/vgui/window.lua')
    AddCSLuaFile('inventory/cl_vgui.lua')
else
    include('inventory/vgui/window.lua')
    include('inventory/cl_vgui.lua')
end