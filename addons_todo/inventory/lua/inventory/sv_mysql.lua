require('mysqloo')

if !mysqloo then
	Error('Unknown-Inventory could load load (mysqloo missing!)');
	return;
end

Inventory.DB = Inventory.DB or {};

Inventory.Config.MySQL = {
	host = '54.37.242.128',
	username = 'u20314_5TLZASuBun',
	password = 'tA1u1zcu1ScFn2d4',
	database = 's20314_darkrp',
	port = 3306
}

function Inventory.DB.Connect()
	local config = Inventory.Config.MySQL;
	
	local db = mysqloo.connect(config.host, config.username, config.password, config.database, config.port);
	
	db.onConnectionFailed = function(msg, err)
		Error("Inventory | MySQL connection failed: " .. tostring(err));
	end
	
	db.onConnected = function()
		print("Inventory | MySQL connection established at " .. os.date());
		
		Inventory.DB.DBHandle = db;
		
		db.onConnected = function() D3A.Print("MySQL connection re-established at " .. os.date()); end
		
		Inventory.DB.Query([[
			CREATE TABLE IF NOT EXISTS inventory_players (
				steamid VARCHAR(55) PRIMARY KEY NOT NULL,
				size INT NOT NULL,
				contents JSON NOT NULL
			)
		]])
	end
	
	db:connect();
	db:wait();
	
	Inventory.DB.DBHandle = db;
end

function Inventory.DB.Disconnect()
	if (Inventory.DB.DBHandle) then
		Inventory.DB.DBHandle = nil;
	end
end

function Inventory.DB.Escape(txt)
	txt = tostring(txt or "");

	return Inventory.DB.DBHandle:escape(txt);
end

function Inventory.DB.Query(query, callback, ret)
	if (!query) then
		print("Inventory | No query given.");
		return;
	end
	
	local db = Inventory.DB.DBHandle;
	local q = db:query(query);
	local d, r;
	
	q.onData = function(self, dat)
		d = d or {};
		table.insert(d, dat);
	end
	
	q.onSuccess = function()
		if (callback) then r = callback(d); end
	end
	
	q.onError = function(q, err, query)
		if (db:status() == mysqloo.DATABASE_NOT_CONNECTED) then
			print("Inventory | MySQL connection lost during query. Reconnecting.");
			
			db:connect();
			db:wait();
			
			r = Inventory.DB.Query(query, callback, ret);
		else
			Error("Inventory | MySQL error: " ..err);
			print(" | Query: " .. query);
		end
	end
	
	q:start();
	
	if (ret) then q:wait(); end
	
	return r;
end

function Inventory.DB.QueryRet(query, callback)
	callback = callback or function(data) return data; end;
	
	return Inventory.DB.Query(query, callback, true);
end

function Inventory.DB.QueryObject(query)
	if (!query) then
		print("Inventory | No query given.");
		return;
	end
	
	local db = Inventory.DB.DBHandle;
	local q = db:query(query);
	
	return q;
end

hook.Add("Initialize", "Inventory.DB.Connect", Inventory.DB.Connect);