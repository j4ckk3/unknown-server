local PANEL = {};

function PANEL:Init()
	self.Size = {x = math.Clamp(ScrW() * 0.5, 632, math.huge), y = math.Clamp(ScrH() * 0.5, 472, math.huge)};
	
	self:SetSize(self.Size.x, self.Size.y);
	
	self._StartTime = SysTime();
	
	self:SetVisible(true);
	self:MakePopup();
	
	self:SetMovement(-self.Size.x, (ScrW() - self.Size.x) / 2, (ScrH() - self.Size.y) / 2, (ScrH() - self.Size.y) / 2);
end

function PANEL:ApplySchemeSettings()
end

function PANEL:PerformLayout()
	local w = self:GetWide();
	local h = self:GetTall();
	
	local half = (w - 15) / 2;
end

function PANEL:SetPos(x, y, force, time)
	if (force) then self.BaseClass.SetPos(self, x, y); return; end

	self:SetMovement(self.x, x, self.y, y, time or nil);
	self._StartTime = SysTime();
	self._Movement = 1;
end

function PANEL:SetMovement(startX, endX, startY, endY, timeOverride)
	self.StartPos = {x = startX, y = startY};
	self.EndPos = {x = endX, y = (endY or startY)};
	
	self._AnimTime = timeOverride or nil;
end

function PANEL:SetVisible(bool)
	if (bool) then
		if (!self.StartPos) then
			self:SetMovement(-self:GetWide(), 0, (ScrH() / 2) - (self:GetTall() / 2));
		end
		
		self:SetPos(self.StartPos.x, self.StartPos.y, true);
		self._StartTime = SysTime();
		self._Movement = 1;
		
		self.BaseClass.SetVisible(self, bool);
	else
		self:Close();
	end
end

function PANEL:Close()
	self:SetMovement(self.x, -self:GetWide(), self.y, self.y);
	self._StartTime = SysTime();
	self._Movement = -1;
	
	self:SetMouseInputEnabled(false);
	
	timer.Simple(3, function() if (self:IsValid()) then self:Remove(); end end);
end

function PANEL:Think()
	if (!self._NoMoves) and (self._Movement != 0) then
		local mul, clamp;
		
		if (self._Movement == 1) then
			clamp = math.Clamp((SysTime() - self._StartTime) / (self._AnimTime or .5), 0, 1);
			mul = math.sin(clamp * (math.pi / 1.418776)) * 1.25;
		else
			clamp = math.Clamp((SysTime() - self._StartTime) / (self._AnimTime or .3), 0, 1);
			mul = math.sin((1 - clamp) * (math.pi / 1.418776)) * 1.25;
		end
		
		local x, y;
	
		if (self._Movement == 1) then
			x = self.StartPos.x + (mul * (self.EndPos.x - self.StartPos.x));
			y = self.StartPos.y + (mul * (self.EndPos.y - self.StartPos.y));
		else
			x = self.StartPos.x - (mul * (self.EndPos.x - self.StartPos.x)) + (self.EndPos.x - self.StartPos.x);
			y = self.StartPos.y - (mul * (self.EndPos.y - self.StartPos.y)) + (self.EndPos.y - self.StartPos.y);
		end
		
		self:SetPos(x, y, true);

		if (clamp == 1) then
			self._AnimTime = nil;
			
			if (self._Movement == -1) then
				self:Remove();
			else
				self._Movement = 0;
			end
		end
	end
	
	-- (modified) dframe think
	local mousex = math.Clamp( gui.MouseX(), 1, ScrW()-1 );
	local mousey = math.Clamp( gui.MouseY(), 1, ScrH()-1 );
		
	if ( self.Dragging ) then
		local x = mousex - self.Dragging[1];
		local y = mousey - self.Dragging[2];

		-- Lock to screen bounds if screenlock is enabled
		if ( self:GetScreenLock() ) then
			x = math.Clamp( x, 0, ScrW() - self:GetWide() );
			y = math.Clamp( y, 0, ScrH() - self:GetTall() );
		end
		
		if (!self.EndPos or self.EndPos.x != x or self.EndPos.y != y) then	
			self:SetPos(x, y);
		end
	end
	
	self:SetCursor("arrow");
end

function PANEL:Paint(w, h)
	surface.SetDrawColor(0, 0, 0, 240);
	surface.DrawRect(0, 0, w, h);
end

vgui.Register("Inventory.Window", PANEL, "DFrame");