D3A.Player = {}

function D3A.Player.CanTarget(pl1, pl2)
	if pl1:IsSuperAdmin() and pl2:IsBot() then return true end
	return D3A.Ranks.CheckWeight(pl1:GetUserGroup(), pl2:GetUserGroup())
end

function D3A.Player.CheckPassword(SteamID, IP, sv_Pass, cl_Pass, Name)
	SteamID = util.SteamIDFrom64(SteamID)

	-- Check if banned
	D3A.Bans.IsBanned(SteamID, function(isbanned, data)
		if isbanned then
			local exp
			if (tonumber(data.Current.Length) == 0) then
				exp = "permanently"
			else
				exp = math.Round(((data.Current.Time + data.Current.Length) - os.time())/60, 2) .. " minutes"
			end
			game.KickID(SteamID, "You are banned!\n\nLength: " .. exp .. "\nReason: " .. data.Current.Reason .. "\nBy: " .. data.Current.A_SteamID .. "\n\nYou can appeal on our forums: www.hyplex.co")
		end
	end)

	-- Check pass
	if (sv_Pass != "") and (cl_Pass != sv_Pass) then
		return false, "Invalid password: " .. cl_Pass
	end

	-- Create data
	D3A.MySQL.Query("CALL createUserInfo('" .. SteamID .. "', '" .. D3A.MySQL.Escape(Name) .. "', '" .. IP .. "', '" .. os.time() .. "');", function(d)
		if (tonumber(d[1].Created) == 1) then
			D3A.Print(SteamID .. " | Connecting for the first time")
		else
			D3A.Print(SteamID .. " | Connecting")
		end
	end)	
	
	return true
end
hook.Add("CheckPassword", "D3A.Player.CheckPassword", D3A.Player.CheckPassword)

function D3A.Player.PlayerAuthed(pl)
	pl:LoadInfo(function(data)
		if (!data) then return end
		local str = pl:SteamName() .. " (" .. pl:SteamID() .. ") has connected"
		if (tonumber(data.LastSeen) == -1) then str = str .. " for the first time" end
		if (data.SteamName != pl:SteamName()) then str = str .. ". Last known as " .. data.SteamName end
		str = str .. "."
		
		D3A.Chat.Broadcast(str, "CONDISCON")
		
		pl:SaveInfo()
	end)
end
hook.Add("PlayerAuthed", "D3A.Player.PlayerAuthed", D3A.Player.PlayerAuthed)

function D3A.Player.PlayerDisconnected(pl)
	local str = pl:SteamName() .. " (" .. pl:SteamID() .. ") has disconnected."
	
	D3A.Chat.Broadcast(str, "CONDISCON")
end
hook.Add("PlayerDisconnected", "D3A.Player.PlayerDisconnected", D3A.Player.PlayerDisconnected)

function D3A.Player.PhysgunPickup(pl, ent)
	if ent:IsPlayer() and pl:HasAccess(D3A.Config.PlayerPhysgun) and pl:GetDataVar("adminmode") and D3A.Player.CanTarget(pl, ent) then
		ent.physAdmin = pl
		ent:Freeze(true)
		ent:SetMoveType(MOVETYPE_NOCLIP)
		return true
	end
end
hook.Add("PhysgunPickup", "D3A.Player.PhysgunPickup", D3A.Player.PhysgunPickup)

function D3A.Player.PhysgunDrop(pl, ent)
	if ent:IsPlayer() and pl:GetDataVar("adminmode") then
		if pl:KeyPressed(IN_ATTACK2) then return end

		ent.physAdmin = nil
		ent:DropToFloor()
		ent:Freeze(false)
		ent:SetMoveType(MOVETYPE_WALK)
	end
end
hook.Add("PhysgunDrop", "D3A.Player.PhysgunDrop", D3A.Player.PhysgunDrop)

-- Noclip cloak system
-- jackk's changes here (TODO:hasn't been tested yet)
noclipped = noclipped or {}
local hpgodded = {}
local function invis(target, bool)
	if !(bool) then
		target:SetNoDraw(false);
		target:SetNotSolid(false);
		-- changes start
		if (hpgodded[target]) then
			hpgodded[target] = nil
			target:GodEnable()
		else
			target:GodDisable()
		end
		-- changes end
		target:DrawWorldModel(true);
	else
		target:SetNoDraw(true);
		target:SetNotSolid(true);
		-- changes start
		if (target:HasGodMode()) then
			hpgodded[target] = true
		end
		-- changes end
		target:GodEnable();
		target:DrawWorldModel(false);
	end;
end
timer.Create("noclip.cloak", 1, 0, function()
	for k, v in pairs(noclipped) do
		if !IsValid(k) then noclipped[k] = nil return end
		k:GetChildren()[1]:SetNoDraw( true )
		invis(k, true)
	end
end)
-- Noclip cloak system

function D3A.Player.PlayerNoClip(pl, state)
	if pl:HasAccess(D3A.Config.PlayerNoClip) then
		if !pl:GetDataVar("adminmode") then
			D3A.Chat.SendToPlayer(pl, "You need to enable adminmode to noclip!", "ERR")
			return false
		end
	else
		return false
	end

	if state then
		noclipped[pl] = true
	else
		noclipped[pl] = nil
		invis(pl, false)
		pl:GetChildren()[1]:SetNoDraw( false )
	end

	return true
end
hook.Add("PlayerNoClip", "D3A.Player.PlayerNoClip", D3A.Player.PlayerNoClip)
