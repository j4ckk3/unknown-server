D3A.Bans = {}

local function parseBans(data)
	local data = data or {}
	
	local Bans = {
		Current = nil,
		Past = {},
		Removed = {}
	}
	
	for k, v in ipairs(data) do
		if (v.UnbanReason) then
			table.insert(Bans.Removed, v)
			continue
		end

		if (tonumber(v.Time) + tonumber(v.Length) <= os.time()) and (tonumber(v.Length) != 0) then
			table.insert(Bans.Past, v)
			continue
		else
			Bans.Current = v
		end
	end
	
	return Bans
end

function D3A.Bans.GetBans(steamid, cb)
	local q = "SELECT * FROM player_bans WHERE SteamID='" .. D3A.MySQL.Escape(steamid) .. "'"
	
	local Bans = {}
	
	if (type(cb) == "function") then
		D3A.MySQL.Query(q, function(data)
			Bans = parseBans(data)
			
			cb(Bans)
		end)
	else
		local data = D3A.MySQL.QueryRet(q)
		
		Bans = parseBans(data)
		
		return Bans
	end
end

function D3A.Bans.IsBanned(steamid, callback)
	if steamid == D3A.Config.owner then
		callback(false)
		return
	end

	D3A.Bans.GetBans(steamid, function(data)
		callback(data.Current ~= nil, data)
	end)
end

function D3A.Bans.BanPlayer(steamid, a_steamid, len, unit, reason, override)
	if steamid == D3A.Config.owner then return end
	local units = {}
		units["perm"] = 0
		units["second"] = 1
		units["minute"] = 60
		units["hour"] = 3600
		units["day"] = 86400
		units["week"] = 604800
		units["month"] = 2419200
		units["year"] = 29030400
	
	local banlen = len * units[unit]
	
	local ret
	
	if (!override) then
		ret = D3A.MySQL.QueryRet("INSERT INTO player_bans (`Time`, `SteamID`, `A_SteamID`, `Length`, `Reason`) VALUES('" .. os.time() .. "', '" .. steamid .. "', '" .. a_steamid .. "', '" .. banlen .. "', '" .. D3A.MySQL.Escape(reason) .. "')", function()
			local pl = D3A.FindPlayer(a_steamid)
			local tg = D3A.FindPlayer(steamid)
			local nm = (pl and pl:Name()) or "Console"
			
			if (tg) then
				local exp
				
				if (type(len) == "boolean") then
					len = (len and tonumber(1)) or tonumber(0); -- idk why boolean is being implied
				end
				
				if (unit == "perm") then exp = "permanently"
				else exp = len .. " " .. unit .. (((len != 1) and "s") or "") end
				tg:Kick("You are banned!\n\nLength: " .. exp .. "\nReason: " .. reason .. "\nBy: " .. a_steamid .. "\n\nYou can appeal on our forums: www.hyplex.co")
			end
		
			return true
		end)
	else
		ret = D3A.MySQL.QueryRet("UPDATE player_bans SET Time='" .. os.time() .. "', A_SteamID='" .. a_steamid .. "', Length='" .. banlen .. "', Reason='" .. D3A.MySQL.Escape(reason) .. "' WHERE Time='" .. override .. "' AND SteamID='" .. steamid .. "'", function()
			return true
		end)
	end
	
	return ret
end

function D3A.Bans.Unban(sid, reason, bantime)
	local ret = D3A.MySQL.QueryRet("UPDATE player_bans SET UnbanReason='" .. D3A.MySQL.Escape(reason) .. "' WHERE Time='" .. bantime .. "' AND SteamID='" .. sid .. "'", function()
		return true
	end)
	
	return ret
end