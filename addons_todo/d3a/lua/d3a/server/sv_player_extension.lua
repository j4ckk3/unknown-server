local meta = FindMetaTable("Player")

function meta:LoadInfo(callback)
	D3A.MySQL.Query("CALL selectUserInfo('" .. self:SteamID() .. "');", function(d)
		if (!self:IsValid()) then return end
		
		local data = d[1]
		
		data.Vars = data.Vars or "[]";
		
		local newVars = util.JSONToTable(data.Vars);
		
		data.Vars = newVars
		local checkToUpdate = table.Count(data.Vars)
		
		self._Vars = {}
		self._PersistVars = {}
		self._NetVars = {}
		
		hook.Call("PlayerDataLoaded", GAMEMODE, self, data)
		
		self._PersistVars = data.Vars
		for k, v in pairs(data.Vars) do
			if D3A.NW.IsRegistered(k) then
				self:SetDataVar(k, v)
			end
			self._Vars[k] = v
		end

		if (table.Count(data.Vars) != checkToUpdate) then -- Some script added a default value
			self:SaveVars()
		end
		
		hook.Call("PostPlayerDataLoaded", GAMEMODE, self, data);
		
		if callback then callback(data) end
	end)
end

function meta:SaveInfo()
	D3A.MySQL.Query("CALL updateUserInfo('" .. self:SteamID() .. "', '" .. D3A.MySQL.Escape(self:SteamName()) .. "', '" .. self:IPAddress() .. "', '" .. os.time() .. "');")
end

function meta:SaveVars() -- The system will call this, you don't need to
	if (!self:IsValid()) then return end
	
	local t = table.Copy(self._PersistVars or {})
	local s = util.TableToJSON(t);
	
	local q = "UPDATE player set Vars=\"" .. D3A.MySQL.Escape(s) .. "\" where SteamID='" .. self:SteamID() .. "';"
	timer.Create("save_" .. self:UniqueID(), 1, 1, function()
		D3A.MySQL.Query(q)
	end)
end

function meta:SetDataVar(name, val, persist, network)
	if (!self._Vars) then
		D3A.Print("Queueing SetDataVar on " .. self:SteamID() .. " : " .. name)
		local pl = self
		hook.Add("PostPlayerDataLoaded", "VarQueue." .. self:SteamID() .. "." .. name, function()
			timer.Simple(0, function() 
				if IsValid(pl) then
					pl:SetDataVar(name, val, persist, network) 
				end
			end)
			hook.Remove("PostPlayerDataLoaded", "VarQueue." .. self:SteamID() .. "." .. name)
		end)
		
		return
	end

	self._Vars[name] = val
	
	if (persist) then
		self._PersistVars[name] = val
		
		self:SaveVars()
	end
	
	if (network) then
		self._NetVars[name] = val
		self:SetNetVar(name, val)
	end
end

meta.SteamName = meta.SteamName or meta.Name