COMMAND.Name = "SetArmor"
COMMAND.Flag = D3A.Config.Commands.SetArmor

COMMAND.Args = {{"player", "Name/SteamID"}, {"number", "Armor"}}

COMMAND.Run = function(pl, args, supp)
	supp[1]:SetArmor(args[2])
	
	D3A.Chat.SendToPlayer(pl, "Set " .. supp[1]:NameID() .. "'s armor to " .. args[2] .. ".")
	D3A.Chat.SendToPlayer(supp[1], pl:NameID() .. " set your armor to " .. args[2] .. ".")
end