COMMAND.Name = "MuteVoice"

COMMAND.Flag = D3A.Config.Commands.MuteVoice
COMMAND.AdminMode = true
COMMAND.CheckRankWeight = true

COMMAND.Args = {{"player", "Name/SteamID"}}

COMMAND.Run = function(pl, args, supp)
	supp[1].VoiceMuted = (not supp[1].VoiceMuted)

	D3A.Chat.Broadcast(pl:Name() .. " has " .. ((supp[1].VoiceMuted) and "muted " or "unmuted ") .. supp[1]:Name() .. "'s voice")
end

hook.Add("PlayerCanHearPlayersVoice", "D3A.MuteVoice.PlayerCanHearPlayersVoice", function(listener, talker)
	if talker.VoiceMuted then 
		return false
	end
end)