COMMAND.Name = "Respawn"
COMMAND.Flag = D3A.Config.Commands.Respawn

COMMAND.Args = {{"player", "Name/SteamID"}}

COMMAND.Run = function(pl, args, supp)
	supp[1]:Spawn()
	
	D3A.Chat.Broadcast(pl:NameID() .. " has respawned " .. supp[1]:NameID() .. ".")
end