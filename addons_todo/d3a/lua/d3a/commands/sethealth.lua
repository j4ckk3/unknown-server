COMMAND.Name = "SetHealth"
COMMAND.Flag = D3A.Config.Commands.SetHealth

COMMAND.Args = {{"player", "Name/SteamID"}, {"number", "Health"}}

COMMAND.Run = function(pl, args, supp)
	supp[1]:SetHealth(args[2])
	
	D3A.Chat.SendToPlayer(pl, "Set " .. supp[1]:NameID() .. "'s health to " .. args[2] .. ".")
	D3A.Chat.SendToPlayer(supp[1], pl:NameID() .. " set your health to " .. args[2] .. ".")
end