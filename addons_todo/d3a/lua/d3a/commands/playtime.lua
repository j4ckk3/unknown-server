COMMAND.Name = "Playtime"
COMMAND.Flag = D3A.Config.Commands.PlayTime
COMMAND.AdminMode = false

COMMAND.Args = {{"player", "Name/SteamID"}}

COMMAND.Run = function(pl, args, supp)
	D3A.Chat.SendToPlayer(pl, supp[1]:Name() .. " has played for " .. D3A.Time.FormatTime(supp[1], true) .. ".")
end