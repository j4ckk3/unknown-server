COMMAND.Name = "Unjail"
COMMAND.Flag = D3A.Config.Commands.Unjail
COMMAND.AdminMode = true

COMMAND.Args = {{"player", "Name/SteamID"}}

COMMAND.Run = function(pl, args, supp)
	local targ = supp[1]

	if (!targ.Jail) then
		D3A.Chat.SendToPlayer(pl, targ:Name() .. " is not jailed!", "ERR")
		return
	end

	targ:SendLua("D3A.CloseMoTD()")

	local sid = targ:SteamID64()
	timer.Remove("JailWatch" .. sid)
	timer.Remove("AutoUnjail" .. sid)

	for k, v in ipairs(targ.JailProps) do
		if (IsValid(v)) then v:SetCanRemove(true) v:Remove() end
	end

	targ.Jail = nil
	targ.JailPos = nil
	targ.JailProps = nil

	D3A.Chat.Broadcast(pl:Name() .. " (" .. pl:SteamID() .. ") has unjailed " .. targ:Name() .. " (" .. targ:SteamID() .. ").")
end