COMMAND.Name = "JailTP"
COMMAND.Flag = D3A.Config.Commands.JailTP
COMMAND.AdminMode = true

COMMAND.Args = {{"player", "Name/SteamID"}, {"number", "Duration (minutes)"}, {"string", "Reason"}}

COMMAND.Run = function(pl, args, supp)
	supp[1].JailTPPos = D3A.FindEmptyPos(pl:GetEyeTrace().HitPos, {pl}, 600, 30, Vector(16, 16, 64))
	
	D3A.Commands.Stored["jail"].Run(pl, args, supp)
end