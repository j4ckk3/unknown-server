COMMAND.Name = "ClearDecals"
COMMAND.Flag = D3A.Config.Commands.ClearDecals

COMMAND.Run = function(pl, args, supp)
	BroadcastLua("LocalPlayer():ConCommand('r_cleardecals')")
	D3A.Chat.Broadcast(pl:NameID() .. " has cleared decals.")
end