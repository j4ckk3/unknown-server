COMMAND.Name = "UnBan"

COMMAND.Flag = D3A.Config.Commands.Unban
COMMAND.AdminMode = true

COMMAND.Args = {{"string", "SteamID"}, {"string", "Reason"}}

COMMAND.Run = function(pl, args, supp)
	local sid = tostring(args[1]):upper()
	
	if (string.sub(sid, 1, 8) != "STEAM_0:") then
		D3A.Chat.SendToPlayer(pl, "Please input a SteamID!", "ERR")
		return
	end
	
	D3A.Bans.GetBans(sid, function(Bans)
		if (!Bans.Current) then
			D3A.Chat.SendToPlayer(pl, sid .. " is not banned!", "ERR")
		else
			local reason = table.concat(args, " ", 2)
			if (D3A.Bans.Unban(sid, reason, Bans.Current.Time)) then
				D3A.Chat.Broadcast(((pl:IsValid() and pl:Name()) or "Console") .. " has unbanned " .. sid .. ". Reason: " .. reason)
			else
				D3A.Chat.SendToPlayer(pl, "An unexpected error has occurred while unbanning. Tell the boss!", "ERR")
			end
		end
	end)
end