COMMAND.Name = "Ban"

COMMAND.Flag = D3A.Config.Commands.Ban
COMMAND.AdminMode = true
COMMAND.CheckRankWeight = true

COMMAND.Args = {{"string", "Name/SteamID"}, {"number", "length"}, {"string", "TimeUnit"}, {"string", "Reason"}}

COMMAND.Run = function(pl, args, supplement)
	local units = {}
		units["seconds"] = "second"
		units["minutes"] = "minute"
		units["hours"] = "hour"
		units["days"] = "day"
		units["weeks"] = "week"
		units["months"] = "month"
		units["years"] = "year"
		
	local unit = args[3]:lower()
	if (units[unit]) then unit = units[unit]
	elseif (!table.HasValue(units, unit)) then
		D3A.Chat.SendToPlayer(pl, "Invalid unit of time! Please use seconds/minutes/hours/days/weeks/months/years!", "ERR")
		return
	end
		
	local plname = (pl:IsValid() and pl:Name()) or "Console"
	local plstid = (pl:IsValid() and pl:SteamID()) or "CONSOLE"
	
	local targ = args[1]:upper()
	local time = supplement[1]
	
	local targpl = D3A.FindPlayer(targ)
	
	local targstid

	if (time == 0) then
		D3A.Commands.Parse(pl, "perma", {args[1], table.concat(args, " ", 4)})
		return
	end

	if (!targpl and string.sub(targ, 1, 8) != "STEAM_0:") then
		D3A.Chat.SendToPlayer(pl, "Unknown player: " .. targ, "ERR")
		return
	elseif (!targpl) then
		targstid = targ
	else
		targstid = targpl:SteamID()
		
		if (!D3A.Ranks.CheckWeight(pl, targpl)) then
			D3A.Chat.SendToPlayer(pl, "Player's rank is equal or greater weight than yours!", "ERR")
			D3A.Chat.SendToPlayer(targpl, plname .. " (" .. plstid .. ") attempted to use Ban on you.", "ERR")
			return false
		end
	end
	
	if (pl:GetUserGroup() == "trialmod") then
		if (unit == "month" || unit == "year") then return false end
		if (time > 1 && unit == "week") then return false end
		if (time > 7 && unit == "days") then return false end
		if (time > 168 && unit == "hours") then return false end
		if (time > 10080 && unit == "minutes") then return false end
		if (time > 604800 && unit == "seconds") then return false end 
	end

	D3A.Bans.GetBans(targstid, function(Bans)
		if (Bans.Current) then
			if (!pl:HasAccess(D3A.Config.Commands.UpdateBan)) then
				D3A.Chat.SendToPlayer(pl, targstid .. " is already banned ('" .. D3A.Config.Commands.UpdateBan .. "' access required to update a ban)", "ERR")
				return
			end
			
			local useunit = (time != 1 and unit .. "s") or unit
			local reason = table.concat(args, " ", 4)
			
			if D3A.Bans.BanPlayer(targstid, plstid, time, unit, reason, Bans.Current.Time) then
				D3A.Chat.Broadcast(targstid .. "'s ban was updated by " .. plname .. " to " .. time .. " " .. useunit .. ". Reason: " .. reason .. ".")
			else
				D3A.Chat.SendToPlayer(pl, "An unexpected error has occurred while banning. Tell the boss!", "ERR")
			end
		else
			local useunit = (time != 1 and unit .. "s") or unit
			local reason = table.concat(args, " ", 4)
		
			if D3A.Bans.BanPlayer(targstid, plstid, time, unit, reason) then
				D3A.Chat.Broadcast(((targpl and targpl:Name()) or targstid) .. " was banned by " .. plname .. " for " .. time .. " " .. useunit .. ". Reason: " .. reason .. ".")
			else
				D3A.Chat.SendToPlayer(pl, "An unexpected error has occurred while banning. Tell the boss!", "ERR")
			end
		end
	end)
end