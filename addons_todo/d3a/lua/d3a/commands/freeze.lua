COMMAND.Name = "Freeze"

COMMAND.Flag = D3A.Config.Commands.Freeze
COMMAND.AdminMode = true
COMMAND.CheckRankWeight = true

COMMAND.Args = {{"player", "Name/SteamID"}}

COMMAND.Run = function(pl, args, supp)
	local targ = supp[1]
	
	if not targ:IsFrozen() then
	targ:Freeze(true)
	targ:SetMoveType(MOVETYPE_NOCLIP)
	
	D3A.Chat.SendToPlayer(pl, "Froze " .. targ:Name() .. ".")
	D3A.Chat.SendToPlayer(targ, pl:Name() .. " has frozen you.")
	else
	targ:Freeze(false)
	targ:SetMoveType(MOVETYPE_WALK)
	
	D3A.Chat.SendToPlayer(pl, "Unfroze " .. targ:Name() .. ".")
	D3A.Chat.SendToPlayer(targ, pl:Name() .. " has unfrozen you.")
	end
end