COMMAND.Name = "PO"

COMMAND.Flag = D3A.Config.Commands.PO
COMMAND.AdminMode = true

COMMAND.Args = {{"string", "SteamID"}}

COMMAND.Run = function(pl, args, supp)
	local sid = tostring(args[1]):upper()
	
	if (string.sub(sid, 1, 8) != "STEAM_0:") then
		D3A.Chat.SendToPlayer(pl, "Please input a SteamID!", "ERR")
		return
	end
	
	D3A.Bans.GetBans(sid, function(Bans)
			if (!Bans.Past[1]) then 
			D3A.Chat.SendToPlayer(pl, "This user has no previous (Non Current) bans", "ERR") 
			else
			D3A.Chat.SendToPlayer(pl, "Check Console", "ERR") 
				for k,v in pairs(Bans.Past) do
				pl:PrintMessage(HUD_PRINTCONSOLE, " ");
					for newk,newv in pairs(v) do
						pl:PrintMessage(HUD_PRINTCONSOLE, newk .. ": " .. newv);
					end
				end
			end
	end)
end