COMMAND.Name = "Reconnect"
COMMAND.Flag = D3A.Config.Commands.Reconnect

COMMAND.Args = {{"player", "Name/SteamID"}}

COMMAND.Run = function(pl, args, supp)
	supp[1]:SendLua("LocalPlayer():ConCommand('retry')")
	
	D3A.Chat.Broadcast(pl:NameID() .. " has reconnected " .. supp[1]:NameID() .. ".")
end