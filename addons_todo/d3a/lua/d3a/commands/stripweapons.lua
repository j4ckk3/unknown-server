COMMAND.Name = "StripWeapons"
COMMAND.Flag = D3A.Config.Commands.StripWeps

COMMAND.Args = {{"player", "Name/SteamID"}}

COMMAND.Run = function(pl, args, supp)
	supp[1]:StripWeapons()
	supp[1]:StripAmmo()
	
	D3A.Chat.Broadcast(pl:NameID() .. " has stripped " .. supp[1]:NameID() .. "'s weapons.")
end