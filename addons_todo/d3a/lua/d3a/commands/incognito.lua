COMMAND.Name = "Incognito"
COMMAND.Flag = D3A.Config.Commands.Incognito

local fakenames = {
	{"John",
	"Dion",
	"Lane",
	"Francisco",
	"Eloy",
	"Gene",
	"Elon",
	"Marcus",
	"Markus",
	"Homer",
	"Russel",
	"Lino",
	"Boris",
	"Rodolfo",
	"Mohamed",
	"Tanner",
	"Quentin",
	"Milo",
	"William",
	"Davis",
	"Jonas"}, -- 20

	{"Porta",
	"Lineberger",
	"Legrand",
	"Jarvie",
	"Marguez",
	"Hirst",
	"Schlater",
	"Templeman",
	"Weems",
	"Marriner",
	"Span",
	"Pita",
	"Bodden",
	"Paniagua",
	"Dunbar",
	"Reitz",
	"Ward",
	"Mcauliffe",
	"Dubin",
	"Kirwin",
	"Felipe"}, -- 20
}

COMMAND.Run = function(pl, args, supp)
	if args then
		if args[1] == "regen" then
			pl.incognito = nil
		elseif args[1] == "streammode" then
			pl.incognito = true
			for k, v in pairs(player.GetAll()) do
				v:SendLua("local ply = player.GetAll()[" .. pl:EntIndex() .. "] ply.incognito = true ply.fakename = '" .. fakenames[1][math.random(1, #fakenames[1])] .. " ".. fakenames[2][math.random(1, #fakenames[2])] .. "'")
			end
			aprint(nil, "Incognito | ", pl, "Your name is now different for every player.")
			return
		end
	end

	if pl.incognito then
		pl.incognito = nil
		BroadcastLua("local ply = player.GetAll()[" .. pl:EntIndex() .. "] ply.incognito = nil ply.fakename = nil")
		aprint(nil, "Incognito | ", pl, "Your name is restored back to " .. pl:Nick())
		return
	end

	local fname = fakenames[1][math.random(1, #fakenames[1])] .. " ".. fakenames[2][math.random(1, #fakenames[2])]

	pl.incognito = true
	BroadcastLua("local ply = player.GetAll()[" .. pl:EntIndex() .. "] ply.incognito = true ply.fakename = '" .. fname .. "'")
	aprint(nil, "Incognito | ", pl, "Your name is now " .. fname)
	aprint(nil, "Incognito | ", pl, "To regenerate your name type `!incognito regen`")
	aprint(nil, "Incognito | ", pl, "To enter Stream Mode type `!incognito streammode`")
end