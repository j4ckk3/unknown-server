COMMAND.Name = "trialdemote"

COMMAND.Flag = D3A.Config.Commands.TrialDemote
COMMAND.AdminMode = true
COMMAND.CheckRankWeight = true

COMMAND.Args = {{"player", "Name/SteamID"}, {"string", "Reason"}}

COMMAND.Run = function(pl, args, supp)
	if (supp[1]:GetUserGroup() == "trialmod") then
		local succ, err = D3A.Ranks.SetPlayerRank(supp[1], "trialmod", 0, "user")
		if succ then
			D3A.Chat.SendToPlayer(pl, "You've demoted " .. supp[1]:Nick() .. "!", "INFO")
			D3A.Chat.SendToPlayer(supp[1], "You've been removed from trial mod!", "INFO")
		else
			D3A.Chat.SendToPlayer(pl, err, "ERR")
		end
	else
		D3A.Chat.SendToPlayer(pl, "User is not a trial mod!", "ERR")	
	end
end