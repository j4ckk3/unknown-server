COMMAND.Name = "Return"

COMMAND.Flag = D3A.Config.Commands.Return
COMMAND.AdminMode = true
COMMAND.CheckRankWeight = true

COMMAND.Args = {{"player", "Name/SteamID"}}

COMMAND.Run = function(pl, args, supp)
	if (not supp[1].LastPos) then
		D3A.Chat.SendToPlayer(pl, supp[1]:Name() .. " has no last known position!", "ERR")
		return
	end

	supp[1]:SetPos(supp[1].LastPos)
	supp[1].LastPos = nil

	D3A.Chat.BroadcastStaff(pl:Name() .. " has returned " .. supp[1]:Name())
end