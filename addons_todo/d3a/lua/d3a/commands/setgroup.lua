COMMAND.Name = "SetGroup"

COMMAND.AdminMode = true
COMMAND.Flag = D3A.Config.Commands.SetGroup

COMMAND.Args = {{"string", "Name/SteamID"}, {"string", "Rank"}, {"number", "Duration(minutes)"}}

COMMAND.Run = function(pl, args, supp)
	local plname = (pl:IsValid() and pl:Name()) or "Console"
	local plstid = (pl:IsValid() and pl:SteamID()) or "CONSOLE"
	local tmname = args[2]:lower()
	local exptime = supp[1] * 60
	local targ = D3A.FindPlayer(args[1]);
	local oldrank = "user"
	if targ then
		oldrank = targ:GetUserGroup()
	end
	local steamID
	local newrank
	
	if (exptime != 0) then
		if (!args[4]) then
			D3A.Chat.SendToPlayer(pl, "When setting a duration other than permanent, please input a rank to set this user to after expiration.", "ERR")
			return
		end
		newrank = args[4]
	end
	
	
	if (targ) then
		if (!D3A.Ranks.CheckWeight(pl, targ)) then
			D3A.Chat.SendToPlayer(pl, "Player's rank is equal or greater weight than yours!");
			D3A.Chat.SendToPlayer(targ, pl:Name() .. " (" .. pl:SteamID() .. ") attempted to use SetGroup on you.", "ERR")
			
			return
		end
		
		local succ, err = D3A.Ranks.SetPlayerRank(targ, tmname, exptime, newrank)
		
		if (!succ) then
			D3A.Chat.SendToPlayer(pl, err, "ERR")
		else
			local len = (exptime != 0 and exptime/60 .. " minute" .. ((exptime/60 != 1 and "s") or "")) or "permanent"
			
			tmname = D3A.Ranks.Stored[tmname].Name
			newrank = (newrank and D3A.Ranks.Stored[newrank].Name) or nil
			steamID = targ:SteamID()

			D3A.Chat.Broadcast(plname .. " has set " .. targ:Name() .. " (" .. steamID .. ") to " .. tmname .. ". Duration: " .. len .. ((newrank and ", expiring to " .. newrank .. ".") or "."))
		end
	else
		steamID = D3A.MySQL.Escape(args[1]);
		
		D3A.MySQL.Query("CALL selectUserInfo('" .. steamID .. "');", function(data)
			if (data and data[1]) then
				data = data[1];

				data.Vars = util.JSONToTable(data.Vars);
				
				oldrank = data.Rank or data.Vars[D3A.Ranks.Prefix .. "_rank"] or "user";

				if (!D3A.Ranks.CheckWeight(pl, oldrank)) then
					D3A.Chat.SendToPlayer(pl, "Player's rank is equal or greater weight than yours!");

					return;
				end
				
				local succ, err = D3A.Ranks.SetSteamIDRank(steamID, tmname, exptime, newrank, data);
				
				if (!succ) then
					D3A.Chat.SendToPlayer(pl, err, "ERR");
				else
					local len = (exptime != 0 and exptime/60 .. " minute" .. ((exptime/60 != 1 and "s") or "")) or "permanent"
			
					tmname = D3A.Ranks.Stored[tmname].Name
					newrank = (newrank and D3A.Ranks.Stored[newrank].Name) or nil
					
					D3A.Chat.Broadcast(plname .. " has set " .. data.SteamName .. " (" .. steamID .. ") to " .. tmname .. ". Duration: " .. len .. ((newrank and ", expiring to " .. newrank .. ".") or "."))
					
					D3A.Chat.SendToPlayer(pl, "WARNING: If this player is online on another D3 server right now, this will not work.", "ERR");
				end
			else
				D3A.Chat.SendToPlayer(pl, "Could not find player by SteamID: " .. steamID .. ".", "ERR");
			end
		end);
	end

	hook.Call( "PlayerRankChange", nil, steamID, tmname, oldrank ) -- entity Ply, string newRank, string oldRank
end