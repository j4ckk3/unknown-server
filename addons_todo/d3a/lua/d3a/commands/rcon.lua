COMMAND.Name = "Rcon"
COMMAND.Flag = D3A.Config.Commands.Rcon

COMMAND.Args = {{"string", "command"}}

COMMAND.Run = function(pl, args, supp)
	local str = ""
	for k, v in pairs(args) do
		str = str .. " " .. str or ""
	end
	
	print(str)
	RunConsoleCommand(str)
	D3A.Chat.Broadcast(pl:NameID() .. " ran an rcon command.")
end