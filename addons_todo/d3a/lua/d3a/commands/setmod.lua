COMMAND.Name = "TrialMod"

COMMAND.AdminMode = true
COMMAND.Flag = D3A.Config.Commands.SetTrialMod

COMMAND.Args = {{"string", "Name/SteamID"}}

COMMAND.Run = function(pl, args, supp)
	local plname = (pl:IsValid() and pl:Name()) or "Console"
	local plstid = (pl:IsValid() and pl:SteamID()) or "CONSOLE"
	local targ = D3A.FindPlayer(args[1]);
	local oldrank = "user"
	if targ then
		oldrank = targ:GetUserGroup()
	end
	local steamID
	
	
	if (targ) then
		if targ:IsMod() then
			D3A.Chat.SendToPlayer(pl, "You can't use this command on this user!");
			D3A.Chat.SendToPlayer(targ, pl:Name() .. " (" .. pl:SteamID() .. ") attempted to set you to trial mod.", "ERR")
			
			return
		end
		
		local succ, err = D3A.Ranks.SetPlayerRank(targ, "trialmod", 20160*60, oldrank)
		
		if (!succ) then
			D3A.Chat.SendToPlayer(pl, err, "ERR")
		else			
			steamID = targ:SteamID()

			D3A.Chat.Broadcast(plname .. " has set " .. targ:Name() .. " (" .. steamID .. ") to trial mod. Trial-Duration: 3 days starting now.")
		end
	else
		steamID = D3A.MySQL.Escape(args[1]);
		
		D3A.MySQL.Query("CALL selectUserInfo('" .. steamID .. "');", function(data)
			if (data and data[1]) then
				data = data[1];

				data.Vars = util.JSONToTable(data.Vars);
				
				oldrank = data.Rank or data.Vars[D3A.Ranks.Prefix .. "_rank"] or "user";

				if (!D3A.Ranks.CheckWeight(pl, oldrank)) then
					D3A.Chat.SendToPlayer(pl, "Player's rank is equal or greater weight than yours!");

					return;
				end
				
				local succ, err = D3A.Ranks.SetSteamIDRank(steamID, tmname, exptime, newrank, data);
				
				if (!succ) then
					D3A.Chat.SendToPlayer(pl, err, "ERR");
				else
					local len = (exptime != 0 and exptime/60 .. " minute" .. ((exptime/60 != 1 and "s") or "")) or "permanent"
			
					tmname = D3A.Ranks.Stored[tmname].Name
					newrank = (newrank and D3A.Ranks.Stored[newrank].Name) or nil
					
					D3A.Chat.Broadcast(plname .. " has set " .. data.SteamName .. " (" .. steamID .. ") to " .. tmname .. ". Duration: " .. len .. ((newrank and ", expiring to " .. newrank .. ".") or "."))
					
					D3A.Chat.SendToPlayer(pl, "WARNING: If this player is online on another D3 server right now, this will not work.", "ERR");
				end
			else
				D3A.Chat.SendToPlayer(pl, "Could not find player by SteamID: " .. steamID .. ".", "ERR");
			end
		end);
	end

	hook.Call( "PlayerRankChange", nil, steamID, tmname, oldrank ) -- entity Ply, string newRank, string oldRank
end