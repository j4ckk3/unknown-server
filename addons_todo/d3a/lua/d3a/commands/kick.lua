COMMAND.Name = "Kick"

COMMAND.Flag = D3A.Config.Commands.Kick
COMMAND.AdminMode = true
COMMAND.CheckRankWeight = true

COMMAND.Args = {{"player", "Name/SteamID"}, {"string", "Reason"}}

COMMAND.Run = function(pl, args, supplement)
	local plname = (pl:IsValid() and pl:Name()) or "Console"
	
	local targ = supplement[1]
	local reason = table.concat(args, " ", 2)
	
	D3A.Chat.Broadcast(targ:Name() .. " was kicked by " .. plname .. ". Reason: " .. reason .. ".")
	targ:Kick("Kicked by " .. plname .. ": " .. reason)
end