COMMAND.Name = "Jail"
COMMAND.Flag = D3A.Config.Commands.Jail
COMMAND.AdminMode = true

COMMAND.Args = {{"player", "Name/SteamID"}, {"number", "Duration (minutes)"}, {"string", "Reason"}}

local populateJail
COMMAND.Run = function(pl, args, supp)
	local targ = supp[1]
	local time = supp[2] * 60
	local reason = table.concat(args, ' ', 3)

	targ:SendLua([[D3A.OpenMoTD(true)]])

	targ:ExitVehicle()

	local JailProps = {}
	local jailDistance = populateJail(JailProps, 1) ^ 2

	targ.Jail = true
	targ.JailPos = targ.JailTPPos or targ:GetPos()
	targ.JailTPPos = nil
	targ.JailProps = targ.JailProps or {}
	if (targ.JailProps[1]) then
		for k, v in ipairs(targ.JailProps) do
			if (IsValid(v)) then v:Remove() end
		end
	end

	for k, v in ipairs(JailProps) do
		local prop = ents.Create("jail_wall")
        prop:SetPos(targ.JailPos + v.pos)
        prop:SetAngles(v.ang)
        prop:SetModel(v.model)
        prop:Spawn()
        prop:Activate()
        
        prop.target = targ
		prop.targetPos = targ.JailPos

		targ.JailProps[#targ.JailProps+1] = prop
	end

	if (time == 0) then -- until !unjail
		D3A.Chat.Broadcast(pl:Name() .. "(" .. pl:SteamID() .. ") has jailed " .. targ:Name() .. " (" .. targ:SteamID() .. ") indefinitely with reason: " .. reason .. ".")
		return
	else
		D3A.Chat.Broadcast(pl:Name() .. "(" .. pl:SteamID() .. ") has jailed " .. targ:Name() .. " (" .. targ:SteamID() .. ") for " .. supp[2] .. " minutes with reason: " .. reason .. ".")
	end

	local sid = targ:SteamID64()
	timer.Create("AutoUnjail" .. sid, time, 1, function()
		timer.Remove("JailWatch" .. sid)

		if (!IsValid(targ)) then return end
		if (!targ.Jail) then return end

		targ:SendLua("D3A.CloseMoTD()")
		
		for k, v in ipairs(targ.JailProps) do
			if (IsValid(v)) then v:SetCanRemove(true) v:Remove() end
		end

		targ.Jail = nil
		targ.JailPos = nil
		targ.JailProps = nil
	end)

	timer.Create("JailWatch" .. sid, 1, 0, function()
		if (!IsValid(targ)) then
			timer.Remove("JailWatch" .. sid)
			return
		end

		if (targ:GetPos():DistToSqr(targ.JailPos) > jailDistance) then
			targ:SetPos(targ.JailPos)
		end
	end)
end

populateJail = function(JailProps, type)
	if (type == 1) then -- normal
		table.insert(JailProps, {pos = Vector(0,0,-5), ang = Angle(90,0,0), model = "models/props_building_details/Storefront_Template001a_Bars.mdl"})
        table.insert(JailProps, {pos = Vector(0,0,97), ang = Angle(90,0,0), model = "models/props_building_details/Storefront_Template001a_Bars.mdl"})

        table.insert(JailProps, {pos = Vector(21,31,46), ang = Angle(0,90,0), model = "models/props_building_details/Storefront_Template001a_Bars.mdl"})
        table.insert(JailProps, {pos = Vector(21,-31,46), ang = Angle(0,90,0), model = "models/props_building_details/Storefront_Template001a_Bars.mdl"})
        table.insert(JailProps, {pos = Vector(-21,31,46), ang = Angle(0,90,0), model = "models/props_building_details/Storefront_Template001a_Bars.mdl"})
        table.insert(JailProps, {pos = Vector(-21,-31,46), ang = Angle(0,90,0), model = "models/props_building_details/Storefront_Template001a_Bars.mdl"})

        table.insert(JailProps, {pos = Vector(-52,0,46), ang = Angle(0,0,0), model = "models/props_building_details/Storefront_Template001a_Bars.mdl"})
		table.insert(JailProps, {pos = Vector(52,0,46), ang = Angle(0,0,0), model = "models/props_building_details/Storefront_Template001a_Bars.mdl"})

		return 70
	end
end

for k, v in ipairs({"PlayerNoClip", "PlayerSpawnObject", "CanPlayerEnterVehicle"}) do
	hook.Add(v, "D3A.Jail", function(pl) if (pl.Jail) then return false end end)
end

hook.Add("PlayerSpawn", "D3A.Jail", function(pl)
    if (pl.Jail) then
        timer.Simple(0.01, function() if IsValid(pl) then pl:SetPos(pl.JailPos) end end)
    end
end)

hook.Add("PlayerEnteredVehicle", "D3A.Jail", function(pl)
    if (pl.Jail) then
    	pl:ExitVehicle()
    end
end)