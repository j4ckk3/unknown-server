COMMAND.Name = "Slay"

COMMAND.Flag = D3A.Config.Commands.Slay
COMMAND.AdminMode = true
COMMAND.CheckRankWeight = true

COMMAND.Args = {{"player", "Name/SteamID"}}

COMMAND.Run = function(pl, args, supp)

	if supp[1]:Alive() then supp[1]:Kill() end
	
	D3A.Chat.SendToPlayer(pl, supp[1]:Name() .. " has been slain.")
	D3A.Chat.SendToPlayer(supp[1], pl:Name() .. " has slain you.")
end