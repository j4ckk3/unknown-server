COMMAND.Name = "MuteChat"

COMMAND.Flag = D3A.Config.Commands.MuteChat
COMMAND.AdminMode = true
COMMAND.CheckRankWeight = true

COMMAND.Args = {{"player", "Name/SteamID"}}

COMMAND.Run = function(pl, args, supp)
	supp[1].ChatMuted = (not supp[1].ChatMuted)

	D3A.Chat.Broadcast(pl:Name() .. " has " .. ((supp[1].ChatMuted) and "muted " or "unmuted ") .. supp[1]:Name() .. "'s chat")
end

hook.Add("PlayerSay", "D3A.MuteChat.PlayerSay", function(pl)
	if pl.ChatMuted then 
		return ""
	end
end)