COMMAND.Name = "Perma"

COMMAND.Flag = D3A.Config.Commands.Perma
COMMAND.AdminMode = true
COMMAND.CheckRankWeight = true

COMMAND.Args = {{"string", "Name/SteamID"}, {"string", "Reason"}}

COMMAND.Run = function(pl, args, supplement)
	local plname = (pl:IsValid() and pl:Name()) or "Console"
	local plstid = (pl:IsValid() and pl:SteamID()) or "CONSOLE"

	local targ = args[1]:upper()
	
	local targpl = supplement[1] or D3A.FindPlayer(targ)
	
	local targstid

	if (!targpl and string.sub(targ, 1, 8) != "STEAM_0:") then
		D3A.Chat.SendToPlayer(pl, "Unknown player: " .. targ, "ERR")
		return
	elseif (!targpl) then
		targstid = targ
	else
		targstid = targpl:SteamID()
		
		if (!D3A.Ranks.CheckWeight(pl, targpl)) then
			D3A.Chat.SendToPlayer(pl, "Player's rank is equal or greater weight than yours!", "ERR")
			D3A.Chat.SendToPlayer(targpl, plname .. " (" .. plstid .. ") attempted to use Ban on you.", "ERR")
			return false
		end
	end
	
	D3A.Bans.GetBans(targstid, function(Bans)
		if (Bans.Current) then
			if (!pl:HasAccess("S")) then
				D3A.Chat.SendToPlayer(pl, targstid .. " is already banned ('S' access required to update a ban)", "ERR")
				return
			end

			local reason = table.concat(args, " ", 2)
			
			if D3A.Bans.BanPlayer(targstid, plstid, 1, "perm", reason, Bans.Current.Time) then
				D3A.Chat.Broadcast(targstid .. "'s ban was updated by " .. plname .. " to permanent. Reason: " .. reason .. ".")
			else
				D3A.Chat.SendToPlayer(pl, "An unexpected error has occurred while banning. Tell the boss!", "ERR")
			end
		else
			local reason = table.concat(args, " ", 2)
		
			if D3A.Bans.BanPlayer(targstid, plstid, 1, "perm", reason) then
				D3A.Chat.Broadcast(((targpl and targpl:Name()) or targstid) .. " was banned permanently by " .. plname .. ". Reason: " .. reason .. ".")
			else
				D3A.Chat.SendToPlayer(pl, "An unexpected error has occurred while banning. Tell the boss!", "ERR")
			end
		end
	end)
end