/* 
	This was coded by KingofBeast & updated by aStonedPenguin.
	Licensed to N/A
*/

D3A = D3A or {
	Config = {},
	Alias = "D3A",
	Version = "1.51",
}

D3A.IncludeSV = (SERVER) and include or function() end
D3A.IncludeCL = (SERVER) and AddCSLuaFile or include
D3A.IncludeSH = function(path) D3A.IncludeSV(path) D3A.IncludeCL(path) end

D3A.IncludeSH "d3a_configs/misc.lua"
D3A.IncludeSH "util.lua"

print "\n\n"
D3A.Print("This server runs " .. D3A.Alias .. " " .. D3A.Version .. "\n\n")

if (SERVER) then
	D3A.IncludeSV "d3a_configs/mysql.lua"
	if D3A.Config.hostname == "1.2.3.4" then -- for the dev server
		D3A.IncludeSV "d3a_mysql_dev.lua"
	end
	D3A.IncludeSV "d3a/server/mysql/sv_init.lua"
end

local Files, Folders
if (SERVER) then
	D3A.Print("Parsing serverside files")
	Files, Folders = file.Find("d3a/server/*.lua", "LUA")
	for k, v in ipairs(Files) do
		D3A.Print(" | " .. v)
		D3A.IncludeSV("d3a/server/" .. v)
	end
end

D3A.IncludeSV "d3a_configs/ranks.lua"

D3A.Print("Parsing clientside files")
Files, Folders = file.Find("d3a/client/*.lua", "LUA")
for k, v in ipairs(Files) do
	D3A.Print(" | " .. v)
	D3A.IncludeCL("d3a/client/" .. v)
end

D3A.Print("Parsing shared files")
Files, Folders = file.Find("d3a/shared/*.lua", "LUA")
for k, v in ipairs(Files) do
	D3A.Print(" | " .. v)
	D3A.IncludeSH("d3a/shared/" .. v)
end

D3A.Print("Scripts loaded. Initializing when GM is ready.")
print "\n\n"

hook.Add("Initialize", "D3A.Initialize", function() hook.Call("D3A_Initialize", GAMEMODE) end)