local fr
function D3A.OpenMoTD(stayOpen)
	if (D3A.Config.MoTD == nil) or (D3A.Config.MoTD == "") or IsValid(fr) then return end

	fr = vgui.Create("DFrame")
	fr:SetTitle("MoTD")
	fr:SetSize(ScrW() * 0.9, ScrH() * 0.9)
	fr:Center()
	fr:MakePopup()

	local html = vgui.Create("DHTML", fr)
	html:SetPos(5, 30)
	html:SetSize(fr:GetWide() - 10, fr:GetTall() - 90)
	html:OpenURL(D3A.Config.MoTD)

	local cls = vgui.Create("DButton", fr)
	cls:SetPos(fr:GetWide()/2 - 75, fr:GetTall() - 55)
	cls:SetSize(150, 50)
	cls:SetText("Close")
	cls.DoClick = function(self)
		fr:Remove()
	end
	
	if (stayOpen) then
		cls:SetText("JAILED")
		cls:SetDisabled(true)
		fr:ShowCloseButton(false)
	end
end

function D3A.CloseMoTD()
	if (IsValid(fr)) then fr:Remove() end
end

hook.Add("InitPostEntity", "D3A.OpenMoTD.InitPostEntity", D3A.OpenMoTD)