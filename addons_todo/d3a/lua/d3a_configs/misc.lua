D3A.Config.MoTD = "" -- Set to blank for no motd
D3A.Config.owners = {
        ["STEAM_0:0:78142582"] = true,
        ["STEAM_1:1:59719012"] = true,
} -- Set to blank for no motd

-- These are used to determine what ranks IsAdmin and IsAdmin IsSuperAdmin return true on
D3A.Config.IsMod 			= "m"
D3A.Config.IsAdmin 			= "a"
D3A.Config.IsSuperAdmin 	= "g"

-- Misc
D3A.Config.PlayerNoClip 	= "m"
D3A.Config.PlayerPhysgun 	= "m"
D3A.Config.StaffChat	 	= "m"

--[[
	Automatic chat advertisements
]]
D3A.Config.Adverts = {
	--[[{
		Time = 305,
		Repos = 4,
		Message = {Color(255,50,50), "FLASH SALE: 25% EXTRA CREDITS RIGHT NOW! Type !shop"},
	},
	{
		Time = 1525,
		Repos = 0,
		Message = {Color(255,50,50), "FLASH SALE: 25% OFF CREDITS RIGHT NOW! Type !shop"},
	},]]--

	{
		Time = 620,
		Repos = 0,
		Message = {Color(50,50,255), "Want extra features? Type !shop"},
	},
	{
		Time = 900,
		Repos = 10,
		Message = {Color(255,50,0), "Servers are expensive to keep up. Consider donating to keep Unknown Networks alive! Type !shop"},
	},
}

--[[
	Use this to set the flag for any stock d3a commands
]]
D3A.Config.Commands = {
	--PlayTime 	= "", 
	--MoTD 		= "", -- Empty will just let anyone use it
	SetMoney 	= "*",
	CrashBan 	= "*",
	Incognito 	= "*",
	Rcon 		= "*",

	SetGroup 	= "g",
	Reconnect 	= "g",
	Reload 		= "g",

	SetTrialMod	= "e",
	TrialDemote = "e",
	
	UpdateBan 	= "s",
	Unban 		= "s",
	NoLag 		= "s",

	Perma 		= "a",

	Adminmode 	= "m",
	Ban 		= "m",
	Bring 		= "m",
	ClearDecals	= "m",
	ForceMoTD 	= "m",
	Freeze 		= "m",
	GetMoney	= "m",
	Goto 		= "m",
	Jail 		= "m",
	JailTP 		= "m",
	Kick 		= "m",
	MuteChat 	= "m",
	MuteVoice 	= "m",
	PO 			= "m",
	Respawn 	= "m",
	Return 		= "m",
	Send 		= "m",
	SetArmor 	= "m",
	SetHealth 	= "m",
	SetJob 		= "m",
	Slay 		= "m",
	StopSounds 	= "m",
	StripWeps 	= "m",
	Tele 		= "m",
	Unjail 		= "m",

	--UnSpectate = "m",
	--Spectate 	= "m",
	--uncuff	= "m",
	--unarrest 	= "m",
	--rplookup 	= "m",
	--unrag	 	= "m",
}
