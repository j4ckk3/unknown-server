-- Ranks
-- Usage: name, weight(1 - 255), flags, global
D3A.Ranks.Register("root", 100, 		"*", true)
D3A.Ranks.Register("cmanager", 70,		"gesam", false)
D3A.Ranks.Register("manager", 65, "gesam")
D3A.Ranks.Register("mentor", 60,		"esam", false)
D3A.Ranks.Register("senioradmin", 50,	"sam", false)
D3A.Ranks.Register("admin", 50,			"am", false)
D3A.Ranks.Register("mod", 25, 			"m", false)
D3A.Ranks.Register("trialmod", 20, 		"m", false)
D3A.Ranks.Register("user", 1, 			"", true) -- NEVER Remove user