-- from the best rp fprp!1 thx notsosuper x

if CLIENT then
	SWEP.PrintName = "Blowtorch"
	SWEP.Slot = 2
	SWEP.SlotPos = 1
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = true
end

SWEP.Author = "NotSoSuper"
SWEP.Instructions = "Left Click: Hold to torch props!"
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.ViewModelFOV = 75
SWEP.ViewModelFlip = true

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

SWEP.ViewModel			= "models/weapons/v_smg_ump45.mdl"
SWEP.WorldModel			= "models/weapons/w_smg_ump45.mdl"

SWEP.ShootSound = Sound("ambient/energy/spark1.wav");

SWEP.TorchDistance = 90;
SWEP.TorchAmount = 1;
SWEP.TorchTimeout = 120;

local TorchableEnts = {"prop_physics"};

function SWEP:TorchEntity(ent, first)

	local PhysObj = ent:GetPhysicsObject()
	if IsValid(PhysObj) then
		PhysObj:EnableMotion(false)
	end

	ent.TorchAmount = nil;
	ent.StartTorch = nil;
	ent.SavedColor = ent:GetColor();
	ent.SavedSolid = ent:GetSolid();
	
	ent:SetRenderMode(RENDERMODE_TRANSALPHA)
	ent:SetColor(Color(ent.SavedColor.r,ent.SavedColor.g,ent.SavedColor.b,100));
	ent:SetSolid(SOLID_NONE);

	local timeout = math.Clamp(self.TorchTimeout - ent:BoundingRadius(), 10, 20)

	timer.Simple(timeout, function()
		if IsValid(ent) then
			ent:SetColor(ent.SavedColor);
			ent:SetSolid(ent.SavedSolid);
		end
	end);

	if SERVER and first then
		if math.random(1,7) == 1 then
			local vPoint = ent:GetPos()
			local effectdata = EffectData()
			effectdata:SetStart(vPoint)
			effectdata:SetOrigin(vPoint)
			effectdata:SetScale(1)
			util.Effect("Explosion", effectdata, true, true)
			self:Remove()
		end
	end
end

function SWEP:Initialize()
	self:SetWeaponHoldType("pistol")
end

function SWEP:Deploy()
	self:SetWeaponHoldType("pistol")
end

function SWEP:CanPrimaryAttack ( ) return true; end

function SWEP:PrimaryAttack()	
	if self:GetTable().LastNoise == nil then self:GetTable().LastNoise = true; end
	if self:GetTable().LastNoise then
		self.Weapon:EmitSound(self.ShootSound, 90)
		self:GetTable().LastNoise = false;
	else
		self:GetTable().LastNoise = true;
	end
	
	local Trace = self.Owner:GetEyeTrace();
	if Trace.HitPos:Distance(self.Owner:GetPos()) > self.TorchDistance || !IsValid(Trace.Entity) then
		self.Weapon:SetNextPrimaryFire(CurTime() + 1)
		--if SERVER then self.Owner:AddNote("Must use on a prop!"); end
		return false;
	end
	
	
	if !table.HasValue(TorchableEnts, Trace.Entity:GetClass()) then
		self.Weapon:SetNextPrimaryFire(CurTime() + 1)
		--if SERVER then self.Owner:AddNote("Must use on a prop!"); end
		return false;
	end
	local effectdata = EffectData()
	effectdata:SetOrigin(Trace.HitPos)
	effectdata:SetMagnitude(1)
	effectdata:SetScale(1)
	effectdata:SetRadius(2)
	util.Effect("Sparks", effectdata)
	self.Weapon:SetNextPrimaryFire(CurTime() + 0.1)
	self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)

	--if !SERVER then return end
	local ent = Trace.Entity;
	local curtime = CurTime()
	if ent.lastCurtime and curtime - ent.lastCurtime < 1 then return end

	ent.lastCurtime = curtime
	if !ent.StartTorch then ent.StartTorch = curtime end
	if !ent.TorchAmount then ent.TorchAmount = 0 end
	ent.TorchAmount = curtime - ent.StartTorch;

	timer.Create(tostring(ent:EntIndex()) .. "_torch_reset", 2, 1, function()
		if IsValid(ent) then
			ent.TorchAmount = nil;
			ent.StartTorch = nil;
			ent.lastCurtime	= nil;
		end
	end)
	
	if ent.TorchAmount >= self.TorchAmount then
		self:TorchEntity(ent, true);
		timer.Remove(tostring(ent:EntIndex()) .. "_torch_reset")

		for k, v in pairs(ents.FindInSphere(ent:GetPos(), 3)) do
			self:TorchEntity(v)
		end
	end
end

function SWEP:SecondaryAttack()
	--self:PrimaryAttack();
end
