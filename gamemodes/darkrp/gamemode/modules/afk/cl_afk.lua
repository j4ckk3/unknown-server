local TextColor = Color(255, 100, 100, 255)

local function AFKHUDPaint()
    if not LocalPlayer():getDarkRPVar("AFK") then return end
    draw.DrawNonParsedSimpleText(DarkRP.getPhrase("afk_mode"), "DermaLarge", ScrW() / 2, (ScrH() / 2) - 100, TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
    draw.DrawNonParsedSimpleText(DarkRP.getPhrase("salary_frozen"), "DermaLarge", ScrW() / 2, (ScrH() / 2) - 60, TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)

    if not LocalPlayer():getDarkRPVar("AFKDemoted") then
        draw.DrawNonParsedSimpleText(DarkRP.getPhrase("no_auto_demote"), "DermaLarge", ScrW() / 2, (ScrH() / 2) - 20, TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
    else
        draw.DrawNonParsedSimpleText(DarkRP.getPhrase("youre_afk_demoted"), "DermaLarge", ScrW() / 2, (ScrH() / 2) - 20, TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
    end

    draw.DrawNonParsedSimpleText(DarkRP.getPhrase("afk_cmd_to_exit"), "DermaLarge", ScrW() / 2, (ScrH() / 2) + 20, TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
end

hook.Add("HUDPaint", "AFK_HUD", AFKHUDPaint)
