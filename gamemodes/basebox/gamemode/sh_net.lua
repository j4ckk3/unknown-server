-- Net proxy to stop people systematically bombing in-effecient addons etc.
-- Could also prevent bugs in addons spamming the server with net messages?
if !basecfg.netprotect then return end
net = net or {}

local whitelist = { -- Things which we know aren't exploitable
	["gm_netmsg"] = true,
	["DarkRP_preferredjobmodels"] = true
}

if CLIENT then -- Assume the client isn't a hacker and the issue is a bug causing them to spam net messages
	local SendToServer = SendToServer or net.SendToServer
	local ply = LocalPlayer()

	local function netProxy() -- Clientside proxy
		local curtime = CurTime()

		if ply.netDelay and ply.netDelay > curtime and not whitelist[msg] then
			if !DEV_SERVER then return else print("You're sending net messages quickly!") end
		end

		ply.netDelay = curtime + 0.5
		SendToServer()
	end

	function net.SendToServer()
		netProxy()
	end
else -- Ensure net messages are not exceding the delay in case they bypassed our clientside detour
	local Receive = Receive or net.Receive

	local function netProxy(len, ply, callback, msg) -- Serverside proxy
		local curtime = CurTime()

		if ply.netDelay and ply.netDelay > curtime and not whitelist[msg] then
			print(ply:SteamID() .. ": is sending net message very quickly: " .. (msg or "Unknown!"))
			if !DEV_SERVER then return else print("  > Ignoring because server is in development mode.") end
		end

		ply.netDelay = curtime + 0.5
		callback(len, ply)
	end

	function net.Receive(msg, callback)
		Receive(msg, function(len, ply) netProxy(len, ply, callback, msg) end)
	end
end