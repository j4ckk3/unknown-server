-- Concommand proxy to stop people systematically bombing concommands
if !basecfg.conprotect then return end

concommand = concommand or {}
local concommand2 = concommand2 or {}
concommand2.Minus = concommand2.Minus or concommand.Add

function concommand2.Proxy(ply, strCmd, tblArgs, strArgs, callback)
	local curtime = CurTime()

	if ply.cmdDelay and ply.cmdDelay > curtime then -- curtime resetting during development is kinda annoying :<
		print(ply:SteamID() .. " is calling " .. strCmd .. " very quickly.")
		if !DEV_SERVER then return else print("  > Ignoring because server is in development mode.") end
	end

	ply.cmdDelay = curtime + 0.5
	callback(ply, strCmd, tblArgs, strArgs)
end

function concommand.Add(strName, funcCallback, funcAutoComplete, strHelpText, flags)
	concommand2.Minus(strName, function(ply, strCmd, tblArgs, strArgs) concommand2.Proxy(ply, strCmd, tblArgs, strArgs, funcCallback) end, funcAutoComplete, strHelpText, flags)
end