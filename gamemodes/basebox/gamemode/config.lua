-- sandbox with improvements and a modular loading system
basecfg = {} basecfg.spawnmenu = {} basecfg.context = {} basecfg.toolgun = {}
DEV_SERVER = true -- Set this to true for net&concommand proxy debugging

-- which tabs at the top of the spawnmenu to load yes
basecfg.spawnmenu.enabled = 	true
	basecfg.spawnmenu.npcs =		false
	basecfg.spawnmenu.weapons = 	true
	basecfg.spawnmenu.entities = 	true
	basecfg.spawnmenu.postprocess =	false
	basecfg.spawnmenu.vehicles = 	false
	basecfg.spawnmenu.extraprops = 	false -- this is the "addons" and "games" tab on the spawnmenu in the props area under "browse"

	-- context menu yes / Note: If the spawnmenu is disabled, the context menu will not load
	basecfg.context.enabled = 	true 
		basecfg.context.persist = 	true -- obvs wont work if you disable persistence system (below)
		basecfg.context.ignite = 	false
		basecfg.context.extinguish= false
		basecfg.context.remover = 	false
		basecfg.context.collision = false
		basecfg.context.drive = 	false
		basecfg.context.gravity = 	false
		basecfg.context.npc_bigger=	false
		basecfg.context.npc_smaller=false
		basecfg.context.keepupright=false
		basecfg.context.bonemanipulate=false
		basecfg.context.bodygroups=	false
		basecfg.context.skin=		false

-- to load tools yes
basecfg.toolgun.enabled = 		true

	-- stable/safe/common shit
	basecfg.toolgun.button =		true
	basecfg.toolgun.camera =		true
	basecfg.toolgun.material =		true
	basecfg.toolgun.colour =		true
	basecfg.toolgun.nocollide =		true
	basecfg.toolgun.remover =		true
	basecfg.toolgun.weld =			true

	-- probably stable shit
	basecfg.toolgun.leafblower =	false
	basecfg.toolgun.axis =			false
	basecfg.toolgun.balloon =		false
	basecfg.toolgun.ballsocket =	true
	basecfg.toolgun.dupe =			false
	basecfg.toolgun.dynamite =		false
	basecfg.toolgun.elasic =		false
	basecfg.toolgun.emitter =		false
	basecfg.toolgun.hoverball =		false
	basecfg.toolgun.hydraulic =		false
	basecfg.toolgun.lamp =			false
	basecfg.toolgun.light =			true
	basecfg.toolgun.motor =			false
	basecfg.toolgun.muscle =		false
	basecfg.toolgun.paint =			false
	basecfg.toolgun.physprop =		false -- aka; physical properties
	basecfg.toolgun.pulley =		false
	basecfg.toolgun.rope =			false
	basecfg.toolgun.slider =		false
	basecfg.toolgun.thruster =		false
	basecfg.toolgun.trails =		false
	basecfg.toolgun.wheel =			false
	basecfg.toolgun.winch =			false

	-- poser shit
	basecfg.toolgun.eyeposer =		false
	basecfg.toolgun.faceposer =		false
	basecfg.toolgun.fingerposer =	false
	basecfg.toolgun.inflator =		false

	-- probably don't disable shit
	basecfg.toolgun.creator =		true -- probably don't disable this
	basecfg.toolgun.editent =		true -- probably don't disable this

-- misc settings
basecfg.camera	= 				true
basecfg.spawncommands =			true -- disable this to totally stop the prop/npc/ragdoll/weapon spawning system
basecfg.persistence =			false -- sandbox persistence system
basecfg.persistencepage =		1 	-- sandbox persistence page
basecfg.netprotect =			false -- net message bomb protection
basecfg.conprotect =			false -- console command bomb protection
	-- Note: the protection systems will break if you Live Lua Refresh!