LawHUD = LawHUD or {}
LawHUD.Draw = true
LawHUD.y = 3
local width = 200
local height = 35
local x = ScrW() - 5 - width
local title = "Laws of the land"
local DrawLawsConVar = CreateClientConVar("cl_drawlaws", "1")

net.Receive("LawHUD_Sync", function(len)
    LawHUD.Laws = LawHUD:ReadCompressedTable()
end)

local blur = Material("pp/blurscreen")
local function DrawBlurRect(x, y, w, h, times)
    times = times or 1
    local X, Y = 0,0

    surface.SetDrawColor(255,255,255)
    surface.SetMaterial(blur)

    for i = 1, times do
        blur:SetFloat("$blur", (i / 3) * (5))
        blur:Recompute()

        render.UpdateScreenEffectTexture()

        render.SetScissorRect(x, y, x+w, y+h, true)
            surface.DrawTexturedRect(X * -1, Y * -1, ScrW(), ScrH())
        render.SetScissorRect(0, 0, 0, 0, false)
    end
   
    draw.RoundedBox(0,x,y,w,h,Color(0,0,0,150))
    surface.SetDrawColor(30,30,30)
    surface.DrawOutlinedRect(x,y,w,h)
end

local function DrawBlur(panel, amount)
    local x, y = panel:LocalToScreen(0, 0)
    local scrW, scrH = ScrW(), ScrH()
    surface.SetDrawColor(255, 255, 255)
    surface.SetMaterial(blur)
    for i = 1, 3 do
        blur:SetFloat("$blur", (i / 3) * (amount or 6))
        blur:Recompute()
        render.UpdateScreenEffectTexture()
        surface.DrawTexturedRect(x * -1, y * -1, scrW, scrH)
    end
end

local function createFont(name, size, weight, outline)
    surface.CreateFont(name, {
        font = "Roboto" .. (size <= 22 and "Light" or ""),
        size = size,
        weight = weight,
        outline = outline
    })
end
createFont("LawHUD_Title", 22, 400)
createFont("LawHUD_Law", 18, 400)
createFont("LawHUD_Button", 32, 200)
createFont("LawHUD_Entry", 22, 400)

local function getTextSize(font, name)
    surface.SetFont(font)
    local x, y = surface.GetTextSize(name)
    return {
        x = x,
        y = y,
    }
end

hook.Add("HUDPaint", "LawHUD", function()
    if not LawHUD.Draw or not DrawLawsConVar:GetBool() then return end
    DrawBlurRect(x, LawHUD.y, width, height)

    local titleSize = getTextSize("LawHUD_Title", title)
    surface.SetFont("LawHUD_Title")
    surface.SetTextColor(color_white)
    surface.SetTextPos(x + width / 2 - titleSize.x / 2, LawHUD.y+5)
    surface.DrawText(title)

    local nextHeight = 15 + titleSize.y
    local nextWidth = 5
    for _, __ in pairs(LawHUD.Laws) do
        local str = _ .. ". " .. __
        local size = getTextSize("LawHUD_Law", str)
        nextHeight = nextHeight + size.y
        if math.Round(height) < nextHeight then continue end
        if size.x > math.Round(nextWidth) then
            nextWidth = size.x + 10 + 10 + 5
        end
        if math.Round(width) < size.x then continue end
        surface.SetFont("LawHUD_Law")
        surface.SetTextColor(color_white)
        surface.SetTextPos(x + 10, LawHUD.y + 5 + titleSize.y + 5 + ((_ - 1) * size.y))
        surface.DrawText(str)
    end
    if titleSize.x > math.Round(nextWidth) then nextWidth = titleSize.x + 10 + 10 end
    height = Lerp(0.1, height, nextHeight)
    width = Lerp(0.1, width, nextWidth)
    x = ScrW() - 10 - width
end)

net.Receive("LawHUD_Admin", function(len)
    print("open")
    if not LawHUD:HasPerm(LocalPlayer()) then return end
    print("Yes")
    local frame = vgui.Create("DFrame")
    frame:SetSize(500, 600)
    frame:Center()
    frame:SetTitle("")
    frame:SetDraggable(true)
    frame:MakePopup()
    frame:ShowCloseButton(false)
    frame.Paint = function(self, w, h)
        DrawBlur(self, 3)
        local x, y = 0, 0
        draw.RoundedBox(0, x, y, w, h, Color(0, 0, 0, 150))
        surface.SetDrawColor(30, 30, 30)
        surface.DrawOutlinedRect(x, y, w, h)
    end

    local save = vgui.Create("DButton", frame)
    save:Dock(BOTTOM)
    save:SetTall(50)
    save:SetText("")
    save.Paint = function(self, w, h)
        surface.SetDrawColor(Color(0, 0, 0, 210))
        surface.DrawRect(0, 0, w, h)

        local size = getTextSize("LawHUD_Button", "Save")
        surface.SetFont("LawHUD_Button")
        surface.SetTextColor(color_white)
        surface.SetTextPos(w / 2 - size.x / 2, h / 2 - size.y / 2)
        surface.DrawText("Save")
    end

    local scroll = vgui.Create("DScrollPanel", frame)
    scroll:Dock(FILL)
    scroll:DockMargin(0, 0, 0, 5)
    local scrollClr = Color(0, 0, 0, 170)
    local a = function(self, w, h) surface.SetDrawColor(scrollClr) surface.DrawRect(0, 0, w, h) end
    local b = function(panel) panel.Paint = a end
    local sbar = scroll:GetVBar()
    b(sbar)
    b(sbar.btnUp)
    b(sbar.btnDown)
    b(sbar.btnGrip)

    local clearMat = Material("icon16/cancel.png", "noclamp smooth")
    local entrys = {}
    for I = 1, 15 do
        local panel = vgui.Create("DPanel", scroll)
        panel:Dock(TOP)
        panel:DockMargin(0, 0, 0, 5)
        panel:SetTall(30)
        panel.Paint = function() end

        local entry = vgui.Create("DTextEntry", panel)
        entry:Dock(FILL)
        entry:SetText(LawHUD.Laws[I] or "Rule " .. I)
        entry:SetFont("LawHUD_Entry")
        table.insert(entrys, entry)

        local clear = vgui.Create("DButton", panel)
        clear:Dock(RIGHT)
        clear:SetWide(24)
        clear:SetText("")
        clear:DockMargin(5, 5, 5, 5)
        clear.Paint = function(self, w, h)
            surface.SetDrawColor(color_white)
            surface.SetMaterial(clearMat)
            surface.DrawTexturedRect(0, 0, w, h)
        end
        clear.DoClick = function()
            entry:SetText("Rule " .. I)
        end

        scroll:AddItem(panel)
    end

    save.DoClick = function()
        local laws = {}
        for _, __ in pairs(entrys) do
            local val = string.sub(__:GetValue(), 0, LawHUD.CharLimit)
            if string.Trim(val) == "" then continue end
            if val == "Rule " .. _ then continue end
            table.insert(laws, val)
        end
        net.Start("LawHUD_UpdateLaws")
            LawHUD:WriteCompressedTable(laws)
        net.SendToServer()
        frame:Close()
    end
end)