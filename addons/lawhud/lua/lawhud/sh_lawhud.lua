LawHUD = LawHUD or {}
LawHUD.Laws = {}
LawHUD.CharLimit = 32

function LawHUD:HasPerm(ply)
    if ply:IsAdmin() then return true end
    if ply:getJobTable().name == "Mayor" then return true end

    return false
end

function LawHUD:CleanTable(tbl)
    local new = {}
    for _, __ in pairs(tbl) do
        if string.Trim(__) == "" then continue end
        if string.len(__) > LawHUD.CharLimit then __ = string.sub(__, 0, LawHUD.CharLimit) end
        table.insert(new, __) 
    end
    return new
end

function LawHUD:WriteCompressedTable(tbl)
    local data = tbl
    data = util.TableToJSON(data)
    data = util.Compress(data)
    local size = string.len(data)
    net.WriteUInt(size, 32)
    net.WriteData(data, size)
end

function LawHUD:ReadCompressedTable()
    local size = net.ReadUInt(32)
    local data = net.ReadData(size)
    data = util.Decompress(data)
    data = util.JSONToTable(data)
    return data
end