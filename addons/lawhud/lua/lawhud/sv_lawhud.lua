LawHUD = LawHUD or {}

util.AddNetworkString("LawHUD_Sync")
util.AddNetworkString("LawHUD_Admin")
util.AddNetworkString("LawHUD_UpdateLaws")

function LawHUD:SendLaws(ply)
    net.Start("LawHUD_Sync")
        LawHUD:WriteCompressedTable(LawHUD.Laws)
    net.Send(ply or player.GetAll())
end

hook.Add("PlayerInitialSpawn", "LawHUD_Sync", function(ply)
    LawHUD:SendLaws(ply)
end)

hook.Add("PlayerSay", "LawHUD_Admin", function(ply, text, team)
    if string.StartWith(text, "/laws") then
        if LawHUD:HasPerm(ply) then
            net.Start("LawHUD_Admin")
            net.Send(ply)
            return ""
        end
        aprint(nil, "Laws | ", ply, "Only the mayor can set the laws!")
        return ""
    end
end)

net.Receive("LawHUD_UpdateLaws", function(len, ply)
    if not LawHUD:HasPerm(ply) then return end
    local laws = LawHUD:ReadCompressedTable()
    LawHUD.Laws = LawHUD:CleanTable(laws)
    LawHUD:SendLaws()
end)