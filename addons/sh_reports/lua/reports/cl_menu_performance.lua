local function L(...) return SH_REPORTS:L(...) end

local matBack = Material("shenesis/general/back.png")

function SH_REPORTS:ShowPerformanceReports()
	if (IsValid(_SH_REPORTS_PERF)) then
		_SH_REPORTS_PERF:Remove()
	end

	local styl = self.Style
	local th, m = self:GetPadding(), self:GetMargin()
	local m2 = m * 0.5
	local ss = self:GetScreenScale()

	local delay = 0
	if (self.ServerTime) then
		delay = self.ServerTime - os.time()
	end

	local curprep

	local frame = self:MakeWindow(L"performance_reports")
	frame:SetSize(800 * ss, 600 * ss)
	frame:Center()
	frame:MakePopup()
	_SH_REPORTS_PERF = frame

		frame:AddHeaderButton(matBack, function()
			frame:Close()
			self:ShowReports()
		end)

		local sel = vgui.Create("DScrollPanel", frame)
		sel:SetDrawBackground(false)
		sel:SetWide(140 * ss)
		sel:Dock(LEFT)
		sel.Paint = function(me, w, h)
			draw.RoundedBoxEx(4, 0, 0, w, h, styl.inbg, false, false, true, false)
		end
		self:PaintScroll(sel)

		local ilist_perf = vgui.Create("DListView", frame)
		ilist_perf:SetVisible(false)
		ilist_perf:SetSortable(false)
		ilist_perf:SetDrawBackground(false)
		ilist_perf:SetDataHeight(32)
		ilist_perf:Dock(FILL)
		ilist_perf:AddColumn(L"admin")
		ilist_perf:AddColumn(L"num_claimed")
		ilist_perf:AddColumn(L"num_closed")
		ilist_perf:AddColumn(L"time_spent")
		self:PaintList(ilist_perf)

		local ilist_rating = vgui.Create("DListView", frame)
		ilist_rating:SetVisible(false)
		ilist_rating:SetSortable(false)
		ilist_rating:SetDrawBackground(false)
		ilist_rating:SetDataHeight(32)
		ilist_rating:Dock(FILL)
		ilist_rating:AddColumn(L"admin")
		ilist_rating:AddColumn(L"rating")
		self:PaintList(ilist_rating)

		frame.ShowStaff = function(me, staffs)
			if (!ilist_perf:IsVisible()) then
				return end

			local i = 0
			for _, info in pairs (staffs) do
				local user = vgui.Create("DPanel", frame)
				user:SetDrawBackground(false)

					local avi = self:Avatar(info.steamid, 24, user)
					avi:SetPos(4, 4)

					local name = self:QuickLabel("...", "{prefix}Medium", styl.text, user)
					name:Dock(FILL)
					name:SetTextInset(ilist_perf:GetDataHeight(), 0)

					self:GetName(info.steamid, function(nick)
						if (IsValid(name)) then
							name:SetText(nick)
						end
					end)

				local line = ilist_perf:AddLine(user, info.claimed, info.closed, self:FullFormatTime(info.timespent))
				line:SetAlpha(0)
				line:AlphaTo(255, 0.1, 0.1 * i)
				self:LineStyle(line)

				i = i + 1
			end
		end

		frame.ShowRatings = function(me, ratings)
			if (!ilist_rating:IsVisible()) then
				return end

			ilist_rating:Clear()

			local i = 0
			for _, info in pairs (ratings) do
				if (info.num == 0) then
					continue end

				local frac = info.total / info.num / 5
				local tot = string.Comma(info.num)
				local tx = " " .. math.Round(frac * 100) .. "% (" .. tot .. ")"

				local user = vgui.Create("DPanel", frame)
				user:SetDrawBackground(false)

					local avi = self:Avatar(info.steamid, 24, user)
					avi:SetPos(4, 4)

					local name = self:QuickLabel("...", "{prefix}Medium", styl.text, user)
					name:Dock(FILL)
					name:SetTextInset(ilist_rating:GetDataHeight(), 0)

					self:GetName(info.steamid, function(nick)
						if (IsValid(name)) then
							name:SetText(nick)
						end
					end)

				local stars = vgui.Create("DPanel", frame)
				stars.Paint = function(me, w, h)
					local _x, _y = me:LocalToScreen(0, 0)

					surface.SetFont("SH_REPORTS.Large")
					local wi = surface.GetTextSize("★★★★★")

					surface.SetFont("SH_REPORTS.Medium")
					local wi2 = surface.GetTextSize(tx)

					local wid = wi + wi2

					draw.SimpleText("★★★★★", "SH_REPORTS.Large", w * 0.5 - wid * 0.5, h * 0.5, styl.inbg, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
					render.SetScissorRect(_x, _y, _x + w * 0.5 - wid * 0.5 + wi * frac, _y + h, true)
						draw.SimpleText("★★★★★", "SH_REPORTS.Large", w * 0.5 - wid * 0.5, h * 0.5, styl.rating, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
					render.SetScissorRect(0, 0, 0, 0, false)

					draw.SimpleText(tx, "SH_REPORTS.Medium", w * 0.5 - wid * 0.5 + wi, h * 0.5, styl.text, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
				end

				local line = ilist_rating:AddLine(user, stars)
				line:SetAlpha(0)
				line:AlphaTo(255, 0.1, 0.1 * i)
				self:LineStyle(line)

				i = i + 1
			end
		end

		local function display_perf(start, prep)
			if (curprep == start) then
				return end

			curprep = start
			frame.m_iID = prep.id

			ilist_perf:Clear()
			ilist_perf:SetVisible(true)
			ilist_rating:SetVisible(false)

			local ds, de = os.date(self.DateFormat, start), os.date(self.DateFormat, prep.end_time)

			frame.m_Title:SetText(L"performance_reports" .. /* 76561198013448859 */ " (" .. ds .. " - " .. de .. ")")
			frame.m_Title:SizeToContentsX()

			if (prep.staff) then
				frame:ShowStaff(prep.staff)
			else
				easynet.SendToServer("SH_REPORTS.RequestPerfReportStaff", {id = prep.id})
			end

			self:Notify(L("displaying_perf_report_from_x_to_y", ds, de), 5, styl.success, frame)
		end

		local btn = self:QuickButton("★ " .. L"rating", function()
			ilist_perf:SetVisible(false)
			ilist_rating:SetVisible(true)
			ilist_rating:Clear()

			easynet.SendToServer("SH_REPORTS.RequestStaffRatings")
		end, sel)
		btn:SetContentAlignment(4)
		btn:SetTextInset(m + 2, 0)
		btn:Dock(TOP)
		btn:SetTall(32 * ss)
		btn.m_iRound = 0
		btn.PaintOver = function(me, w, h)
			if (ilist_rating:IsVisible()) then
				surface.SetDrawColor(styl.header)
				surface.DrawRect(0, 0, 4, h)
			end
		end

		for _, prep in SortedPairs (self.CachedPerfReports, true) do
			local btn = self:QuickButton(os.date(self.DateFormat, prep.start_time), function()
				display_perf(prep.start_time, prep)
			end, sel, nil, prep.end_time >= (os.time() + delay) and styl.success or styl.text)
			btn:SetContentAlignment(4)
			btn:SetTextInset(m + 2, 0)
			btn:Dock(TOP)
			btn:SetTall(32 * ss)
			btn.m_iRound = 0
			btn.PaintOver = function(me, w, h)
				if (curprep == prep.start_time and ilist_perf:IsVisible()) then
					surface.SetDrawColor(styl.header)
					surface.DrawRect(0, 0, 4, h)
				end
			end
		end

	frame:SetAlpha(0)
	frame:AlphaTo(255, 0.1)
end

easynet.Callback("SH_REPORTS.SendPerfReports", function(data)
	SH_REPORTS.CachedPerfReports = data.struct_perf_reports
	SH_REPORTS:ShowPerformanceReports()
end)

easynet.Callback("SH_REPORTS.SendPerfReportStaff", function(data)
	if (!IsValid(_SH_REPORTS_PERF) or _SH_REPORTS_PERF.m_iID ~= data.id) then
		return end

	_SH_REPORTS_PERF:ShowStaff(data.struct_perf_reports_staff)
end)

easynet.Callback("SH_REPORTS.SendRatings", function(data)
	if (!IsValid(_SH_REPORTS_PERF)) then
		return end

	_SH_REPORTS_PERF:ShowRatings(data.struct_rating)
end)