-- All this stuff is jsut for the LERp of the hovering over button within the UI, just here so everyhting looks nice and clean

TitleStore.Animation = {}
TitleStore.Animation.Table = false


TitleStore.Animation.PanRight = function(self,w,h,speed,color)
	self.animationrightlerp = self.animationrightlerp or 0
	if self:IsHovered() then
		self.animationrightlerp = Lerp(speed, self.animationrightlerp, w)
	else
		self.animationrightlerp = Lerp(speed, self.animationrightlerp, 0)
	end
	draw.RoundedBox(0, 0, 0, self.animationrightlerp, h, color)
end


TitleStore.Animation.PanLeft = function(self,w,h,speed,color)
	self.animationleftlerp = self.animationleftlerp or w
	if self:IsHovered() then
		self.animationleftlerp = Lerp(speed, self.animationleftlerp, 0)
	else
		self.animationleftlerp = Lerp(speed, self.animationleftlerp, w)
	end
	draw.RoundedBox(0, self.animationleftlerp, 0, w - self.animationleftlerp, h, color)
end