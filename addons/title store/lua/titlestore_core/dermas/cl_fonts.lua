-- This just creates the font. To keep them organised

surface.CreateFont( "titlestore120", {
	font = TitleStore.Config.Font,
	size = 120,
	weight = 100
})
surface.CreateFont( "titlestore40", {
	font = TitleStore.Config.Font,
	size = 40,
	weight = 100
})
surface.CreateFont( "titlestore30", {
	font = TitleStore.Config.Font,
	size = 30,
	weight = 100
})

surface.CreateFont( "titlestore15", {
	font = TitleStore.Config.Font,
	size = 15,
	weight = 100
})