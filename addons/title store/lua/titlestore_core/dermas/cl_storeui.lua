include("cl_animation.lua")

-- The derma, nothing fancy here, jsut the basic stuff.
net.Receive("ts_derma", function()
    -- So that we can check if a player has selected as title/aniamtion
    local CurrentTitle = {price = 0}
    local CurrentAnim = {price = 0}

    local CustomTitle = "Custom Title"

    -- The core frame
    local frame = vgui.Create("DFrame")
    frame:SetSize(ScrW()*0.75, ScrH()*0.85)
    frame:SetTitle("")
    frame:SetVisible(true)
    frame:ShowCloseButton(false)
    frame:MakePopup()
    frame:Center()
    frame.Paint = function(self, w, h)
        TitleStore.Core.BlurFunc(frame, 3)
        draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
        draw.RoundedBox(0, 0, 0, w, 25, Color(40,40,40))

        draw.RoundedBox(0, 5,30, w/2, 40, Color(0, 0, 0, 180))
        draw.SimpleText("Titles", "titlestore40", w*0.25+10, 50, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
        
        draw.RoundedBox(0, w/2+10, 30, w/2-15, 40, Color(0, 0, 0, 180))
        draw.SimpleText("Animations", "titlestore40", w*0.75, 50, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
    end
 
    -- Custom close button
    local close = vgui.Create("DButton", frame)
    close:SetSize(25,25)
    close:SetPos(frame:GetWide()-25,0)
    close:SetText("")
    close.DoClick = function() frame:Close() end
    close.Paint = function(self, w, h)
        draw.RoundedBox(0, 0, 0, w, h, Color(100,0,0))
        draw.SimpleText("X", "titlestore30", w/2, h/2, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
    end

    -- The shell for the lists
    local listshell = vgui.Create("DPanel", frame)
    listshell:SetPos(5, 70)
    listshell:SetSize(frame:GetWide()-10, (frame:GetTall()*0.75)-80-40)
    listshell.Paint = function(self,w,h) end

    --Title Code
    local titleshell = vgui.Create( "DPanel", listshell)
    titleshell:SetPos(0,0)
    titleshell:SetSize(listshell:GetWide()/2+5, listshell:GetTall())
    titleshell.Paint = function()  end
 
    -- Creating and adding to the title list.
    local titlelist = vgui.Create( "DPanelList", titleshell)
    titlelist:SetPos(0,0)
    titlelist:SetSize(titleshell:GetWide()+25, titleshell:GetTall())
    titlelist.Paint = function() end
    titlelist:SetSpacing(3)
    titlelist:EnableHorizontal(false)
    titlelist:EnableVerticalScrollbar(false)

    for k, v in ipairs(TitleStore.Config.Titles) do
        local titlepan = vgui.Create("DButton", titlelist)
        titlepan:SetSize(titlelist:GetWide()-13, 80)
        titlepan:SetText("")

        titlepan.Paint = function(self, w, h)

            if LocalPlayer():getDarkRPVar("money") < v.price then
                draw.RoundedBox(0, 0, 0, w, h, TitleStore.Config.NoAccess)
            elseif v.customCheck ~= nil then
                if !(v.customCheck(LocalPlayer())) then
                    draw.RoundedBox(0, 0, 0, w, h, TitleStore.Config.NoAccess)
                else
                    if CurrentTitle.name == v.name then
                        draw.RoundedBox(0, 0, 0, w, h, TitleStore.Config.ListColor)
                    else
                        draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
                        TitleStore.Animation.PanRight(self,w,h,0.04,TitleStore.Config.ListColor)
                    end
                end
            else
                if CurrentTitle.name == v.name then
                    draw.RoundedBox(0, 0, 0, w, h, TitleStore.Config.ListColor)
                else
                    draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
                    TitleStore.Animation.PanRight(self,w,h,0.04,TitleStore.Config.ListColor)
                end
            end

            draw.SimpleText(v.name, "titlestore40", w/2, h/2, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM )
            if LocalPlayer():GetNWString("TitleTitle") == v.name then 
                draw.SimpleText("$0", "titlestore40", w/2, h/2, Color(100, 200, 100), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
            else
                draw.SimpleText("$"..string.Comma(v.price), "titlestore40", w/2, h/2, Color(100, 200, 100), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
            end
        end
        titlepan.DoClick = function()
            CurrentTitle.name = v.name
            CurrentTitle.price = v.price
        end
        
        titlelist:AddItem(titlepan)
    end

    if TitleStore.Config.CustomTags then
        local titlepan = vgui.Create("DButton", titlelist)
        titlepan:SetSize(titlelist:GetWide()-13, 80)
        titlepan:SetText("")
        
        titlepan.DoClick = function()
            local cframe = vgui.Create("DFrame")
            cframe:SetSize(400, 60)
            cframe:SetTitle("")
            cframe:SetVisible(true)
            cframe:MakePopup()
            cframe:Center()
            cframe.Paint = function(self, w, h)
                TitleStore.Core.BlurFunc(cframe, 3)
                draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
                draw.RoundedBox(0, 0, 0, w, 25, Color(40,40,40))
            end
            local entry = vgui.Create("DTextEntry", cframe)
            entry:SetPos(5,30)
            entry:SetSize(cframe:GetWide()-10, 25)
            entry:SetText( "i love cats" )

            entry.OnEnter = function( self )
                CustomTitle = self:GetText()
                CurrentTitle.name = CustomTitle
                CurrentTitle.price = TitleStore.Config.CustomPrice
                cframe:Close()
            end

        end

        titlepan.Paint = function(self, w, h)

            if LocalPlayer():getDarkRPVar("money") < TitleStore.Config.CustomPrice then
                draw.RoundedBox(0, 0, 0, w, h, TitleStore.Config.NoAccess)
            else
                if CurrentTitle.name == CustomTitle then
                    draw.RoundedBox(0, 0, 0, w, h, TitleStore.Config.ListColor)
                else
                    draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
                    TitleStore.Animation.PanRight(self,w,h,0.04,TitleStore.Config.ListColor)
                end
            end

            draw.SimpleText(CustomTitle.." (Custom)", "titlestore40", w/2, h/2, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM )

            if LocalPlayer():GetNWString("TitleTitle") == CustomTitle then 
                draw.SimpleText("$0", "titlestore40", w/2, h/2, Color(100, 200, 100), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
            else
                draw.SimpleText("$"..string.Comma(TitleStore.Config.CustomPrice), "titlestore40", w/2, h/2, Color(100, 200, 100), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
            end
        end
        
        titlelist:AddItem(titlepan)
    end


    --Animation Code
    local animationshell = vgui.Create( "DPanel", listshell)
    animationshell:SetPos(listshell:GetWide()/2+10,0)
    animationshell:SetSize(listshell:GetWide()/2+5, listshell:GetTall())
    animationshell.Paint = function()  end
 
    -- Creating and adding to the animation list.
    local animationlist = vgui.Create( "DPanelList", animationshell)
    animationlist:SetPos(0,0)
    animationlist:SetSize(animationshell:GetWide(), animationshell:GetTall())
    animationlist.Paint = function() end
    animationlist:SetSpacing(3)
    animationlist:EnableHorizontal(false)
    animationlist:EnableVerticalScrollbar(false)

    for k, v in ipairs(TitleStore.Config.Animations) do
    

        local animationpan = vgui.Create("DButton", animationlist)
        animationpan:SetSize(animationlist:GetWide()-13, 80)
        animationpan:SetText("")
        animationpan.Paint = function(self, w, h)
            
            if LocalPlayer():getDarkRPVar("money") < v.price then
                draw.RoundedBox(0, 0, 0, w, h, TitleStore.Config.NoAccess)
            elseif v.customCheck ~= nil then
                if !(v.customCheck(LocalPlayer())) then
                    draw.RoundedBox(0, 0, 0, w, h, TitleStore.Config.NoAccess)
                else
                    if CurrentAnim.name == v.name then
                        draw.RoundedBox(0, 0, 0, w, h, TitleStore.Config.ListColor)
                    else
                        draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
                        TitleStore.Animation.PanLeft(self,w,h,0.04,TitleStore.Config.ListColor)
                    end
                end
            else
                if CurrentAnim.name == v.name then
                    draw.RoundedBox(0, 0, 0, w, h, TitleStore.Config.ListColor)
                else
                    draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
                    TitleStore.Animation.PanLeft(self,w,h,0.04,TitleStore.Config.ListColor)
                end
            end

            draw.SimpleText(v.name, "titlestore40", w/2, h/2, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM )
            if LocalPlayer():GetNWString("TitleAnimation") == v.name then 
                draw.SimpleText("$0", "titlestore40", w/2, h/2, Color(100, 200, 100), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
            else
                draw.SimpleText("$"..string.Comma(v.price), "titlestore40", w/2, h/2, Color(100, 200, 100), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
            end

        end

        animationpan.DoClick = function()
            CurrentAnim.name = v.name
            CurrentAnim.price = v.price
        end

        animationlist:AddItem(animationpan)
    end
 


    -- For the color and buy panels
    local extrashell = vgui.Create("DPanel", frame)
    extrashell:SetPos(5, (frame:GetTall()*0.75)-45)
    extrashell:SetSize(frame:GetWide()-10, (frame:GetTall()*0.25)+40)
    extrashell.Paint = function(self, w, h)
        draw.RoundedBox(0, 0, 0, w/2+5, 40, Color(0, 0, 0, 180))
        draw.SimpleText("Color", "titlestore40", w*0.25, 20, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

        draw.RoundedBox(0, w/2+10, 0, w/2+5, 40, Color(0, 0, 0, 180))
        draw.SimpleText("Buy", "titlestore40", w*0.75, 20, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
    end



    -- A shell for the color mixer
    local titlecolorshell = vgui.Create("DPanel", extrashell)
    titlecolorshell:SetPos(0,40)
    titlecolorshell:SetSize(extrashell:GetWide()/2+5, extrashell:GetTall()-40)
    titlecolorshell.Paint = function(self, w, h)
        draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
    end
 
    -- The color mixer
   local Mixer = vgui.Create( "DColorMixer", titlecolorshell )
   Mixer:DockMargin( 10, 10, 10, 10 )
   Mixer:Dock( FILL )        
   Mixer:SetPalette(false)   
   Mixer:SetAlphaBar( false ) 
   Mixer:SetWangs( true )    
   Mixer:SetColor( Color( 30, 100, 160 ) ) 



   -- The shel for the buy panel
    local buyshell = vgui.Create("DPanel", extrashell)
    buyshell:SetPos(extrashell:GetWide()/2+10, 40)
    buyshell:SetSize(extrashell:GetWide()/2-10, extrashell:GetTall()-40)
    buyshell.Paint = function(self,w,h)
        draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
        if LocalPlayer():GetNWString("TitleTitle") == CurrentTitle.name then CurrentTitle.price = 0 end
        if LocalPlayer():GetNWString("TitleAnimation") == CurrentAnim.name then CurrentAnim.price = 0 end
        draw.SimpleText("$"..string.Comma(CurrentTitle.price+CurrentAnim.price), "titlestore40", w*0.5, self:GetTall()*0.2, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
    end

    -- the buy button
    local buybutton = vgui.Create("DButton", buyshell)
    buybutton:SetPos(0,buyshell:GetTall()*0.4)
    buybutton:SetSize(buyshell:GetWide(), buyshell:GetTall()*0.6)
    buybutton:SetText("")
    buybutton.Paint = function(self, w, h)
        draw.RoundedBox(4, 0, 0, w, h, Color(40, 120, 40, 255))
        draw.SimpleText("Buy", "titlestore40", w/2, h/2, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
    end
    -- Sending the data to the server
    buybutton.DoClick = function()
        if CurrentTitle.name == nil then TitleStore.Core.ChatNotify("Please choose a title") return end
        if CurrentAnim.name == nil then TitleStore.Core.ChatNotify("Please choose an animation") return end
        net.Start("ts_buytitle")
            net.WriteTable(CurrentTitle)
            net.WriteTable(CurrentAnim)
            net.WriteTable(Mixer:GetColor())
        net.SendToServer()
        frame:Close()
    end

end)
