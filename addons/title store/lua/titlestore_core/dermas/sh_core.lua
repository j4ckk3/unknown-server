TitleStore.Core = {}

if CLIENT then
	-- These are the chat notifications, the system used is the most optimised way I could think of
	net.Receive( "ts_msg", function()
		chat.AddText( TitleStore.Config.PrefixColor, TitleStore.Config.Prefix..": ", Color( 255, 255, 255 ), net.ReadString() )
	end )

	function TitleStore.Core.ChatNotify(a)
		chat.AddText( TitleStore.Config.PrefixColor, TitleStore.Config.Prefix..": ", Color( 255, 255, 255 ), a )
	end

	-- The function for blur panels
	local blur = Material("pp/blurscreen")
	TitleStore.Core.BlurFunc = function(panel, amount)
		local x, y = panel:LocalToScreen(0, 0)
		local scrW, scrH = ScrW(), ScrH()
		surface.SetDrawColor(255, 255, 255)
		surface.SetMaterial(blur)
		for i = 1, 6 do
			blur:SetFloat("$blur", (i / 3) * (amount or 6))
			blur:Recompute()
			render.UpdateScreenEffectTexture()
			surface.DrawTexturedRect(x * -1, y * -1, scrW, scrH)
		end
	end

end