TitleStore.Animations = {}


-- Only edit this if you have advance knowledge of Lua and math.
-- Ensure the number and name match the number and name in the sh_config.lua else it will not register.

TitleStore.Animations.Table = {
	[1] = {		name = "None",	func = function(ply, text, color)
    	local offset = Vector(0, 0, 19+TitleStore.Config.Height)
    	local ang = LocalPlayer():EyeAngles()
    	local pos
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
		ang:RotateAroundAxis( ang:Forward(), 90 )
		ang:RotateAroundAxis( ang:Right(), 90 )
		cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
			draw.DrawText(text,"titlestore120",0, 200, color, TEXT_ALIGN_CENTER)
		cam.End3D2D()
	end},

	[2] = {		name = "Bouncing",	func = function(ply, text, color)
    	local offset = Vector( 0, 0, 19+TitleStore.Config.Height)
    	local ang = LocalPlayer():EyeAngles()
		local pos
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end
        ang:RotateAroundAxis( ang:Forward(), 90 )
		ang:RotateAroundAxis( ang:Right(), 90 )
		
		if animationlerp == nil then animationlerp = 1 end
		if animationlerpto == nil then animationlerpto = 250 end
		cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
		    -- Math.Approaching the animation
		    if animationlerp == 250 then
		        animationlerpto = 1
		    elseif animationlerp == 1 then
		        animationlerpto = 250
		    end
		    animationlerp = math.Approach(animationlerp,animationlerpto,FrameTime()*animationlerp*math.pi)
		    draw.DrawText(text,"titlestore120",0,animationlerp,color,TEXT_ALIGN_CENTER)
		cam.End3D2D()
	end},

	[3] = {		name = "Spin",	func = function(ply, text, color)
    	local offset = Vector( 0, 0, 19+TitleStore.Config.Height)
    	local ang = LocalPlayer():EyeAngles()
		local pos
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 		ang:RotateAroundAxis( ang:Forward(), 90 )
		ang:RotateAroundAxis( ang:Right(), (CurTime()*40)%360 )
		
		cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
			draw.DrawText(text, "titlestore120", 0, 200, color, TEXT_ALIGN_CENTER)
		cam.End3D2D()
		cam.Start3D2D( pos, Angle( 0, ang.y+180, 90 ), 0.03 )
			draw.DrawText(text, "titlestore120", 0, 200, color, TEXT_ALIGN_CENTER)
		cam.End3D2D()
	end},

	[4] = {		name = "Wave",	func = function(ply, text, color)
    	local offset = Vector( 0, 0, 19+TitleStore.Config.Height)
    	local ang = LocalPlayer():EyeAngles()
		local pos
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
		ang:RotateAroundAxis( ang:Forward(), 90 )
		ang:RotateAroundAxis( ang:Right(), 90 )
	
		cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
    		local textexp = string.Explode("", text)
    		surface.SetFont("titlestore120")
    		local chars_x = 0
    		local x, y = 0, 200
    		for i = 1, #textexp do
    		    local char = textexp[i]
    		    local char_w, char_h = surface.GetTextSize(char)
    		    local text_w, text_h = surface.GetTextSize(text)
    		    local y_pos = 1
    		    local mod = math.sin((RealTime() - (i * 0.1)) * 6)
    		    y_pos = y_pos - mod
    		    draw.SimpleText(char, "titlestore120", (x + chars_x)-(text_w/2), y - (5 * y_pos), color, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
    		    chars_x = chars_x + char_w
    		end
		cam.End3D2D()
	end},

	[5] = {		name = "Wiggle",	func = function(ply, text, color)
    	local offset = Vector( 0, 0, 19+TitleStore.Config.Height)
    	local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
		ang:RotateAroundAxis( ang:Forward(), 90 )
		ang:RotateAroundAxis( ang:Right(), 90 )
	
		cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
   	 		local x_pos = 0
   	 		local sin = math.sin(RealTime()*5)
   	 		x_pos = x_pos - sin
			draw.DrawText(text,"titlestore120",(50*x_pos), 200, color, TEXT_ALIGN_CENTER)
		cam.End3D2D()
	end},

	[6] = {		name = "TypeWriter",	func = function(ply, text, color)
    	local offset = Vector( 0, 0, 19+TitleStore.Config.Height)
    	local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
		ang:RotateAroundAxis( ang:Forward(), 90 )
		ang:RotateAroundAxis( ang:Right(), 90 )
	
		cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
    		local textexp = string.Explode("", text)
    		surface.SetFont("titlestore120")
    		local chars_x = 0
    		local x, y = 0, 200
    		for i = 1, #textexp do
    		    local char = textexp[i]
    		    local char_w, char_h = surface.GetTextSize(char)
    		    local text_w, text_h = surface.GetTextSize(text)
    		    local mod = (RealTime()%#textexp)*1.5
				if math.Round(mod) > i then
    		   		draw.SimpleText(char, "titlestore120", (x + chars_x)-(text_w/2), 200, color, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
    		    end
    		    chars_x = chars_x + char_w
    		end
		cam.End3D2D()
	end},

	[7] = {		name = "Rotate Left-Right",	func = function(ply, text, color)
    	local offset = Vector( 0, 0, 19+TitleStore.Config.Height )
    	local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
		ang:RotateAroundAxis( ang:Forward(), 90 )
		ang:RotateAroundAxis( ang:Right(), 90 )
	
		cam.Start3D2D( pos, Angle( math.sin(RealTime()*5), ang.y, 90 ), 0.03 )
			draw.DrawText(text, "titlestore120", 0, 200, color, TEXT_ALIGN_CENTER)
		cam.End3D2D()
	end},

	[8] = {		name = "Glitch",	func = function(ply, text, color)
		local offset = Vector(0,0,19+TitleStore.Config.Height)
		local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
		ang:RotateAroundAxis( ang:Forward(), 90 )
		ang:RotateAroundAxis( ang:Right(), 90 )
		local glitchletters = {"!", "#", "%", "&", "*", "?", }
	
		cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
    		local textexp = string.Explode("", text)
    		surface.SetFont("titlestore120")
    		local chars_x = 0
    		local x, y = 0, 200
    		for i = 1, #textexp do
    		    local char = textexp[i]
    		    local char_w, char_h = surface.GetTextSize(char)
    		    local text_w, text_h = surface.GetTextSize(text)
    		    local ran = math.random(1,100)
    		    if ran < 5 then
    		   		draw.SimpleText(table.Random(glitchletters), "titlestore120", (x + chars_x)-(text_w/2), 200, color, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
    		   	else
    		   		draw.SimpleText(char, "titlestore120", (x + chars_x)-(text_w/2), 200, color, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
    		   	end
    		    chars_x = chars_x + char_w
    		end
		cam.End3D2D()
	end},

	[9] = {		name = "Spinning Bounce",	func = function(ply,text, color)
    	local offset = Vector(0,0,19+TitleStore.Config.Height)
    	local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
	    ang:RotateAroundAxis( ang:Forward(), 90 )
	    ang:RotateAroundAxis( ang:Right(), (CurTime()*40)%360 )
	 
	    if animationlerp == nil then animationlerp = 1 end
	    if animationlerpto == nil then animationlerpto = 250 end
	    cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
	        -- Math.Approaching the animation
	        if animationlerp == 250 then
	            animationlerpto = 1
	        elseif animationlerp == 1 then
	            animationlerpto = 250
	        end
	        animationlerp = math.Approach(animationlerp,animationlerpto,FrameTime()*animationlerp*math.pi)
	        draw.DrawText(text,"titlestore120",0,animationlerp,color,TEXT_ALIGN_CENTER)
	    cam.End3D2D()
	   
	    cam.Start3D2D( pos, Angle( 0, ang.y+180, 90 ), 0.03 )
	        -- Math.Approaching the animation
	        if animationlerp == 250 then
	            animationlerpto = 1
	        elseif animationlerp == 1 then
	            animationlerpto = 250
	        end
	        animationlerp = math.Approach(animationlerp,animationlerpto,FrameTime()*animationlerp*math.pi)
	        draw.DrawText(text,"titlestore120",0,animationlerp,color,TEXT_ALIGN_CENTER)
	    cam.End3D2D()
	end},

	[10] = {		name = "Spinning Wave",	func = function(ply,text, color)
    	local offset = Vector(0,0,19+TitleStore.Config.Height)
    	local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
    	ang:RotateAroundAxis( ang:Forward(), 90 )
    	ang:RotateAroundAxis( ang:Right(), (CurTime()*40)%360 )
 	
    	cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
    	    local texte = string.Explode("", text)
    	    surface.SetFont("titlestore120")
    	    local chars_x = 0
    	    local x, y = 0, 200
 	
    	    for i = 1, #texte do
    	        local char = texte[i]
    	        local char_w, char_h = surface.GetTextSize(char)
    	        local text_w, text_h = surface.GetTextSize(text)
    	        local y_pos = 1
    	        local sin = math.sin((RealTime() - (i * 0.1)) * 6)
 	
    	        y_pos = y_pos - sin
   	
    	        draw.SimpleText(char, "titlestore120", (x + chars_x)-(text_w/2), y - (5 * y_pos), color, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
    	        chars_x = chars_x + char_w
    	    end
    	cam.End3D2D()
   	
    	cam.Start3D2D( pos, Angle( 0, ang.y+180, 90 ), 0.03 )
    	    local texte = string.Explode("", text)
    	    surface.SetFont("titlestore120")
    	    local chars_x = 0
    	    local x, y = 0, 200
 	
    	    for i = 1, #texte do
    	        local char = texte[i]
    	        local charw, charh = surface.GetTextSize(char)
    	        local textw, texth = surface.GetTextSize(text)
    	        local y_pos = 1
    	        local sin = math.sin((RealTime() - (i * 0.1)) * 6)
 	
    	        y_pos = y_pos - sin
   	
    	        draw.SimpleText(char, "titlestore120", (x + chars_x)-(textw/2), y - (5 * y_pos), color, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
    	        chars_x = chars_x + charw
    	    end
    	cam.End3D2D()
	end},

	[11] = {		name = "Spinning Glitch",	func = function(ply,text,color)
	    local offset = Vector(0,0,19+TitleStore.Config.Height)
	    local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
	    ang:RotateAroundAxis( ang:Forward(), 90 )
	    ang:RotateAroundAxis( ang:Right(), (CurTime()*40)%360 )
	 
	    local glitchletters = {"!", "#", "%", "&", "*", "?", }
	 
	    cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
	        local textexp = string.Explode("", text)
	        surface.SetFont("titlestore120")
	        local chars_x = 0
	        local x, y = 0, 200
	 
	        for i = 1, #textexp do
	            local char = textexp[i]
	            local char_w, char_h = surface.GetTextSize(char)
	            local text_w, text_h = surface.GetTextSize(text)
	            local ran = math.random(1,100)
	            if ran < 5 then
	                draw.SimpleText(table.Random(glitchletters), "titlestore120", (x + chars_x)-(text_w/2), 200, color, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
	            else
	                draw.SimpleText(char, "titlestore120", (x + chars_x)-(text_w/2), 200, color, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
	            end
	            chars_x = chars_x + char_w
	        end
	    cam.End3D2D()
	 
	    cam.Start3D2D( pos, Angle( 0, ang.y+180, 90 ), 0.03 )
	        local texte = string.Explode("", text)
	        surface.SetFont("titlestore120")
	        local chars_x = 0
	        local x, y = 0, 200
	 
	        for i = 1, #texte do
	            local char = texte[i]
	            local char_w, char_h = surface.GetTextSize(char)
	            local text_w, text_h = surface.GetTextSize(text)
	            local ran = math.random(1,100)
	            if ran < 5 then
	                draw.SimpleText(table.Random(glitchletters), "titlestore120", (x + chars_x)-(text_w/2), 200, color, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
	            else
	                draw.SimpleText(char, "titlestore120", (x + chars_x)-(text_w/2), 200, color, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
	            end
	            chars_x = chars_x + char_w
	        end
	    cam.End3D2D()
	end},

	[12] = {		name = "Spinning Left-Right",	func = function(ply,text,color)
	    local offset = Vector(0,0,19+TitleStore.Config.Height)
	    local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
	    ang:RotateAroundAxis( ang:Forward(), 90 )
	    ang:RotateAroundAxis( ang:Right(), (CurTime()*40)%360 )
	 
	    cam.Start3D2D( pos, Angle( math.sin(RealTime()*5), ang.y, 90 ), 0.03 )
	        draw.DrawText(text,"titlestore120",0,200,color,TEXT_ALIGN_CENTER)
	    cam.End3D2D()
	   
	    cam.Start3D2D( pos, Angle( math.sin(RealTime()*5), ang.y+180, 90 ), 0.03 )
	        draw.DrawText(text,"titlestore120",0,200,color,TEXT_ALIGN_CENTER)
	    cam.End3D2D()
	end},
	
    [13] = {        name = "Rainbow",   func = function(ply,text,color)
        local offset = Vector(0,0,19+TitleStore.Config.Height)
        local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
        ang:RotateAroundAxis( ang:Forward(), 90 )
        ang:RotateAroundAxis( ang:Right(), 90 )
 
 
        cam.Start3D2D( pos,Angle(0,ang.y,90), 0.03 )
        local texte = string.Explode("", text)
            surface.SetFont("titlestore120")
            local chars_x = 0
            local x, y = 0, 200
            for i = 1, #texte do
 
                local col = HSVToColor( i*(math.sin((RealTime() - ((i+1) * 0.1)) * 6)*8) % 360, 1, 1 )
                local char = texte[i]
                local textw, texth = surface.GetTextSize(text)
                local charw, charh = surface.GetTextSize(char)
                draw.SimpleText(char, "titlestore120", (x + chars_x)-(textw/2), 200, col, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
                chars_x = chars_x + charw
            end
 
        cam.End3D2D()
    end},
   
    [14] = {        name = "Spinning Rainbow",  func = function(ply,text,color)
        local offset = Vector(0,0,19+TitleStore.Config.Height)
        local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
        ang:RotateAroundAxis( ang:Forward(), 90 )
        ang:RotateAroundAxis( ang:Right(), (CurTime()*40)%360 )
 
 
        cam.Start3D2D( pos,Angle(0,ang.y,90), 0.03 )
        local texte = string.Explode("", text)
            surface.SetFont("titlestore120")
            local chars_x = 0
            local x, y = 0, 200
            for i = 1, #texte do
 
                local col = HSVToColor( i*(math.sin((RealTime() - ((i+1) * 0.1)) * 6)*8) % 360, 1, 1 )
                local char = texte[i]
                local textw, texth = surface.GetTextSize(text)
                local charw, charh = surface.GetTextSize(char)
                draw.SimpleText(char, "titlestore120", (x + chars_x)-(textw/2), 200, col, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
                chars_x = chars_x + charw
            end
        cam.End3D2D()
       
        cam.Start3D2D( pos,Angle(0,ang.y+180,90), 0.03 )
        local texte = string.Explode("", text)
            surface.SetFont("titlestore120")
            local chars_x = 0
            local x, y = 0, 200
            for i = 1, #texte do
 
                local col = HSVToColor( i*(math.sin((RealTime() - ((i+1) * 0.1)) * 6)*15) % 360, 1, 1 )
                local char = texte[i]
                local textw, texth = surface.GetTextSize(text)
                local charw, charh = surface.GetTextSize(char)
                draw.SimpleText(char, "titlestore120", (x + chars_x)-(textw/2), 200, col, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
                chars_x = chars_x + charw
            end
        cam.End3D2D()
    end},
	
    [15] = {        name = "Bouncing Rainbow",  func = function(ply,text,color)
    	local offset = Vector( 0, 0, 19+TitleStore.Config.Height)
    	local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
		ang:RotateAroundAxis( ang:Forward(), 90 )
		ang:RotateAroundAxis( ang:Right(), 90 )
		
		if animationlerp == nil then animationlerp = 1 end
		if animationlerpto == nil then animationlerpto = 250 end
		cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
		    -- Math.Approaching the animation
		    if animationlerp == 250 then
		        animationlerpto = 1
		    elseif animationlerp == 1 then
		        animationlerpto = 250
		    end	
		    animationlerp = math.Approach(animationlerp,animationlerpto,FrameTime()*animationlerp*math.pi)
			local texte = string.Explode("", text)
            surface.SetFont("titlestore120")
            local chars_x = 0
            local x, y = 0, 200
            for i = 1, #texte do
                local col = HSVToColor( i*(math.sin((RealTime() - ((i+1) * 0.1)) * 6)*15) % 360, 1, 1 )
                local char = texte[i]
                local textw, texth = surface.GetTextSize(text)
                local charw, charh = surface.GetTextSize(char)
                draw.SimpleText(char, "titlestore120", (x + chars_x)-(textw/2), animationlerp, col, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
                chars_x = chars_x + charw
            end
		cam.End3D2D()
    end},

    [16] = {        name = "Wavey Rainbow",  func = function(ply,text,color)
    	local offset = Vector( 0, 0, 19+TitleStore.Config.Height)
    	local ang = LocalPlayer():EyeAngles()
    	if ply:LookupBone("ValveBiped.Bip01_Head1") != nil then
    		pos = ply:GetBonePosition(ply:LookupBone("ValveBiped.Bip01_Head1")) + offset 
    	else 
    		pos = ply:GetPos()+ Vector(0,0,70+TitleStore.Config.Height)
    	end 
		ang:RotateAroundAxis( ang:Forward(), 90 )
		ang:RotateAroundAxis( ang:Right(), 90 )
	
		cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.03 )
    		local texte = string.Explode("", text)
    		surface.SetFont("titlestore120")
    		local chars_x = 0
    		local x, y = 0, 200
    		for i = 1, #texte do
                local col = HSVToColor( i*(math.sin((RealTime() - ((i+1) * 0.1)) * 6)*15) % 360, 1, 1 )
    		    local char = texte[i]
    		    local char_w, char_h = surface.GetTextSize(char)
    		    local text_w, text_h = surface.GetTextSize(text)
    		    local y_pos = 1
    		    local mod = math.sin((RealTime() - (i * 0.1)) * 6)
    		    y_pos = y_pos - mod
    		    draw.SimpleText(char, "titlestore120", (x + chars_x)-(text_w/2), y - (5 * y_pos), col, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
    		    chars_x = chars_x + char_w
    		end
		cam.End3D2D()
	end}
}