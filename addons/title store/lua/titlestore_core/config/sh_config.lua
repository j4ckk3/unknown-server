TitleStore.Config = {}

-- If you run into any issues and the addon throws errors at you, contact me through a support ticket, or add me on steam.

/*
====================================================
This first section is for the core side of the addon
====================================================
*/

-- The font used throughout the addon
TitleStore.Config.Font = "Calibri"

-- Notification color
TitleStore.Config.PrefixColor = Color(255, 255, 0)

-- Chat prefix
TitleStore.Config.Prefix = "[Title Store]"

-- The NPC text
TitleStore.Config.NPCText = "Title Store"

-- NPC model
TitleStore.Config.NPCModel = "models/Humans/Group01/male_07.mdl"

-- The chat command to open the admin menuS
TitleStore.Config.AdminMenu = "!titleadmin"

-- The ulx ranks that have access to the admin menu
TitleStore.Config.AdminMenuRanks = {
	["root"] = true,
	["cmanager"] = true

}

-- Titles list color, this is the color of the animated bar that comes out when you hover over a title in the UI
TitleStore.Config.ListColor = Color(0,0,100,155)

-- Animation list color, this is the color of the animated bar that comes out when you hover over an animation in the UI
TitleStore.Config.NoAccess = Color(255,0,0,100)

-- Animation height, this is how high the animations show above the players head.
TitleStore.Config.Height = 1

-- This is the height of the animation shows above models that may not have a head bone, for example models/Zombie/Classic_legs.mdl
TitleStore.Config.NoHeadHeight = 1

-- Should custom tags be buyable?
TitleStore.Config.CustomTags = true

-- If above si ture, the price for a custom title
TitleStore.Config.CustomPrice = 1000000

-- Waht should the character limit be for custom titles?
TitleStore.Config.CustomLimit = 25

-- The titles the user can choose.
-- name = The name of the title (What is shoqwn in both the store and above the players head)
-- price = How much it costs
-- customceck = allows you to restric the titles to specific things, e.g ulx groups
TitleStore.Config.Titles = {
	[1] = {		name = "God",				price = 1000000		},
	[2] = {		name = "The Boss",			price = 5500		},
	[3] = {		name = "Big Server Man",	price = 1000,		},
	[4] = {		name = "Pimp",				price = 45000		},
	[5] = {		name = "Legend",			price = 15000		},
	[6] = {		name = "Owner",				price = 1000000,	},
	[7] = {		name = ":D",				price = 15000		},
	[8] = {		name = "Wannabe Owner",		price = 5000		},
	[9] = {		name = "Rich boys club",	price = 10000000	},
	[10] = {	name = "Boi",				price = 15000,		},
	[11] = {	name = "Cat",				price = 200000,		},
	[12] = {	name = "Dab",				price = 1000000		},
	[13] = {	name = "Squad",				price = 145000		},
	[14] = {	name = "Gangster",			price = 10000		},
	[15] = {	name = "Abuser",			price = 40000		},
	[16] = {	name = "Hackerman",			price = 35000		},	
	[17] = {	name = "Pro Coder",			price = 750000		},
	[18] = {	name = "Poor boys club",	price = 500			}
}		

-- Animations, adding new animations requires a good knowledge of both Lua and math. It's suggested
-- to leave the animations as is. However, if you want to add more animations, you will need to add
-- them to both this file and the "sh_animations.lua" file. Make sure they both have the same name and number.

-- If yon don't edit the animations, KEEP THE NAMES AS IS!! If you want to change the names, open a support ticket.
-- However, you can change the price here just fine. 
TitleStore.Config.Animations = {
	[1] = {		name = "None",					price = 0		},
	[2] = {		name = "Bouncing",				price = 50000	},
	[3] = {		name = "Spin",					price = 60000,		customCheck = function(ply) return ply:IsAdmin() end },
	[4] = {		name = "Wave",					price = 45000	},
	[5] = {		name = "Wiggle",				price = 15000	},
	[6] = {		name = "TypeWriter",			price = 50000	},
	[7] = {		name = "Rotate Left-Right",		price = 10000	},
	[8] = {		name = "Glitch",				price = 60000	},
	[9] = {		name = "Spinning Bounce",		price = 75000,		customCheck = function(ply) return ply:IsAdmin() end },
	[10] = {	name = "Spinning Wave",			price = 55000	},
	[11] = {	name = "Spinning Glitch",		price = 90000	},
	[12] = {	name = "Spinning Left-Right",	price = 47000	},
	[13] = {    name = "Rainbow",				price = 50000,   	customCheck = function(ply) return kShop.PlayerHasUpgrade(ply, "darkrp-rainbow_titles") end },
    [14] = {    name = "Spinning Rainbow",		price = 100000, 	customCheck = function(ply) return kShop.PlayerHasUpgrade(ply, "darkrp-rainbow_titles") end },
    [15] = {    name = "Bouncing Rainbow",		price = 100000,  	customCheck = function(ply) return kShop.PlayerHasUpgrade(ply, "darkrp-rainbow_titles") end },
	[16] = {	name = "Wavey Rainbow",			price = 100000,  	customCheck = function(ply) return kShop.PlayerHasUpgrade(ply, "darkrp-rainbow_titles") end },
}
