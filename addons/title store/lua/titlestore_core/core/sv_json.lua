-- Some JSON read/write stuff (A mini lib if you will)

-- A table for organisation
TitleStore.JSON = {}

-- So that it can easily apply stuff to a specific user
local meta = FindMetaTable( "Player" )

-- Writes the table
function TitleStore.JSON.WriteData(a)
	local TitleData = a
	file.Write("title_store/"..TitleData.id..".txt", util.TableToJSON(TitleData))
	print("Saved Title User data")
end

-- Sets the players info in NetWork strings and vectors
function meta:TitleStore_JSON_SetTitle()
	-- Gets the players SteamID64 as that's the id for the core system.
	local TarID64 = self:SteamID64()

	-- Checks through the table to see if they player actually has a title/animation
	if file.Exists( "title_store/"..TarID64..".txt", "DATA" ) then
		local TABLE = util.JSONToTable(file.Read("title_store/"..TarID64..".txt", "DATA"))
		self:SetNWString("TitleTitle", TABLE.title)
		self:SetNWString("TitleAnimation", TABLE.animation)
		self:SetNWVector("TitleColor", Vector(TABLE.color.r, TABLE.color.g, TABLE.color.b) )
	else
	-- If they don't have a title/animation, it sets it to nothing
		self:SetNWString("TitleTitle", "")
		self:SetNWString("TitleAnimation", "")
		self:SetNWVector("TitleColor", Vector(0, 0, 0) )
	end

end


-- Adds data to the table

-- Table format:
-- local writetable = {id = SteamID64, title = "dasdas", animation = "dsa", color = Color(255,255,255)}
function meta:TitleStore_JSON_InsertData(a)
	local TABLE = {}
	local TitleData = a
	local ply = self

	TitleStore.JSON.WriteData(TitleData)
	ply:TitleStore_JSON_SetTitle()
end

-- Removes a user from the table
function meta:TitleStore_JSON_RemoveData()
	local TarID64 = self:SteamID64()
	if file.Exists( "title_store/"..TarID64..".txt", "DATA" ) then
		file.Delete( "title_store/"..TarID64..".txt" )
	end
	self:TitleStore_JSON_SetTitle()
end

-- Removes a user from the table
function TitleStore.JSON.ReadID(a)
	local TarID64 = a

	if file.Exists( "title_store/"..TarID64..".txt", "DATA" ) then
		return util.JSONToTable(file.Read("title_store/"..TarID64..".txt", "DATA"))
	else
		return {title = "", animation = "This user has no title!", color = Color(0,0,0,0)}
	end

end

hook.Add( "PlayerInitialSpawn", "TitleStore_LoadPlayer", function( ply )
	ply:TitleStore_JSON_SetTitle()
end)
