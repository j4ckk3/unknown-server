-- Presents the animation/title above the players head!
hook.Add("PostPlayerDraw", "DrawTitle", function(ply)
	-- Some basic checks
	if ( !IsValid( ply ) ) then return end
	if ( !ply:Alive() ) then return end
	if LocalPlayer():GetPos():Distance( ply:GetPos() ) > 750 then return end

	local PlyColor = ply:GetNWString("TitleColor")

	-- Chceks for the animation
	for k, v in pairs(TitleStore.Animations.Table) do
		if ply:GetNWString("TitleAnimation") == v.name then
			v.func(ply, ply:GetNWString("TitleTitle"), Color(PlyColor.x,PlyColor.y,PlyColor.z))
		end
	end
end)