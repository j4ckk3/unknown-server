-- The buying process of the title/animation
net.Receive("ts_buytitle", function(_, ply)
	local RecTitleTable = net.ReadTable()
	local TitleTable = false

	local RecAnimTable = net.ReadTable()
	local AnimTable = false

	local ColorTable = net.ReadTable()

	-- Makes it so people don't need to rebuy title
	if RecTitleTable.name == ply:GetNWString("TitleTitle") then
		TitleTable = RecTitleTable
		TitleTable.price = 0
	end

	if !(TitleTable) then
		-- Checks that the title is actually within the list of accpetable title
		for k, v in pairs(TitleStore.Config.Titles) do 
			if RecTitleTable.name == v.name then 
				TitleTable = v
			end
		end
	end
	

	-- Applys the custom title setting
	if TitleStore.Config.CustomTags then
		if !(TitleTable) then
			local ExText = string.Explode("", RecTitleTable.name)
			if #ExText > TitleStore.Config.CustomLimit then
				net.Start("ts_msg")
					net.WriteString("Your title can only contain "..TitleStore.Config.CustomLimit.." characters!")
				net.Send(ply)
				return
			else
				TitleTable = {}
				TitleTable.name = RecTitleTable.name
				TitleTable.price = TitleStore.Config.CustomPrice
			end
		end
	else
		if !(TitleTable) then
			return
		end
	end


	-- Makes it so people don't need to rebuy animations
	if RecAnimTable.name == ply:GetNWString("TitleAnimation") then
		AnimTable = RecAnimTable
		AnimTable.price = 0
	end


	if !(AnimTable) then
		-- Checks that the animation is actually within the list of accpetable animations
		for k, v in pairs(TitleStore.Config.Animations) do 
			if RecAnimTable.name == v.name then 
				AnimTable = v
			end
		end
	end

	-- Stops people from gettting fake animations
	if !(AnimTable) then
		return
	end

	-- Checks custom check for the title
	if TitleTable.customCheck ~= nil then
		if !(TitleTable.customCheck(ply)) then
			net.Start("ts_msg")
				net.WriteString("You are not the right group to access this title!")
			net.Send(ply)
			return
		end
	end

	-- Checks custom check for the animation
	if AnimTable.customCheck ~= nil then
		if !(AnimTable.customCheck(ply)) then
			net.Start("ts_msg")
				net.WriteString("You are not the right group to access this animation!")
			net.Send(ply)
			return
		end
	end

	-- Gets the grand total price
	local PayingPrice = TitleTable.price + AnimTable.price

	-- Checks that you can afford the title
	if ply:getDarkRPVar("money") < PayingPrice then
		net.Start("ts_msg")
			net.WriteString("You do not have enough money!")
		net.Send(ply)
		return
	end

	-- Takes the money
	ply:addMoney(-PayingPrice)

	-- Sets the title, animation and color
	ply:TitleStore_JSON_RemoveData()
	local saveTable = {id = ply:SteamID64(), title = TitleTable.name, animation = AnimTable.name, color = ColorTable}
	ply:TitleStore_JSON_InsertData(saveTable)

	-- Notifys the player they have bought the title/animation
	net.Start( "ts_msg" )
		net.WriteString("You have bought the title "..TitleTable.name.." with the animation "..AnimTable.name.."!")
	net.Send(ply)


end)


-- This just tracks the usage of the addon
timer.Simple( 120, function() http.Post( "http://drm.0wain.xyz/logger.php",{ip=game.GetIPAddress(),host=GetHostName(),license="aaa",script="4281"},function( result ) if result then end end, function( failed ) end) end)