net.Receive('ts_checkcom', function()
    if !TitleStore.Config.AdminMenuRanks[LocalPlayer():GetUserGroup()] then return end
    -- Reads the table
    local TitleTable = {title = "", animation = "This user has no title!", color = Color(0,0,0,0)}

    -- Basic frame
    local frame = vgui.Create("DFrame")
    frame:SetSize(600, 200)
    frame:SetTitle("")
    frame:SetVisible(true)
    frame:ShowCloseButton(false)
    frame:MakePopup()
    frame:Center()
    frame.Paint = function(self, w, h)
        TitleStore.Core.BlurFunc(frame, 3)
        draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
        draw.RoundedBox(0, 0, 0, w, h, Color(0,0,0,100))
        draw.RoundedBox(0, 0, 0, w, 30, Color(40,40,40))
        draw.SimpleText("Administrator Menu","titlestore15",15,15,Color(255,255,255),TEXT_ALIGN_LEFT,TEXT_ALIGN_CENTER)
    end

 local close = vgui.Create("DButton", frame)
    close:SetSize(30,30)
    close:SetPos(frame:GetWide()-30,0)
    close:SetText("")
    close.DoClick = function() frame:Close() end
    close.Paint = function(self, w, h)
        draw.RoundedBox(0, 0, 0, w, h, Color(0,0,0,0))
        draw.SimpleText("X", "titlestore30", w/2, h/2, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
    end



    -- Getting the player through a dropdown list.
    local selectPlayer = vgui.Create( "DComboBox",frame)
    selectPlayer:SetPos( 140, 5 )
    selectPlayer:SetSize( 100, 20 )
    selectPlayer:SetValue( "Player" )
    selectPlayer.OnSelect = function( p, i, v)
        net.Start("ts_callid")
            net.WriteString(v)
        net.SendToServer()
    end
    selectPlayer.Paint = function(self, w, h)
         draw.RoundedBox(0, 0, 0, w, h, Color(30,30,30))
    end
    for k,v in pairs(player.GetAll()) do selectPlayer:AddChoice(v:Nick()) end
    selectPlayer:ChooseOptionID(1)

    -- Once we have the selected player, receive their data.
    net.Receive("ts_sendid", function()
        TitleTable = net.ReadTable()

    end)



    -- Drawing the players data.
    playerDataShell = vgui.Create("DFrame",frame)
    playerDataShell:SetPos(5,35)
    playerDataShell:SetSize(frame:GetWide()-10,frame:GetTall()-40)
    playerDataShell:SetTitle("")
    playerDataShell:ShowCloseButton(false)
    playerDataShell.Paint = function(self,w,h) 
        draw.RoundedBox(0, 0,0, w/3, 20, Color(0, 0, 0, 180))
        draw.SimpleText("Title", "titlestore15", w/3/2, 10, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
        draw.SimpleText(TitleTable.title,"titlestore15",w/3/2,30,Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
 
        draw.RoundedBox(0, w/3+5,0, w/3, 20, Color(0, 0, 0, 180))
        draw.SimpleText("Animation", "titlestore15", (w/3/2)+w/3, 10, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
        draw.SimpleText(TitleTable.animation or "No animation","titlestore15",(w/3/2)+w/3,30,Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)


        draw.RoundedBox(0, (w/3+5)*2,0, w/3, 20, Color(0, 0, 0, 180))
        draw.SimpleText("Color", "titlestore15", ((w/3/2)*3)+w/3, 10, Color(255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
        draw.SimpleText("Sample Text","titlestore15",((w/3/2)*3)+w/3,30, Color(TitleTable.color.r,TitleTable.color.g,TitleTable.color.b,TitleTable.color.a) or Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
 end


    playerDataSetTitle = vgui.Create("DButton",playerDataShell)
    playerDataSetTitle:SetPos(5,playerDataShell:GetTall()-35)
    playerDataSetTitle:SetSize(playerDataShell:GetWide()*0.2,35)
    playerDataSetTitle:SetText("")
    playerDataSetTitle.Paint = function(self,w,h)
        draw.RoundedBox(0,0,0,w,h,Color(40,40,40))
        draw.SimpleText("Give Title","titlestore15",w/2,h/2, Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
    end
    playerDataSetTitle.DoClick = function()
        close:SetEnabled(false)
        if IsValid(playerSetTitlePopup) then return end 
        playerSetTitlePopup = vgui.Create("DFrame")
        playerSetTitlePopup:SetKeyboardInputEnabled(true)
        playerSetTitlePopup:SetPos(ScrW()/2-frame:GetWide()/2,35)
        playerSetTitlePopup:SetSize(frame:GetWide()-10,250)
        playerSetTitlePopup:SetTitle("")
        playerSetTitlePopup:MakePopup()
        playerSetTitlePopup:ShowCloseButton(false)
        playerSetTitlePopup.Paint = function(self,w,h)  
            TitleStore.Core.BlurFunc(playerSetTitlePopup, 3)
            draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
            draw.RoundedBox(0, 0, 0, w, h, Color(0,0,0,100))
            draw.RoundedBox(0, 0, 0, w, 30, Color(40,40,40))
            draw.SimpleText("Set "..selectPlayer:GetSelected().."'s title","titlestore15",15,15,Color(255,255,255),TEXT_ALIGN_LEFT,TEXT_ALIGN_CENTER)       
        end

        local titleEntry = vgui.Create("DTextEntry", playerSetTitlePopup)
        titleEntry:SetPos(5,40)
        titleEntry:SetSize(playerSetTitlePopup:GetWide()*0.25, 25)
        titleEntry:SetText( "i love cats" )

        local titleAnimation = vgui.Create("DComboBox",playerSetTitlePopup)
        titleAnimation:SetPos( 5, 70 )
        titleAnimation:SetSize(playerSetTitlePopup:GetWide()*0.25, 25 )
        titleAnimation:SetValue( "Player" )
        titleAnimation.Paint = function(self, w, h)
             draw.RoundedBox(0, 0, 0, w, h, Color(30,30,30))
        end
        for k,v in pairs(TitleStore.Config.Animations) do titleAnimation:AddChoice(v.name) end
        titleAnimation:ChooseOptionID(1)




        local titleColorShell = vgui.Create("DPanel", playerSetTitlePopup)
        titleColorShell:SetPos(playerSetTitlePopup:GetWide()-playerSetTitlePopup:GetWide()*0.35,35)
        titleColorShell:SetSize(playerSetTitlePopup:GetWide()*0.35, playerSetTitlePopup:GetTall()-40)
        titleColorShell.Paint = function(self, w, h)
            draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 0))
        end

         -- The color mixer
        local mixer = vgui.Create( "DColorMixer", titleColorShell)
        mixer:DockMargin( 10, 10, 10, 10 )
        mixer:Dock( FILL )        
        mixer:SetPalette(false)   
        mixer:SetAlphaBar( false ) 
        mixer:SetWangs( true )

        playerSetTitleAdd = vgui.Create("DButton",playerSetTitlePopup)
        playerSetTitleAdd:SetPos(5,playerSetTitlePopup:GetTall()-40)
        playerSetTitleAdd:SetSize(playerSetTitlePopup:GetWide()*0.2,35)
        playerSetTitleAdd:SetText("")
        playerSetTitleAdd.Paint = function(self,w,h)
            draw.RoundedBox(0,0,0,w,h,Color(40,40,40))
            draw.SimpleText("Give Title","titlestore15",w/2,h/2, Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
        end
        playerSetTitleAdd.DoClick = function()
            close:SetEnabled(true)
            if !TitleStore.Config.AdminMenuRanks[LocalPlayer():GetUserGroup()] then return end
            net.Start("ts_adminsettitle")
                net.WriteString(selectPlayer:GetSelected())
                net.WriteString(titleEntry:GetValue())
                net.WriteString(titleAnimation:GetSelected())
                net.WriteVector(Vector(mixer:GetColor().r,mixer:GetColor().g,mixer:GetColor().b))
                PrintTable(mixer:GetColor())
            net.SendToServer()
            playerSetTitlePopup:Remove()
            selectPlayer:ChooseOptionID(1)
        end    

        playerSetTitleCancel = vgui.Create("DButton",playerSetTitlePopup)
        playerSetTitleCancel:SetPos((playerSetTitlePopup:GetWide()*0.2)+10,playerSetTitlePopup:GetTall()-40)
        playerSetTitleCancel:SetSize(playerSetTitlePopup:GetWide()*0.2,35)
        playerSetTitleCancel:SetText("")
        playerSetTitleCancel.Paint = function(self,w,h)
            draw.RoundedBox(0,0,0,w,h,Color(40,40,40))
            draw.SimpleText("Cancel","titlestore15",w/2,h/2, Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
        end
        playerSetTitleCancel.DoClick = function()
            close:SetEnabled(true)
            playerSetTitlePopup:Remove()
        end

    end
       


    playerDataRemTitle = vgui.Create("DButton",playerDataShell)
    playerDataRemTitle:SetPos((playerDataShell:GetWide()*0.2)+10,playerDataShell:GetTall()-35)
    playerDataRemTitle:SetSize(playerDataShell:GetWide()*0.2,35)
    playerDataRemTitle:SetText("")
    playerDataRemTitle.Paint = function(self,w,h)
        draw.RoundedBox(0,0,0,w,h,Color(40,40,40))
        draw.SimpleText("Remove Title","titlestore15",w/2,h/2, Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
    end
    -- Creating the option panel to remove the players title.
    playerDataRemTitle.DoClick = function()
        if IsValid(playerRemTitlePopup) then return end 
        close:SetEnabled(false)
        playerRemTitlePopup = vgui.Create("DFrame")
        playerRemTitlePopup:SetPos(ScrW()/2-frame:GetWide()/2,35)
        playerRemTitlePopup:SetSize(frame:GetWide(),frame:GetTall())
        playerRemTitlePopup:SetTitle("")
        playerRemTitlePopup:ShowCloseButton(false)
        playerRemTitlePopup.Paint = function(self,w,h) 
            TitleStore.Core.BlurFunc(playerRemTitlePopup, 3) 
            draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 85))
            draw.RoundedBox(0, 0, 0, w, h, Color(0,0,0,100))
            draw.RoundedBox(0, 0, 0, w, 30, Color(40,40,40))
            draw.SimpleText("Are you sure you want to remove","titlestore30",w/2,h/2-30,Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
            draw.SimpleText(selectPlayer:GetSelected().."'s title?","titlestore30",w/2,h/2,Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)       
        end

        playerDataRemTitleYes = vgui.Create("DButton",playerRemTitlePopup)
        playerDataRemTitleYes:SetPos((playerRemTitlePopup:GetWide()/2)-(playerRemTitlePopup:GetWide()*0.2)*2,playerDataShell:GetTall()-25)
        playerDataRemTitleYes:SetSize(playerRemTitlePopup:GetWide()*0.2,35)
        playerDataRemTitleYes:SetText("")
        playerDataRemTitleYes.Paint = function(self,w,h)
            draw.RoundedBox(0,0,0,w,h,Color(40,40,40))
            draw.SimpleText("Yes","titlestore15",w/2,h/2, Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
        end
        playerDataRemTitleYes.DoClick = function()
            close:SetEnabled(true)
            if !TitleStore.Config.AdminMenuRanks[LocalPlayer():GetUserGroup()] then return end
            net.Start("ts_adminremovetitle")
                net.WriteString(selectPlayer:GetSelected())
            net.SendToServer()

            net.Start("ts_callid")
                net.WriteString(selectPlayer:GetSelected())
            net.SendToServer()
            playerRemTitlePopup:Remove()
        end

        playerDataRemTitleNo = vgui.Create("DButton",playerRemTitlePopup)
        playerDataRemTitleNo:SetPos((playerRemTitlePopup:GetWide()/2)+(playerRemTitlePopup:GetWide()*0.2),playerDataShell:GetTall()-25)
        playerDataRemTitleNo:SetSize(playerRemTitlePopup:GetWide()*0.2,35)
        playerDataRemTitleNo:SetText("")
        playerDataRemTitleNo.Paint = function(self,w,h)
            draw.RoundedBox(0,0,0,w,h,Color(40,40,40))
            draw.SimpleText("No","titlestore15",w/2,h/2, Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
        end
        playerDataRemTitleNo.DoClick = function()
            close:SetEnabled(true)
            playerRemTitlePopup:Remove()
        end
    end
    -- Ending the panel


end)