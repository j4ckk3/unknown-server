TitleStore = {}

print("Loading Title Store")
if SERVER then
	-- Loads stuff server side

	include("titlestore_core/config/sh_config.lua")
	include("titlestore_core/config/sh_animations.lua")
	include("titlestore_core/core/sv_netmsg.lua")
	include("titlestore_core/core/sv_buytitle.lua")
	include("titlestore_core/core/sv_json.lua")
	include("titlestore_core/dermas/sh_core.lua")

	AddCSLuaFile("titlestore_core/config/sh_config.lua")
	AddCSLuaFile("titlestore_core/config/sh_animations.lua")
	AddCSLuaFile("titlestore_core/dermas/sh_core.lua")
	AddCSLuaFile("titlestore_core/dermas/cl_animation.lua")
	AddCSLuaFile("titlestore_core/core/cl_titles.lua")
	AddCSLuaFile("titlestore_core/dermas/cl_fonts.lua")
	AddCSLuaFile("titlestore_core/dermas/cl_storeui.lua")
	
	hook.Add( "InitPostEntity", "TitleStore.JSON.InitialHook", function()
		file.CreateDir( "title_store" )
		if file.Exists( "title_store/userdata.txt", "DATA" ) then
			local TABLE = util.JSONToTable(file.Read("title_store/userdata.txt", "DATA"))
			for k, v in pairs(TABLE) do
				TitleStore.JSON.WriteData(v)
			end
			file.Delete( "title_store/userdata.txt" )
		end
	end)

end

if CLIENT then
	-- Loads stuff client side

	include("titlestore_core/config/sh_config.lua")
	include("titlestore_core/config/sh_animations.lua")
	include("titlestore_core/dermas/sh_core.lua") 
	include("titlestore_core/dermas/cl_animation.lua")
	include("titlestore_core/dermas/cl_fonts.lua")
	include("titlestore_core/dermas/cl_storeui.lua")
	include("titlestore_core/core/cl_titles.lua")

end
print("Loaded Title Store")