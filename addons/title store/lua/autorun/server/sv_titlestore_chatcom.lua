-- A simple chat command hook
hook.Add("PlayerSay", "TitleStoreCommand", function( ply, text )
	if string.lower(text) == string.lower(TitleStore.Config.AdminMenu) then

		-- Checks the user is the correct group to access the command
		if !TitleStore.Config.AdminMenuRanks[ply:GetUserGroup()] then
			net.Start("ts_msg")
				net.WriteString("You do not have access to that command!")
			net.Send(ply)
			return
		end
		-- Opens the derma
		net.Start('ts_checkcom')
		net.Send(ply)
	end

	net.Receive("ts_callid", function(len,ply)
		-- Checks the user is the correct group to access the command
		if !TitleStore.Config.AdminMenuRanks[ply:GetUserGroup()] then return end
		local called = net.ReadString()
		for k,v in pairs(player.GetAll()) do
			if v:Nick() == called then 
				local steamid = v:SteamID64()
				local IDsTable = TitleStore.JSON.ReadID(steamid)
				net.Start("ts_sendid")
					net.WriteTable(IDsTable)
				net.Send(ply)
				break
			end
		end
	end)
end)

net.Receive("ts_adminremovetitle",function(len,ply)
	if !TitleStore.Config.AdminMenuRanks[ply:GetUserGroup()] then return end
	local called = net.ReadString()
	for k,v in pairs(player.GetAll()) do
			if v:Nick() == called then 
				local steamid = v:SteamID64()
				v:TitleStore_JSON_RemoveData()
				break
			end
		end
end)



net.Receive("ts_adminsettitle",function(len,ply)
	if !TitleStore.Config.AdminMenuRanks[ply:GetUserGroup()] then return end
	local ply = net.ReadString()
	for k,v in pairs(player.GetAll()) do
		if v:Nick() == ply then 
			local steamid = v:SteamID64()
			player.GetBySteamID64(steamid):TitleStore_JSON_RemoveData()
			local text = net.ReadString()
			local anim = net.ReadString()
			local color = net.ReadVector()
			local saveTable = {id = steamid, title = text, animation = anim, color = color}
			player.GetBySteamID64(steamid):TitleStore_JSON_InsertData(saveTable)
			break	
		end
	end
end)