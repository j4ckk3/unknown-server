AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
AddCSLuaFile("titlestore_core/config/sh_config.lua")
include("shared.lua")
include("titlestore_core/config/sh_config.lua")

-- Sets the players modela and basic physics ect..
function ENT:Initialize()
	self:SetModel(TitleStore.Config.NPCModel)
	self:SetHullType(HULL_HUMAN);
	self:SetHullSizeNormal();
	self:SetNPCState(NPC_STATE_SCRIPT)
	self:SetSolid(SOLID_BBOX)
	self:SetUseType(SIMPLE_USE)
	self:DropToFloor()
end

function ENT:AcceptInput(name, activator, caller)
	-- Basic checks
	if activator:IsPlayer() == false then return end
	if activator:GetPos():Distance( self:GetPos() ) > 100 then return end

	-- Opens derma
	net.Start("ts_derma")
	net.Send(activator)
end