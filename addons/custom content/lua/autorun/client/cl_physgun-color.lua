-- This is how you should do rainbow physguns
rainbowPhys = rainbowPhys or {}
rainbowVector = Vector(1, 1, 1)
rainbowColor = Color(255, 255, 255)

net.Receive("physgunUpdate", function()
	local tbl = net.ReadTable()
	rainbowPhys = tbl

	for k, v in pairs(tbl) do
		k.rainbowPhys = true
	end
end)

hook.Add("Tick","physguncol.Tick", function()
	local counter = CurTime()/2
	local red = math.sin(3 * counter) * 127 + 128
	local green = math.sin(3 * counter + 2) * 127 + 128
	local blue = math.sin(3 * counter + 4) * 127 + 128
	rainbowVector = Vector(red / 255, green / 255, blue / 255)
	rainbowColor = Color(red, green, blue)
end)

hook.Add("DrawPhysgunBeam", "physguncol.DrawPhysgunBeam", function(ply, wep)
	if ply.rainbowPhys then
		ply:SetWeaponColor(rainbowVector)
	end
end)