local AFKS = {}
AFKS.Max = game.MaxPlayers() - 8
if player.GetCount then -- cba to update my dev server to get this function lol
	AFKS.GetCount = player.GetCount
else
	AFKS.GetCount = function() return #player.GetAll() end
end

hook.Add("playerSetAFK", "AFKMod", function(ply, state)
	if state then
		timer.Create(ply:SteamID() .. "AFKS", 5400, 0, function()
			if ply and IsValid(ply) then
				if AFKS.GetCount() < AFKS.Max then return end
				ply:Kick("AFK for 2 hours on a full server.\nYou can rejoin when you read this, apologies for the inconvenience")
			end
		end)
	elseif ply and IsValid(ply) then
		timer.Remove(ply:SteamID() .. "AFKS")
	end
end)

hook.Add("PlayerDisconnected", "AFKMod.PlayerDisconnected", function(ply)
	timer.Remove(ply:SteamID() .. "AFKS")
end)

hook.Add("canGoAFK", "AFKMod", function(ply, state)
	if ply:IsFrozen() and state then
		return false
	end
end)