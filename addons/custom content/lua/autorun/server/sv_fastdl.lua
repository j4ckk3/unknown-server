--fas2
resource.AddWorkshop( "201027186" ) -- Misc
resource.AddWorkshop( "201027715" ) -- U. Rifles
resource.AddWorkshop( "183140076" ) -- Shotguns
resource.AddWorkshop( "181656972" ) -- Rifles
resource.AddWorkshop( "181283903" ) -- Pistols
resource.AddWorkshop( "183139624" ) -- SMGs
resource.AddWorkshop( "180507408" ) -- Base

-- tdm
	--resource.AddWorkshop( "112606459" ) -- Base Pack
	--resource.AddWorkshop( "113120185" ) -- Audi
	--resource.AddWorkshop( "349281554" ) -- Emergency Vehicles
	--resource.AddWorkshop( "323285641" ) -- GTA Vehicles
	--resource.AddWorkshop( "259899351" ) -- Porsche

-- misc
resource.AddWorkshop( "924744897" ) -- Fidget Spinner
resource.AddWorkshop( "704145900" ) -- Hobo Swep
--resource.AddWorkshop( "110286060" ) -- downtown v4c v2
resource.AddWorkshop( "1171188058" ) -- downtown v4c h1
resource.AddWorkshop( "877752661" ) -- Bitminers2
resource.AddWorkshop( "696466446" ) -- Plate Press
resource.AddWorkshop( "1086143799") -- Moonshine
resource.AddWorkshop( "970566268" ) -- Opium
resource.AddWorkshop( "171935748" ) -- Popcorn Swep
resource.AddWorkshop( "834550080" ) -- Police Scanner

-- accessories
resource.AddWorkshop( "148215278" ) -- GMod Tower
resource.AddWorkshop( "572310302" ) -- GTA V
resource.AddWorkshop( "282958377" ) -- Animal masks