hook.Add("PlayerSay", "KickAllnShutdown", function(ply, str)
	if str == "!restartserver" and ply:IsSuperAdmin() then
		game.ConsoleCommand("sv_password 324\n")
		aprint(Color(255,0,0), "WARNING: ", true, "Server restarting in 60 seconds")
		aprint(Color(0,255,0), "WARNING: ", true, "Server restarting in 60 seconds")
		aprint(Color(0,0,255), "WARNING: ", true, "Server restarting in 60 seconds")

		local tminus = 60
		timer.Create("Shutdown-timer", 10, 6, function()
			tminus = tminus - 10
			aprint(Color(255,0,0), "WARNING: ", true, "Server restarting in " .. tminus .. " seconds")
			aprint(Color(0,255,0), "WARNING: ", true, "Server restarting in " .. tminus .. " seconds")
			aprint(Color(0,0,255), "WARNING: ", true, "Server restarting in " .. tminus .. " seconds")

			if tminus == 0 then game.ConsoleCommand("_exit\n") end
		end)
	end
end)

concommand.Add( "_exit", function( ply )
	if IsValid(ply) and !ply:IsSuperAdmin() then return end

	hook.Run("ShutDown") -- Call this to get MySQL and stuff to save
	print("Server shutting down...")

	Entity(0):GetAnimInfo(99999999) -- Crashes the server to trigger reboot
	Entity(0):Input()
	Entity(0):Remove() -- They fixed this wat?
end )