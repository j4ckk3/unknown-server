util.AddNetworkString("chatcolUpdate")
rainbowChat = rainbowChat or {}

function setupChatColor(ply)
	for k in pairs(rainbowChat) do
		if !IsValid(k) or !k.IsPlayer then
			rainbowChat[k] = nil
		end
	end

	ply.rainbowChat = true
	if rainbowChat[ply] then return end
	rainbowChat[ply] = true

	net.Start("chatcolUpdate")
		net.WriteTable(rainbowChat)
	net.Broadcast()
end

hook.Add("PlayerInitialSpawn", "rainbowChat.PlayerInitialSpawn", function(ply)
	timer.Simple(5, function()
		if (kShop.PlayerHasUpgrade(ply, "darkrp-rainbow_chat")) then
			setupChatColor(ply)
		end
	end)
end)