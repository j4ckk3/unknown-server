Aerobank = Aerobank or {}
Aerobank.BankPlys = {}
Aerobank.BankArea = {
	Vector(-1136.3514404297, -2446.4204101563, -42.406047821045),
	Vector(-1665.4454345703, -3222.4494628906, -189.7915802002)
}

--#######--
-- Hooks --
--#######--

timer.Create("Aerobank", 1, 0, function()
	local ents = ents.FindInBox(Aerobank.BankArea[1], Aerobank.BankArea[2])
	Aerobank.BankPlys = {}

	for k, v in pairs(ents) do
		if v:IsPlayer() then
			Aerobank.BankPlys[v] = true
		end
	end

	for k, v in pairs(player.GetAll()) do
		if v:getJobTable().name == "Bank Security" then
			if Aerobank.BankPlys[v] then
				if v.hasBankEquipment then continue end
				local wep = v:Give("fas2_mp5a5")
				wep.nodrop = true
				v.hasBankEquipment = true
			elseif v.hasBankEquipment then
				v:StripWeapon("fas2_mp5a5")
				v.hasBankEquipment = nil
			end
		end
	end

end)

hook.Add("PlayerDeath", "Aerobank.PlayerDeath", function(ply)
	if Aerobank.BankPlys[ply] then
		-- if cop, add NLR thing?
	end
end)

-- Stop players re-entering if they died during the raid
hook.Add("ShouldCollide", "Aerobank.ShouldCollide", function(ent1, ent2)
	if (ent1 ~= Aerobank.doorEnt and ent1 ~= Aerobank.windowEnt) then return end

	if ent2.WasRobbingBank and BankIsBeingRobbed then
		aprint(Color(255,0,0), "NLR | ", ent2, "You died during this raid, you can't return. Leave the area.")
		return true
	end

	return false
end)

-- Stop the props being networked to the players
hook.Add("PlayerInitialSpawn", "Aerobank.PlayerInitialSpawn", function(ply)
	Aerobank.doorEnt:SetPreventTransmit(ply, true)
	Aerobank.windowEnt:SetPreventTransmit(ply, true)
end)

-- Spawn the entities (Needs window doing)
hook.Add("InitPostEntity", "Aerobank.InitPostEntity", function()
	-- door
	print("Running InitPostEntity!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!---------------------------------------------------------------------------------------")
	Aerobank.doorEnt = ents.Create( "prop_physics" )
	if ( !IsValid( Aerobank.doorEnt ) ) then return end
	Aerobank.doorEnt:SetModel( "models/hunter/plates/plate2x2.mdl" )
	Aerobank.doorEnt:SetPos(Vector(-1242.0067138672, -2441.8168945313, -142.09230041504))
	Aerobank.doorEnt:SetAngles(Angle(90, 90, 180))
	Aerobank.doorEnt:Spawn()

	local phys = Aerobank.doorEnt:GetPhysicsObject()
	if ( IsValid( phys ) ) then
		phys:EnableMotion(false)
	end

	Aerobank.doorEnt:SetCustomCollisionCheck(true)
	Aerobank.doorEnt:CollisionRulesChanged()

	-- window
	Aerobank.windowEnt = ents.Create( "prop_physics" )
	if ( !IsValid( Aerobank.windowEnt ) ) then return end
	Aerobank.windowEnt:SetModel( "models/hunter/plates/plate2x3.mdl" )
	Aerobank.windowEnt:SetPos(Vector(-1670.1572265625, -2611.38671875, -110.48138427734))
	Aerobank.windowEnt:SetAngles(Angle(90, 180, 180))
	Aerobank.windowEnt:Spawn()

	local phys = Aerobank.windowEnt:GetPhysicsObject()
	if ( IsValid( phys ) ) then
		phys:EnableMotion(false)
	end

	Aerobank.windowEnt:SetCustomCollisionCheck(true)
	Aerobank.windowEnt:CollisionRulesChanged()
end)