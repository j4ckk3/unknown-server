util.AddNetworkString("physgunUpdate")
rainbowPhys = rainbowPhys or {}

function setupRainbowPhysgun(ply)
	for k in pairs(rainbowPhys) do
		if !IsValid(k) or !k.IsPlayer then
			rainbowPhys[k] = nil
		end
	end

	ply.rainbowPhys = true
	if rainbowPhys[ply] then return end
	rainbowPhys[ply] = true

	net.Start("physgunUpdate")
		net.WriteTable(rainbowPhys)
	net.Broadcast()
end

hook.Add("PlayerLoadout", "physguncol.PlayerLoadout", function(ply)
	if ply.rainbowPhys then return
	elseif (kShop.PlayerHasUpgrade(ply, "physgun")) then
		setupRainbowPhysgun(ply)
		return
	end
	
	local col = ply:getJobTable().color
	ply:SetWeaponColor( Vector( col.r/255, col.g/255, col.b/255 ) )
end)

hook.Add("PlayerInitialSpawn", "physguncol.PlayerInitialSpawn", function(ply)
	timer.Simple(5, function()
		setupRainbowPhysgun(ply)
	end)
end)