rslots = rslots or {}
rslots.cache = rslots.cache or {}
rslots.connecting = rslots.connecting or {}
if player.GetCount then -- cba to update my dev server to get this function lol
	rslots.GetCount = player.GetCount
else
	rslots.GetCount = function() return #player.GetAll() end
end

-- config
rslots.debug = false
rslots.reserved = 0
rslots.staff = 4
rslots.lang = {
	["reserved"] = "The last " .. rslots.reserved .. " slots are reserved for members, sorry!\nAccess them by putting hyplex.co in your Steam name.",
	["staff"] = "#GameUI_ServerRejectServerFull",
}
rslots.ranks = {
	["root"] = true,
	["cmanager"] = true,
	["mentor"] = true,
	["admin"] = true,
	["mod"] = true,
}

-- Get public slots and max slots
rslots.mslots = game.MaxPlayers()
rslots.public = rslots.mslots - (rslots.reserved+rslots.staff)
rslots.publicres = rslots.public+rslots.reserved

-- debug stuff
local function dprint(...)
	print(...)
end
if rslots.debug == false then
	function dprint(...) end
end

-- Setting the max playersss
timer.Create("rslots", 1, 1, function()
	RunConsoleCommand("sv_visiblemaxplayers", rslots.publicres)
end)

-- Count the players who aren't spawned in
hook.Add("PlayerConnect", "rslots.PlayerConnect", function(_, ip)
	rslots.connecting[ip] = true
	timer.Create(ip.."rslots", 400, 1, function()
		rslots.connecting[ip] = nil -- In case they time out or some shit
	end)
end)
hook.Add("PlayerInitialSpawn", "rslots.PlayerInitialSpawn", function(ply)
	local ip = ply:IPAddress()
	rslots.connecting[ip] = nil
	timer.Remove(ip.."rslots")
end)
hook.Add("PlayerDisconnected", "rslots.PlayerDisconnected", function(ply) -- This shouldn't be needed, but w/e
	local ip = ply:IPAddress()
	rslots.connecting[ip] = nil
	if !rslots.cache[ip] then return end
	
	timer.Create(ip.."cache", 300, 1, function() -- Wait 5 mins in case their game crashed, or they're rejoining
		rslots.cache[ip] = nil
	end)
end)

-- Check if there is enough space!
hook.Add("CheckPassword", "rslots.CheckPassword", function(sid, ip, _, _, name)
	local connecting = table.Count(rslots.connecting) - 1 -- We allow for an error margin of 1
	local sid = util.SteamIDFrom64(sid)

	local function d(d, cached)
		if cached then
			dprint("> Loading " .. ip)

			if rslots.cache[ip] == true and !d then
				dprint("WARNING> Cached data is invalid. Query may be delayed, retrying in 2 seconds.")
				timer.Simple(3, function() -- Give the initial query time to answer?
					d(true, true)
				end)
			elseif rslots.cache[ip] == true and d then
				dprint("WARNING> Cached data is still invalid after retry...")
				d = {}
			else
				dprint("OK> Loaded Cached Data")
				d = rslots.cache[ip]
				timer.Create(ip.."cache", 1800, 1, function() -- Clear cached data after a while
					rslots.cache[ip] = nil
				end)
			end
		else
			dprint("> Caching " .. ip)

			rslots.cache[ip] = d
			timer.Create(ip.."cache", 1800, 1, function() -- Clear cached data after a while
				rslots.cache[ip] = nil
			end)
		end
		dprint("INFO> Current Players:", connecting + rslots.GetCount() - 1) -- We take away one on because GMC reconnects the player after calling PlayerConnect. This causes the connecting player to be counted in the amonut of players already spawned in...

		local data = d[1]
		if data then
			local rank = data.Rank
			local rank2 = nil
			if data.Vars then
				rank2 = util.JSONToTable(data.Vars).darkrp_rank
			end
			dprint("INFO> Rank Data:", rank, rank2)

			if (rank and rslots.ranks[rank]) or (rank2 and rslots.ranks[rank2]) then -- If they're staff they bypass everything
				dprint("OK> Rank Whitelisted")
				return
			end
		else
			dprint("WARNING> Data is nil; database down or player is new")
		end

		plys = connecting + rslots.GetCount() - 1 -- We take away one because GMC reconnects the player after calling PlayerConnect. This causes the connecting player to be counted in the amonut of players already spawned in...
		if plys >= rslots.publicres then
			dprint("KICK> Player is joining staff slot, kick!")
			game.KickID(sid, rslots.lang["staff"])
			rslots.connecting[ip] = nil
		elseif plys > rslots.public then
			dprint("INFO> Player joining reserved hyplex.co slot")
			if string.find(name:lower(), "hyplex.co") then
				dprint("OK> Player has hyplex.co in their name")
				return
			end
			dprint("KICK> Hyplex.co isn't in their name, kick!")
			game.KickID(sid, rslots.lang["reserved"])
			rslots.connecting[ip] = nil
		end
	end

	-- This cache system is in place because GMCHosting's DDoS mitigation reconnects the player instantly; causing this hook to be called twice before a MySQL query would even normally respond...
	if rslots.cache[ip] then
		local delay = 0
		if rslots.cache[ip] == true then delay = 1 end -- The first query probably hasn't been returned yet, wait a second before calling

		timer.Simple(delay, function() -- Give the initial query time to answer
			d(false, true)
		end)
		return
	end
	
	rslots.cache[ip] = true
	D3A.MySQL.Query("CALL selectUserInfo('" .. sid .. "');", d)
end)