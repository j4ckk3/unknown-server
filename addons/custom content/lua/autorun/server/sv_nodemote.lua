hook.Add("canDemote", "DisableDarkRPDemote", function(ply)
	if ply and ply:IsPlayer() and !ply:isMayor() then
		return {false, "Only the mayor can demote people"}
	end
end)