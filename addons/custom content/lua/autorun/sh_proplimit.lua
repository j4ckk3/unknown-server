hook.Add("InitPostEntity", "proplimit:InitPostEntity", function()
	timer.Simple(5, function()
		local meta = FindMetaTable( "Player" )
		if ( !meta ) then return end

		function meta:CheckLimit( str )
			-- No limits in single player
			if ( game.SinglePlayer() ) then return true end

			local c = cvars.Number( "sbox_max" .. str, 0 ) + (self.extraprops or 0)

			if ( c < 0 ) then return true end
			if ( self:GetCount( str ) > c - 1 ) then
				if ( SERVER ) then self:LimitHit( str ) end
				return false
			end
			return true
		end
	end)
end)

hook.Add("PlayerInitialSpawn", "proplimit:PlayerInitialSpawn", function(ply)
	timer.Simple(5, function()
		if !IsValid(ply) then return end

		if (kShop.PlayerHasUpgrade(ply, "darkrp-extraprops_1")) then
			ply.extraprops = (ply.extraprops and ply.extraprops + 20) or 20
		end
		if (kShop.PlayerHasUpgrade(ply, "darkrp-extraprops_2")) then
			ply.extraprops = (ply.extraprops and ply.extraprops + 20) or 20
		end
	end)
end)