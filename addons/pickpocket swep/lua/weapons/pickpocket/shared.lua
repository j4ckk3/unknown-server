SWEP.Base = "progressweapon_base_v1"
if CLIENT then
	SWEP.PrintName = "Pickpocket"
	SWEP.Instructions = "Steal money from inconsiderate fools.\nCome close to your victim and press left mouse button."
else
	AddCSLuaFile("shared.lua")
end

/*
	Configuration
*/
SWEP.CheckTime = 2 // pickpocket time
SWEP.StealDetection = 20 // time required to detect the loss of money (victim will receive a notify about his loss right after this time passed)
SWEP.MoneyToSteal = {20, 40} // you will earn from 20 to 40 $ for every pickpocket
SWEP.StealTreshold = 200 // you can not pickpocket a player who has less than 200$


/*
	Do not edit the code below unless you know what you are doing
*/
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

function SWEP:PlaySound()
end

function SWEP:Done(victim)
	local money_to_steal = math.random(self.MoneyToSteal[1], self.MoneyToSteal[2])
	if not victim:MultiversionCanAfford(money_to_steal) or (victim.nextSteal and victim.nextSteal > CurTime()) or !victim:MultiversionCanAfford(self.StealTreshold) then self.Owner:MultiversionNotify(NOTIFY_ERROR, "Have some mercy! This guy already has nothing left!") return end
	
	victim.nextSteal = CurTime() + 30

	self.Owner:MultiversionNotify(NOTIFY_GENERIC, "You stole "..tostring(money_to_steal).."$!")
	
	local time = self.StealDetection
	timer.Simple(time, function() if IsValid(victim) then victim:MultiversionNotify(NOTIFY_ERROR, "Someone stole "..money_to_steal.."$ from your pocket!") end end)
	
	victim:MultiversionAddMoney(-money_to_steal)
	self.Owner:MultiversionAddMoney(money_to_steal)

	umsg.Start("anim_keys")
		umsg.Entity(self.Owner)
		umsg.String("usekeys")
	umsg.End()
end

if SERVER then
	hook.Add("PlayerInitialSpawn", "PickpocketDelay", function(ply)
		ply.nextSteal = CurTime() + 30 // Prevent recently connected players from pickpocket
	end)
end