---------------------------------MAP---------------------------
resource.AddWorkshop("876839844") --Map

resource.AddWorkshop("789417054") --Map Content 1 (GLife)
resource.AddWorkshop("328735857") --Map Content 2 (Rockford)


-------------------------------OTHER---------------------------
resource.AddWorkshop("144982052") --M9K Special
resource.AddWorkshop("128089118") --M9K Assault
resource.AddWorkshop("128093075") --M9K Pistols
resource.AddWorkshop("128091208") --M9K Heavy
resource.AddWorkshop("582366370") --TFA Melee Weapons
resource.AddWorkshop("266579667") --LW Texture Content
resource.AddWorkshop("1400113491") -- LW BMW
resource.AddWorkshop("327281224") --Roleplay Props
resource.AddWorkshop("650064006") --Modern Notifications

---------------------------MODELS------------------------------
resource.AddWorkshop("412149725") --Suits & Robbers
resource.AddWorkshop("857787798") --Farmer Model

---------------------SCRIPT CONTENT-----------------------------
resource.AddWorkshop("632470227") --VcMod Content Main
resource.AddWorkshop("200318235") --Arcbank Content
resource.AddWorkshop("854872051") --Stungun
resource.AddWorkshop("609456211") --Police Shield
resource.AddWorkshop("685913625") --Deployable Keypad Cracker
resource.AddWorkshop("684399836") --Breaching Charge
resource.AddWorkshop("615887479") --Police Baton