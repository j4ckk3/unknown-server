AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("base_code.lua")

include("shared.lua")

-- Set this to false if you want to use FastDL for the content instead.
local USE_WORKSHOP = true

if (USE_WORKSHOP) then
	resource.AddWorkshop("834550080")
else
	resource.AddSingleFile("materials/entities/weapon_sh_detector.png")
	resource.AddSingleFile("materials/shenesis/circleoutline.png")
	resource.AddSingleFile("materials/shenesis/battery.png")
	resource.AddSingleFile("materials/shenesis/detector_overlay.vmt")
	resource.AddSingleFile("materials/shenesis/detector_overlay.vtf")
	resource.AddSingleFile("materials/weapons/weapon_sh_detector.vmt")
	resource.AddSingleFile("materials/weapons/weapon_sh_detector.vtf")
end

SWEP.m_fNextSecond = 0
SWEP.m_fNextRegen = 0

function SWEP:Think()
	if (!self.UseBattery) then
		return end

	local ct = CurTime()
	if (ct >= self.m_fNextSecond) then
		local bat = self:GetBattery()
		if (bat > 0) then
			self:SetBattery(bat - 1)
		end

		self.m_fNextSecond = ct + 1
	end
end

function SWEP:RegenBatteryThink()
	if (!self.UseBattery) then
		return end

	local owner = self.Owner
	if (IsValid(owner) and owner:GetActiveWeapon() == self) then
		return end

	local ct = CurTime()
	if (ct >= self.m_fNextRegen) then
		local bat = self:GetBattery()
		if (bat < self.BatteryLength) then
			self:SetBattery(bat + 1)
		end

		self.m_fNextRegen = ct + self.BatteryRegenRate
	end
end