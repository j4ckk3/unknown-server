SWEP.Category = "SH Weapons"
SWEP.Spawnable = true
SWEP.UseHands = true
SWEP.HoldType = "slam"
SWEP.Slot = 5

SWEP.ViewModel = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.ViewModelFOV = 50
SWEP.WorldModel = "models/weapons/w_c4.mdl"

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Ammo = "none"
SWEP.Primary.Automatic = false

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.Automatic = false

/** GENERAL OPTIONS **/

-- Entities that the detector can detect
SWEP.EntitiesToDetect = {
	["rprint_emeraldprinter"] = true,
	["rprint_rubyprinter"] = true,
	["rprint_platinumprinter"] = true,
	["rprint_crystal_printer"] = true,
	["rprint_nuclearprinter"] = true,
}

-- Range (in units) of the detector
-- One meter is roughly 64 units
SWEP.DetectRange = 1000

-- List refresh rate of the detector in seconds. A number too low can lead to lag.
SWEP.RefreshRate = 0.5

-- Play sound when deploying/holstering detector
SWEP.PlaySounds = true

/** BATTERY OPTIONS **/

-- Should the detector run on a battery?
SWEP.UseBattery = true

-- How long the detector's battery lasts in seconds
SWEP.BatteryLength = 150

-- Battery regen rate in seconds
SWEP.BatteryRegenRate = 0.5

/** CLIENTSIDE OPTIONS **/

-- Draw pulsing circle on detected entities?
SWEP.DrawPulseCircle = true

-- Draw distance to detected entities?
SWEP.DrawDistance = true

-- Play (clientside) beeping sound when detecting an entity?
-- The closer the player is to a detected entity, the faster the beeping.
SWEP.PlayBeepSound = true

-- Beep sound file, volume and pitch. Only applicable if "PlayBeepSound" is true
SWEP.BeepSoundInfo = {
	snd = Sound("npc/turret_floor/ping.wav"),
	pitch = 100, -- 0 to 255, 100 is normal
	volume = 1 -- 0 to 1
}

-- Should the detector emit light?
SWEP.EmitLight = true

-- Light intensity multiplier (1 is normal)
SWEP.LightIntensity = 1

-- Light distance multiplier (1 is normal)
SWEP.LightDistance = 1

function SWEP:Initialize()
	self:SetDeploySpeed(1)
	self:SetHoldType("slam")
	self:SetBattery(self.BatteryLength)

	if (SERVER) then
		hook.Add("Think", self, function(self)
			if (IsValid(self)) then
				self:RegenBatteryThink()
			end
		end)
	else
		self:BaseInitialize()
	end
end

function SWEP:SetupDataTables()
	self:NetworkVar("Float", 0, "Battery")
end

function SWEP:OnRemove()
	if (CLIENT) then
		if (IsValid(self.m_DetectorPanel)) then
			self.m_DetectorPanel:Remove()
			self.m_DetectorPanel = nil
		end

		self:BaseOnRemove()
	else
		hook.Remove("Think", self)
	end
end

function SWEP:Deploy()
	if (self.PlaySounds and SERVER) then
		self.Owner:EmitSound("items/nvg_on.wav")
	end
end

function SWEP:Holster()
	if (self.PlaySounds and SERVER) then
		self.Owner:EmitSound("items/nvg_off.wav")
	end

	return self.BaseClass.Holster(self)
end

function SWEP:PrimaryAttack()
end

function SWEP:SecondaryAttack()
end

function SWEP:OutOfBattery()
	return self.UseBattery and self:GetBattery() <= 0
end