include("shared.lua")
include("base_code.lua")

SWEP.PrintName = "Detector"
SWEP.Purpose = "Detects various electronics and other contraband."
SWEP.Instructions = "Point in any direction and the detector will display any detected objects in range."
SWEP.WepSelectIcon = surface.GetTextureID("weapons/weapon_sh_detector")
SWEP.BounceWeaponIcon = false
SWEP.DrawCrosshair = false
SWEP.BobScale = 0.5
SWEP.SwayScale = 0.5

SWEP.VElements = {
	["base"] = { type = "Model", model = "models/props_phx/construct/glass/glass_curve180x2.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "", pos = Vector(8.649, 0.716, -2.415), angle = Angle(54.223, -92.088, 90), size = Vector(0.046, 0.046, 0.068), color = Color(25, 25, 25, 255), surpresslightning = false, material = "models/props_c17/FurnitureMetal001a", skin = 0, bodygroup = {} },
	["base+"] = { type = "Model", model = "models/hunter/plates/plate1x1.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "base", pos = Vector(-0.741, -0.894, 3.131), angle = Angle(0, 0, 90), size = Vector(0.025, 0.025, 0.025), color = Color(25, 25, 25, 255), surpresslightning = false, material = "models/props_c17/FurnitureMetal001a", skin = 0, bodygroup = {} },
	["screen"] = { type = "Model", model = "models/props_phx/rt_screen.mdl", bone = "ValveBiped.Bip01_Spine4", rel = "base+", pos = Vector(-0.442, 0, -0.514), angle = Angle(90, 0, 0), size = Vector(0.1, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
	["base"] = { type = "Model", model = "models/props_phx/construct/glass/glass_curve180x2.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "", pos = Vector(7.341, -0.7, -1.219), angle = Angle(77.125, 75.404, 104.463), size = Vector(0.05, 0.05, 0.05), color = Color(25, 25, 25, 255), surpresslightning = false, material = "models/props_c17/FurnitureMetal001a", skin = 0, bodygroup = {} },
	["screen"] = { type = "Model", model = "models/props_phx/rt_screen.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base+", pos = Vector(0, 0, 0), angle = Angle(-90, -90, 180), size = Vector(0.079, 0.079, 0.079), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base+"] = { type = "Model", model = "models/hunter/plates/plate1x1.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "base", pos = Vector(-0.891, -1.619, 2.424), angle = Angle(90, 0, 90), size = Vector(0.019, 0.046, 0.019), color = Color(25, 25, 25, 255), surpresslightning = false, material = "models/props_c17/FurnitureMetal001a", skin = 0, bodygroup = {} }
}

SWEP.ShowWorldModel = false

SWEP.IronSightsPos = Vector(-10.976, -9.042, -7.441)
SWEP.IronSightsAng = Vector(14.718, 60.512, -60.156)

SWEP.m_DetectedEntities = {}
SWEP.m_fNextSweep = 0

local ipairs = ipairs
local CurTime = CurTime
local IsValid = IsValid
local color_white = color_white
local TEXT_ALIGN_CENTER = TEXT_ALIGN_CENTER
local draw = draw
local math = math
local ents = ents
local table = table
local draw_SimpleTextOutlined = draw.SimpleTextOutlined
local math_Round = math.Round
local ents_GetAll = ents.GetAll
local table_insert = table.insert

function SWEP:Think()
	local ct = CurTime()
	if (ct >= self.m_fNextSweep) then
		local pnl = self.m_DetectorPanel
		if (IsValid(pnl)) then
			local range = self.DetectRange
			local ep = self.Owner:EyePos()

			local tbl = {}

			for _, v in ipairs (ents_GetAll()) do
				if (self.EntitiesToDetect[v:GetClass()]) then
					local pos = v:LocalToWorld(v:OBBCenter())
					if (pos:Distance(ep) > range) then
						continue end

					local ts = pos:ToScreen()
					if (ts.x >= pnl.x and ts.y >= pnl.y and ts.x <= pnl.x + pnl:GetWide() and ts.y <= pnl.y + pnl:GetTall()) then
						table_insert(tbl, v)
					end
				end
			end

			self.m_DetectedEntities = tbl
		end

		self.m_fNextSweep = ct + self.RefreshRate
	end
end

function SWEP:PreDrawViewModel(vm)
	render.SetBlend(0)
end

local matCircleOutline = Material("shenesis/circleoutline.png", "noclamp smooth")
local matBattery = Material("shenesis/battery.png", "noclamp smooth")
local colOutline = Color(75, 75, 75)
local colOutOfBattery = Color(225, 0, 0)

SWEP.m_fPulseSize = 1

function SWEP:DrawDetector(pnl)
	local owner = self.Owner
	if (!IsValid(owner) or owner:ShouldDrawLocalPlayer()) then
		return end

	local ded = self:OutOfBattery()
	local ep = owner:EyePos()

	if (SH_DETECTOR_JAMMER:IsJammed(ep)) then
		local f = math.abs(math.sin(RealTime() * 3))
		local c = Color(255, 255 * f, 255 * f)
		draw_SimpleTextOutlined("SIGNAL JAMMED", "DermaLarge", pnl:GetWide() * 0.5, pnl:GetTall() * 0.5, c, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, colOutline)
	elseif (!ded) then

		self.m_fPulseSize = self.m_fPulseSize - FrameTime() * 0.5
		if (self.m_fPulseSize <= 0) then
			self.m_fPulseSize = 1
		end

		local s = 128 * self.m_fPulseSize
		local s2 = s * 0.5

		local closest
		for _, ent in ipairs (self.m_DetectedEntities) do
			if (!IsValid(ent)) then
				continue end

			local pos = ent:LocalToWorld(ent:OBBCenter())
			local dist = ep:Distance(pos)
			local mdist = math_Round(dist / 64)
			local ts = pos:ToScreen()
			local x, y = pnl:ScreenToLocal(ts.x, ts.y)

			if (!closest or dist < closest) then
				closest = dist
			end

			if (self.DrawPulseCircle) then
				surface.SetMaterial(matCircleOutline)
				surface.SetDrawColor(255, 255, 255, 255 * self.m_fPulseSize)
				surface.DrawTexturedRect(x - s2, y - s2, s, s)
			end

			if (self.DrawDistance) then
				draw_SimpleTextOutlined(math_Round(mdist) .. " M", "DermaLarge", x, y, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, colOutline)
			end
		end

		if (closest) then
			local ct = CurTime()
			if (!self.m_fNextBeep or ct >= self.m_fNextBeep) then
				local time = math.max(0.2, 2 * (closest / self.DetectRange))

				LocalPlayer():EmitSound(self.BeepSoundInfo.snd, 75, self.BeepSoundInfo.pitch, self.BeepSoundInfo.volume)
				self.m_fNextBeep = ct + time
			end
		end
	end

	if (self.UseBattery) then
		local w = pnl:GetWide()
		local y = pnl:GetTall() - 48
		local wi = w - 80

		surface.SetMaterial(matBattery)
		surface.SetDrawColor(ded and colOutOfBattery or color_white)
		surface.DrawTexturedRect(16, y, 32, 32)

		surface.DrawOutlinedRect(64, y + 8, wi, 16)

		local frac = math.Clamp(self:GetBattery() / self.BatteryLength, 0, 1)
		surface.DrawRect(64, y + 8, wi * frac, 16)
	end

	if (self.EmitLight) then
		local mdl = self.VElements.screen.modelEnt
		if (IsValid(mdl)) then
			local dlight = DynamicLight(self:EntIndex())
			if (dlight) then
				dlight.pos = mdl:GetPos()
				dlight.r = 0
				dlight.g = 75
				dlight.b = 255
				dlight.brightness = 2 * self.LightIntensity
				dlight.decay = 1000
				dlight.size = 64 * self.LightDistance
				dlight.dietime = CurTime() + 2
			end
		end
	end
end

local matBlack = CreateMaterial("sh_detector_black", "UnlitGeneric", {
	["$basetexture"] = "models/debug/debugwhite",
	["$color2"] = "[0 0 0]",
})

function SWEP:PostDrawViewModel(vm)
	render.SetBlend(1)

	local mdl = self.VElements.screen.modelEnt
	if (IsValid(mdl)) then
		mdl:SetSubMaterial(1, self:OutOfBattery() and "!matBlack" or "shenesis/detector_overlay")

		local s = SysTime()
		local pos, ang = mdl:GetPos(), mdl:GetAngles()
		local topleft3d = pos + ang:Up() * 3.56 + ang:Right() * 2.85
		local bottomright3d = pos + ang:Up() * 0.1 - ang:Right() * 2.95

		local topleft = topleft3d:ToScreen()
		local bottomright = bottomright3d:ToScreen()

		local x, y, w, h = topleft.x, topleft.y, (bottomright.x - topleft.x), (bottomright.y - topleft.y)
		if (!IsValid(self.m_DetectorPanel)) then -- !
			local pnl = vgui.Create("DPanel")
			pnl.Paint = function(me, w, h)
				if (!IsValid(self) or !IsValid(self.Owner) or self ~= self.Owner:GetActiveWeapon()) then
					me:Remove()
					return
				end

				self:DrawDetector(me)
			end
			self.m_DetectorPanel = pnl
		else
			local pnl = self.m_DetectorPanel
			pnl:SetPos(x, y)
			pnl:SetSize(w, h)
		end
	end
end

function SWEP:DrawWorldModel()
	self:BaseDrawWorldModel()

	if (self.EmitLight and IsValid(self.Owner)) then
		local pos = self.Owner:GetShootPos()
		local mdl = self.WElements.screen.modelEnt
		if (IsValid(mdl)) then
			pos = mdl:GetPos()
		end

		local dlight = DynamicLight(self:EntIndex())
		if (dlight) then
			dlight.pos = pos
			dlight.r = 0
			dlight.g = 75
			dlight.b = 255
			dlight.brightness = 4 * self.LightIntensity
			dlight.decay = 1000
			dlight.size = 64 * self.LightDistance
			dlight.dietime = CurTime() + 2
		end
	end
end

SWEP.IronsightTime = 0

function SWEP:GetViewModelPosition(pos, ang)
	local bIron = true
	local fIronTime = self.fIronTime or 0

	if (!bIron and fIronTime < CurTime() - self.IronsightTime) then
		return pos, ang
	end

	local Mul = 1.0

	if (fIronTime > CurTime() - self.IronsightTime) then
		Mul = math.Clamp((CurTime() - fIronTime) / self.IronsightTime, 0, 1)
		if (!bIron) then Mul = 1 - Mul end
	end

	local Offset = self.IronSightsPos

	if (self.IronSightsAng) then
		ang = ang * 1
		ang:RotateAroundAxis(ang:Right(), self.IronSightsAng.x * Mul)
		ang:RotateAroundAxis(ang:Up(), self.IronSightsAng.y * Mul)
		ang:RotateAroundAxis(ang:Forward(), self.IronSightsAng.z * Mul)
	end

	local Right = ang:Right()
	local Up = ang:Up()
	local Forward = ang:Forward()

	pos = pos + Offset.x * Right * Mul
	pos = pos + Offset.y * Forward * Mul
	pos = pos + Offset.z * Up * Mul

	return pos, ang
end