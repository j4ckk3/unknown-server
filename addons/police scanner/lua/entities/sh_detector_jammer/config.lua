SH_DETECTOR_JAMMER = {}

-- The jam range in units.
-- One meter is roughly 64 units
SH_DETECTOR_JAMMER.JamRange = 1200

-- Should the jammer play a radio-type sound?
SH_DETECTOR_JAMMER.PlaySound = true