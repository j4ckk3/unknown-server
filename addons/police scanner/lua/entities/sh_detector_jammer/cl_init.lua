include("config.lua")
include("shared.lua")

ENT.RenderGroup = RENDERGROUP_TRANSLUCENT

surface.CreateFont("SH_DETECTOR_JAMMER", {font = "Roboto", size = 300})

SH_JAMMERS = SH_JAMMERS or {}

function ENT:Initialize()
	SH_JAMMERS[tostring(self)] = self
end

function ENT:OnRemove()
	SH_JAMMERS[tostring(self)] = nil
end

function ENT:DrawTranslucent()
	self:DrawModel()

	local pos, ang = self:GetPos(), self:GetAngles()
	ang:RotateAroundAxis(ang:Up(), 90)
	cam.Start3D2D(pos + ang:Up() * 3.25, ang, 0.04)
		draw.SimpleTextOutlined(self.PrintName, "SH_DETECTOR_JAMMER", 0, 0, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 3, color_black)
	cam.End3D2D()

	ang:RotateAroundAxis(ang:Forward(), 180)
	cam.Start3D2D(pos + ang:Up() * 3.25, ang, 0.04)
		draw.SimpleTextOutlined(self.PrintName, "SH_DETECTOR_JAMMER", 0, 0, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 3, color_black)
	cam.End3D2D()
end