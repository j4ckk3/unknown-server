ENT.Type = "anim"

ENT.PrintName = "Jammer"
ENT.Category = "SH Entities"
ENT.Spawnable = true

for i = 1, 15 do
	util.PrecacheSound("ambient/levels/prison/radio_random" .. i .. ".wav")
end

-- Use this function to see if LocalPlayer() is within jam range
function SH_DETECTOR_JAMMER:IsJammed(pos)
	local rng = SH_DETECTOR_JAMMER.JamRange
	for k, v in pairs (SH_JAMMERS) do
		if (!IsValid(v)) then
			SH_JAMMERS[k] = nil
			continue
		end

		if (v:GetPos():Distance(pos) <= rng) then
			return true
		end
	end

	return false
end