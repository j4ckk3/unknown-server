AddCSLuaFile("shared.lua")
AddCSLuaFile("config.lua")
AddCSLuaFile("cl_init.lua")

include("config.lua")
include("shared.lua")

SH_JAMMERS = SH_JAMMERS or {}

function ENT:Initialize()
	self:SetModel("models/props_lab/reciever01a.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:PhysWake()

	self:SetHealth(100)
	SH_JAMMERS[tostring(self)] = self
end

function ENT:OnRemove()
	SH_JAMMERS[tostring(self)] = nil
end

function ENT:Think()
	if (SH_DETECTOR_JAMMER.PlaySound) then
		local snd = "ambient/levels/prison/radio_random" .. math.random(15) .. ".wav"
		self:EmitSound(snd, 70)
		self:NextThink(CurTime() + SoundDuration(snd))
		return true
	end
end

function ENT:OnTakeDamage(dmginfo)
	self:SetHealth(self:Health() - dmginfo:GetDamage())
	if (self:Health() <= 0) then
		local eff = EffectData()
		eff:SetOrigin(self:GetPos())
		util.Effect("ManhackSparks", eff, false, true)

		self:EmitSound("npc/scanner/cbot_energyexplosion1.wav")
		self:Remove()
	end
end