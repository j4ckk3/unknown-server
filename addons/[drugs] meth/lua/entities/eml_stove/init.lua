AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/props_c17/furnitureStove001a.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	self:SetHealth(EML_Stove_Health);
   
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);
   
	self:SetNWInt("distance", EML_DrawDistance);
   
	self:SetNWInt("stoveConsumption", EML_Stove_Consumption);
	self:SetNWInt("stoveHeat", EML_Stove_Heat);
   
	self:SetNWInt("gasStorage", EML_Stove_Storage);
	self:SetNWInt("gasStorageMax", EML_Stove_Storage);
   
	self:SetNWBool("firePlace1", false);
	self:SetNWBool("firePlace2", false);
	self:SetNWBool("firePlace3", false);
	self:SetNWBool("firePlace4", false);
	self:SetNWBool("explode", false);
   
	self:SetPos(self:GetPos()+Vector(0, 0, 32));
   
	if EML_Stove_GravityGun then
			self:GetPhysicsObject():SetMass(105);
	end;   
end;

function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("eml_stove");
	ent:SetPos(trace.HitPos + trace.HitNormal * 8);
	ent:Spawn();
	ent:Activate();
 
	return ent;
end;

function ENT:OnTakeDamage(dmginfo)
	if (EML_Stove_ExplosionType == 2) then
			self:SetHealth(self:Health()-dmginfo:GetDamage());
			if self:Health() <= dmginfo:GetDamage() then
					if !self:GetNWBool("explode") then
							self:SetNWBool("explode", true);
							self:Explode();
					end;
			end;
	elseif (EML_Stove_ExplosionType == 1) then
			self:SetHealth(self:Health()-dmginfo:GetDamage());
			if self:Health() <= dmginfo:GetDamage() then
					self:Remove();
			end;
	elseif (EML_Stove_ExplosionType == 0) then             
			return false;
	end;
end;

function ENT:Think()
	if (EML_Stove_ExplodeUnderwater) then
		if (self:WaterLevel() >= 2) then
			self:Explode();
		end;
	end;

	local traceF1 = {}     
	traceF1.start = self:GetPos()+(self:GetUp()*20)+(self:GetForward()*2.8)+(self:GetRight()*11.5);
	traceF1.endpos = self:GetPos()+(self:GetUp()*24)+(self:GetForward()*2.8)+(self:GetRight()*11.5);
	traceF1.filter = self;

	local traceF2 = {}     
	traceF2.start = self:GetPos()+(self:GetUp()*20)+(self:GetForward()*2.8)+(self:GetRight()*-11.2);
	traceF2.endpos = self:GetPos()+(self:GetUp()*24)+(self:GetForward()*2.8)+(self:GetRight()*-11.2);
	traceF2.filter = self;

	local traceF3 = {}     
	traceF3.start = self:GetPos()+(self:GetUp()*20)+(self:GetForward()*-9.8)+(self:GetRight()*-11.2);
	traceF3.endpos = self:GetPos()+(self:GetUp()*24)+(self:GetForward()*-9.8)+(self:GetRight()*-11.2);
	traceF3.filter = self;

	local traceF4 = {}     
	traceF4.start = self:GetPos()+(self:GetUp()*20)+(self:GetForward()*-9.8)+(self:GetRight()*11.5);
	traceF4.endpos = self:GetPos()+(self:GetUp()*24)+(self:GetForward()*-9.8)+(self:GetRight()*11.5);
	traceF4.filter = self;

	local traceFire1 = util.TraceLine(traceF1);
	local traceFire2 = util.TraceLine(traceF2);
	local traceFire3 = util.TraceLine(traceF3);
	local traceFire4 = util.TraceLine(traceF4);

	if ((!self.nextHeat or CurTime() >= self.nextHeat) and (self:GetNWInt("gasStorage")>0)) then   
		self:PotProgress(traceFire1.Entity, 1);
		self:PotProgress(traceFire2.Entity, 2);
		self:PotProgress(traceFire3.Entity, 3);
		self:PotProgress(traceFire4.Entity, 4);

	self.nextHeat = CurTime() + 1;
	end;
end;


function ENT:PotProgress(pot, num)
	if IsValid(pot) then  
		if ((((pot:GetClass() == "eml_pot") and ((pot:GetSulfur()>0) and (pot:GetMuriaticAcid()>0))))
		or (((pot:GetClass() == "eml_spot") and ((pot:GetRedp()>0) and (pot:GetCiodine()>0))))) then
			-- Consume gas storage and change pot time and status
			self:SetNWInt("gasStorage", math.Clamp(self:GetNWInt("gasStorage")-EML_Stove_Consumption, 0, self:GetNWInt("gasStorageMax")));
			pot:SetTime(math.Clamp(pot:GetTime()-1, 0, pot:GetMaxTime()));                
			if (pot:GetTime() <= 0) then
				pot:SetReady(true);
			end;

			-- Make a sound                                           
			local soundChance = math.random(1, 2);
			if soundChance == 2 then
					pot:EmitSound("ambient/levels/canals/toxic_slime_gurgle"..math.random(2, 8)..".wav", EML_Sound_Level, EML_Sound_Pitch, EML_Sound_Volume);
			end;
			self:SetNWBool("firePlace"..num, true);
		end;
	else
		self:SetNWBool("firePlace"..num, false);
	end;
end;


function ENT:Explode() 
	local explosionSize = EML_Stove_ExplosionDamage;
   
	local explosion = ents.Create("env_explosion");                        
	explosion:SetPos(self:GetPos());
	explosion:SetKeyValue("iMagnitude", explosionSize);
	explosion:Spawn();
	explosion:Activate();
	explosion:Fire("Explode", 0, 0);
   
	local shake = ents.Create("env_shake");
	shake:SetPos(self:GetPos());
	shake:SetKeyValue("amplitude", (explosionSize*2));
	shake:SetKeyValue("radius", explosionSize);
	shake:SetKeyValue("duration", "1.5");
	shake:SetKeyValue("frequency", "255");
	shake:SetKeyValue("spawnflags", "4");
	shake:Spawn();
	shake:Activate();
	shake:Fire("StartShake", "", 0);
   
	self:Remove();
end;