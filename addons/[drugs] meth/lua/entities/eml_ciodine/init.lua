AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/props_junk/rock001a.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);

	self:SetMaterial("models/shiny");
	self:SetColor(Color(220, 134, 159, 255));	
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);
	
	-- Old NWVars turned to new
	self:SetAmount(0);
	self:SetMaxAmount(0);

	if EML_NoCollide_Ingredients then
		self:SetCollisionGroup(COLLISION_GROUP_WEAPON);
	end;
end;
 
function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("eml_ciodine");
	ent:SetPos(trace.HitPos + trace.HitNormal * 16);
	ent:Spawn();
	ent:Activate();

	ent:SetAmount(1);
	ent:SetMaxAmount(1);

	return ent;
end;

function ENT:OnTakeDamage(dmginfo)
	self:VisualEffect();
end;

function ENT:VisualEffect()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
	self:Remove();
end;

