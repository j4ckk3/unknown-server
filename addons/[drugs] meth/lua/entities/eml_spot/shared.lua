ENT.Base = "base_gmodentity";
ENT.Type = "anim";

ENT.PrintName		= "Special Pot";
ENT.Category 		= "EML";
ENT.Author			= "EnnX49";

ENT.Contact    		= "";
ENT.Purpose 		= "";
ENT.Instructions 	= "" ;

ENT.Spawnable			= true;
ENT.AdminSpawnable		= true;

function ENT:SetupDataTables()
	self:NetworkVar("Int", 0, "Redp");
	self:NetworkVar("Int", 1, "Ciodine");

	self:NetworkVar("Int", 2, "Time");
	self:NetworkVar("Int", 3, "MaxTime");

	self:NetworkVar("Bool", 0, "Ready");
end;
