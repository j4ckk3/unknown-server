ENT.Base = "base_gmodentity";
ENT.Type = "anim";

ENT.PrintName		= "Iodine";
ENT.Category 		= "EML";
ENT.Author			= "EnnX49";

ENT.Contact    		= "";
ENT.Purpose 		= "";
ENT.Instructions 	= "" ;

ENT.Spawnable			= true;
ENT.AdminSpawnable		= true;

function ENT:SetupDataTables()
	self:NetworkVar("Int", 0, "Amount");
	self:NetworkVar("Int", 1, "MaxAmount");
end;