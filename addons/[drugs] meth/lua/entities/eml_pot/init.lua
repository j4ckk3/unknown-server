AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");


function ENT:Initialize()
	self:SetModel("models/props_c17/metalPot001a.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);

	self:SetMuriaticAcid(0);
	self:SetSulfur(0);

	self:SetTime(EML_Pot_StartTime);
	self:SetMaxTime(EML_Pot_StartTime);

	self:SetReady(false);
	self:SetPos(self:GetPos()+Vector(0, 0, 8));
	self.jailWall = true;

	if (EML_NoCollide_Cookware) then
		self:SetCollisionGroup(COLLISION_GROUP_WEAPON);
	end;
end;


function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("eml_pot");
	ent:SetPos(trace.HitPos + trace.HitNormal * 8);
	ent:Spawn();
	ent:Activate();
     
	return ent;
end;


function ENT:OnTakeDamage(dmginfo)
	self:VisualEffect();
	self:Remove()
end;


function ENT:Touch(touchEntity)
	-- Muriatic Acid
	if (touchEntity:GetClass() == "eml_macid") then
		if (self:GetMuriaticAcid() < EML_Pot_MuriaticAcid_Limit) and !self:GetReady() then
			if (touchEntity:GetAmount()>0) then
				touchEntity:SetAmount(math.Clamp(touchEntity:GetAmount() - 1, 0, touchEntity:GetMaxAmount()));
				if EML_Pot_DestroyEmpty then
					if (touchEntity:GetAmount() == 0) then	
						touchEntity:VisualEffect();
					end;		
				end;
				self:SetTime(self:GetTime()+EML_Pot_OnAdd_MuriaticAcid);
				self:SetMaxTime(self:GetMaxTime()+EML_Pot_OnAdd_MuriaticAcid);

				self:SetMuriaticAcid(self:GetMuriaticAcid() + 1);

				self:EmitSound("ambient/levels/canals/toxic_slime_sizzle3.wav", EML_Sound_Level, EML_Sound_Pitch, EML_Sound_Volume);
				self:VisualEffect();
			end;
		end;
	end;

	-- Sulfur
	if (touchEntity:GetClass() == "eml_sulfur") then
		if (self:GetSulfur() < EML_Pot_LiquidSulfur_Limit) and !self:GetReady() then
			if (touchEntity:GetAmount()>0) then
				touchEntity:SetAmount(math.Clamp(touchEntity:GetAmount() - 1, 0, touchEntity:GetMaxAmount()));
				if EML_Pot_DestroyEmpty then
					if (touchEntity:GetAmount() == 0) then	
						touchEntity:VisualEffect();
					end;		
				end;
				self:SetTime(self:GetTime()+EML_Sulfur_Amount);
				self:SetMaxTime(self:GetMaxTime()+EML_Sulfur_Amount);

				self:SetSulfur(self:GetSulfur() + 1);

				self:EmitSound("ambient/levels/canals/toxic_slime_sizzle3.wav", EML_Sound_Level, EML_Sound_Pitch, EML_Sound_Volume);
				self:VisualEffect();
			end;
		end;
	end;
end


function ENT:Use( activator, caller )
local curTime = CurTime();
	if (!self.nextUse or curTime >= self.nextUse) then
		
		if (self:GetReady() and (self:GetMuriaticAcid()>0) and (self:GetSulfur()>0)) then			
			local redpAmount = (self:GetMuriaticAcid()+self:GetSulfur());
		
			self:EmitSound("ambient/levels/canals/toxic_slime_sizzle2.wav", EML_Sound_Level, EML_Sound_Pitch, EML_Sound_Volume);

			self:SetMuriaticAcid(0);
			self:SetSulfur(0);

			self:SetTime(EML_Pot_StartTime);
			self:SetMaxTime(EML_Pot_StartTime);
			self:SetReady(false);			
			
			redP = ents.Create("eml_redp");
			redP:SetPos(self:GetPos()+self:GetUp()*12);
			redP:SetAngles(self:GetAngles());
			redP:Spawn();
			redP:GetPhysicsObject():SetVelocity(self:GetUp()*2);

			redP:SetAmount(redpAmount);
			redP:SetMaxAmount(redpAmount);			
		end;
		
		self.nextUse = curTime + 0.5;
	end;
end;


function ENT:VisualEffect()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
end;