ENT.Base = "base_gmodentity";
ENT.Type = "anim";

ENT.PrintName		= "Pot";
ENT.Category 		= "EML";
ENT.Author			= "EnnX49";

ENT.Contact    		= "";
ENT.Purpose 		= "";
ENT.Instructions 	= "" ;

ENT.Spawnable			= true;
ENT.AdminSpawnable		= true;

-- "String", "Bool", "Float", "Int", "Vector", "Angle", "Entity"
function ENT:SetupDataTables()
	self:NetworkVar("Int", 0, "MuriaticAcid");
	self:NetworkVar("Int", 1, "Sulfur");

	self:NetworkVar("Int", 2, "Time");
	self:NetworkVar("Int", 3, "MaxTime");

	self:NetworkVar("Bool", 0, "Ready");
end;
