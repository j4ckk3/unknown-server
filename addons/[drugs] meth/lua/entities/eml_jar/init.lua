AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");


function ENT:Initialize()
	self:SetModel("models/props_lab/jar01a.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);

	self:SetEML_Jar_Macid(0);	
	self:SetEML_Jar_Iodine(0);
	self:SetEML_Jar_Water(0);

	self:SetEML_Jar_Progress(EML_Jar_StartProgress);
	self:SetEML_Jar_Ready(false);

	self:SetPos(self:GetPos()+Vector(0, 0, 8));	
	self.jailWall = true;

	if EML_NoCollide_Cookware then
		self:SetCollisionGroup(COLLISION_GROUP_WEAPON);
	end;
end;


function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("eml_jar");
	ent:SetPos(trace.HitPos + trace.HitNormal * 8);
	ent:Spawn();
	ent:Activate();
     
	return ent;
end;

function ENT:OnTakeDamage(dmginfo)
	self:VisualEffect();
	self:Remove()
end;


function ENT:Touch(touchEntity)
	-- Muriatic Acid
	if (touchEntity:GetClass() == "eml_macid") then
		if (self:GetEML_Jar_Macid() < EML_Jar_MuriaticAcid_Limit) and !self:GetEML_Jar_Ready() then
			if (touchEntity:GetAmount()>0) then
				touchEntity:SetAmount(math.Clamp(touchEntity:GetAmount() - 1, 0, touchEntity:GetMaxAmount()));
				if EML_Jar_DestroyEmpty then
					if (touchEntity:GetAmount() == 0) then	
						touchEntity:VisualEffect();
					end;		
				end;

				self:SetEML_Jar_Macid(self:GetEML_Jar_Macid() + 1);
				self:EmitSound("ambient/levels/canals/toxic_slime_sizzle3.wav", EML_Sound_Level, EML_Sound_Pitch, EML_Sound_Volume);
				self:VisualEffect();
			end;
		end;
	end;

	-- Iodine
	if (touchEntity:GetClass() == "eml_iodine") then
		if (self:GetEML_Jar_Iodine() < EML_Jar_Iodine_Limit) and !self:GetEML_Jar_Ready() then
			if (touchEntity:GetAmount()>0) then
				touchEntity:SetAmount(math.Clamp(touchEntity:GetAmount() - 1, 0, touchEntity:GetMaxAmount()));
				if EML_Jar_DestroyEmpty then
					if (touchEntity:GetAmount() == 0) then	
						touchEntity:VisualEffect();
					end;		
				end;

				self:SetEML_Jar_Iodine(self:GetEML_Jar_Iodine() + 1);
				self:EmitSound("ambient/levels/canals/toxic_slime_sizzle3.wav", EML_Sound_Level, EML_Sound_Pitch, EML_Sound_Volume);
				self:VisualEffect();
			end;
		end;
	end;

	-- Water
	if (touchEntity:GetClass() == "eml_water") then
		if (self:GetEML_Jar_Water() < EML_Jar_Water_Limit) and !self:GetEML_Jar_Ready() then
			if (touchEntity:GetAmount()>0) then
				touchEntity:SetAmount(math.Clamp(touchEntity:GetAmount() - 1, 0, touchEntity:GetMaxAmount()));
				if EML_Jar_DestroyEmpty then
					if (touchEntity:GetAmount() == 0) then	
						touchEntity:VisualEffect();
					end;		
				end;

				self:SetEML_Jar_Water(self:GetEML_Jar_Water() + 1);
				self:EmitSound("ambient/levels/canals/toxic_slime_sizzle3.wav", EML_Sound_Level, EML_Sound_Pitch, EML_Sound_Volume);
				self:VisualEffect();
			end;
		end;
	end;	
end


function ENT:Think()
	local progressTime = CurTime();

	if ((!self.progressTime or CurTime() >= self.progressTime) and (self:GetEML_Jar_Macid()>0) and (self:GetEML_Jar_Iodine()>0) and (self:GetEML_Jar_Water()>0)) then
		if (self:GetEML_Jar_Progress() != 100) then
			if ((self:GetVelocity():Length() > EML_Jar_MinShake) and (self:GetVelocity():Length() < EML_Jar_MaxShake)) then
				self:SetEML_Jar_Progress(math.Clamp(self:GetEML_Jar_Progress() + EML_Jar_CorrectShake, 0, 100));
				self:EmitSound("ambient/levels/canals/toxic_slime_sizzle4.wav", EML_Sound_Level, 200, EML_Sound_Volume);
			elseif (self:GetVelocity():Length() > EML_Jar_MaxShake) then
				self:SetEML_Jar_Progress(math.Clamp(self:GetEML_Jar_Progress() - EML_Jar_WrongShake, 0, 100));
				self:EmitSound("ambient/levels/canals/toxic_slime_sizzle4.wav", EML_Sound_Level, 150, EML_Sound_Volume);			
			end;
		elseif (self:GetEML_Jar_Progress() == 100) then		
			self:SetEML_Jar_Ready(true);
		end;
	self.progressTime = CurTime() + 0.5;
	end;
end;


function ENT:Use(activator, caller)
local curTime = CurTime();
	if (!self.nextUse or curTime >= self.nextUse) then
		
		if (self:GetEML_Jar_Ready() and (self:GetEML_Jar_Macid()>0) and (self:GetEML_Jar_Iodine()>0) and (self:GetEML_Jar_Water()>0)) then			
			local ciodineAmount = math.Round((self:GetEML_Jar_Macid()+self:GetEML_Jar_Iodine()+self:GetEML_Jar_Water())/2);
		
			self:EmitSound("ambient/levels/canals/toxic_slime_sizzle2.wav", EML_Sound_Level, EML_Sound_Pitch, EML_Sound_Volume);
			self:SetEML_Jar_Macid(0);			
			self:SetEML_Jar_Iodine(0);
			self:SetEML_Jar_Water(0);

			self:SetEML_Jar_Progress(EML_Jar_StartProgress);
			self:SetEML_Jar_Ready(false);			
			
			redP = ents.Create("eml_ciodine");
			redP:SetPos(self:GetPos()+self:GetUp()*12);
			redP:SetAngles(self:GetAngles());
			redP:Spawn();
			redP:GetPhysicsObject():SetVelocity(self:GetUp()*2);

			redP:SetAmount(ciodineAmount);
			redP:SetMaxAmount(ciodineAmount);			
		end;
		
		self.nextUse = curTime + 0.5;
	end;
end;

function ENT:VisualEffect()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
end;
