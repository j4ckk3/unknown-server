ENT.Base = "base_gmodentity";
ENT.Type = "anim";

ENT.PrintName		= "Jar";
ENT.Category 		= "EML";
ENT.Author			= "EnnX49";

ENT.Contact    		= "";
ENT.Purpose 		= "";
ENT.Instructions 	= "" ;

ENT.Spawnable			= true;
ENT.AdminSpawnable		= true;

function ENT:SetupDataTables()
	self:NetworkVar("Int", 0, "EML_Jar_Macid");
	self:NetworkVar("Int", 1, "EML_Jar_Iodine");
	self:NetworkVar("Int", 2, "EML_Jar_Water");

	self:NetworkVar("Int", 3, "EML_Jar_Progress");

	self:NetworkVar("Bool", 0, "EML_Jar_Ready");
end;