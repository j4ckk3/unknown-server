ENT.Base = "base_gmodentity";
ENT.Type = "anim";

ENT.PrintName		= "Gas";
ENT.Category 		= "EML";
ENT.Author			= "EnnX49";

ENT.Contact    		= "";
ENT.Purpose 		= "";
ENT.Instructions 	= "" ;

ENT.Spawnable			= true;
ENT.AdminSpawnable		= true;

function ENT:SetupDataTables()
	self:NetworkVar("Int", 0, "Amount");
	self:NetworkVar("Int", 1, "MaxAmount");

	self:NetworkVar("Bool", 0, "Open");
	self:NetworkVar("Bool", 1, "Exploded");
end;