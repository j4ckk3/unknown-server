TEAM_EML_COOK = DarkRP.createJob("Meth Cook", {
	color = Color(0, 128, 255, 255),
	model = {"models/player/hostage/hostage_01.mdl"},
	description = [[The lowest person of crime.
		A meth cook generally works a gang to sell meth.]],
	weapons = {},
	command = "methcook",
	max = 4,
	salary = 45,
	admin = 0,
	vote = false,
	hasLicense = false,
	category="Gangsters"
})

DarkRP.createCategory{
	name = "Meth Goods",
	categorises = "entities",
	startExpanded = true,
	color = Color(12, 208, 219, 255),
	canSee = function(ply)
		return (ply:Team() == TEAM_EML_COOK)
	end,
	sortOrder = 90
}

DarkRP.createEntity("Gas Canister", {
	ent = "eml_gas",
	model = "models/props_c17/canister01a.mdl",
	price = 600,
	max = 20,
	cmd = "buygascanister_eml",
	category = "Meth Goods",
	allowed = TEAM_EML_COOK
})

DarkRP.createEntity("Liquid Iodine", {
	ent = "eml_iodine",
	model = "models/props_lab/jar01b.mdl",
	price = 40,
	max = 20,
	cmd = "buyiodine_eml",
	category = "Meth Goods",
	allowed = TEAM_EML_COOK
})

DarkRP.createEntity("Jar", {
	ent = "eml_jar",
	model = "models/props_lab/jar01a.mdl",
	price = 1200,
	max = 20,
	cmd = "buyjar_eml",
	category = "Meth Goods",
	allowed = TEAM_EML_COOK	
})

DarkRP.createEntity("Muriatic Acid", {
	ent = "eml_macid",
	model = "models/props_junk/garbage_plasticbottle001a.mdl",
	price = 40,
	max = 20,
	cmd = "buymacid_eml",
	category = "Meth Goods",
	allowed = TEAM_EML_COOK
})

DarkRP.createEntity("Pot", {
	ent = "eml_pot",
	model = "models/props_c17/metalPot001a.mdl",
	price = 800,
	max = 20,
	cmd = "buypot_eml",
	category = "Meth Goods",
	allowed = TEAM_EML_COOK
})

DarkRP.createEntity("Special Pot", {
	ent = "eml_spot",
	model = "models/props_c17/metalPot001a.mdl",
	price = 1200,
	max = 20,
	cmd = "buyspot_eml",
	category = "Meth Goods",
	allowed = TEAM_EML_COOK
})

DarkRP.createEntity("Stove", {
	ent = "eml_stove",
	model = "models/props_c17/furnitureStove001a.mdl",
	price = 3200,
	max = 5,
	cmd = "buystove_eml",
	category = "Meth Goods",
	allowed = TEAM_EML_COOK
})

DarkRP.createEntity("Liquid Sulfur", {
	ent = "eml_sulfur",
	model = "models/props_lab/jar01b.mdl",
	price = 20,
	max = 20,
	cmd = "buysulfur_eml",
	category = "Meth Goods",
	allowed = TEAM_EML_COOK
})

DarkRP.createEntity("Water", {
	ent = "eml_water",
	model = "models/props_junk/garbage_plasticbottle003a.mdl",
	price = 20,
	max = 20,
	cmd = "buywater_eml",
	category = "Meth Goods",
	allowed = TEAM_EML_COOK
})