opium = {}
opium.ahshop = {}

-- If you want to sell the opium by clicking E on pack, then set it to true.
opium.ahshop.SellOpiumWithoutNpc = false

-- Opium values.
opium.ahshop.LowOpiumPrice = 1000
opium.ahshop.mediumOpiumPrice = 2000
opium.ahshop.PremiumOpiumPrice = 3000

-- Packer config:
opium.ahshop.PackerColor = Color(33,33,33)

-- Opium npc config:
opium.ahshop.BuyDistance = 200
opium.ahshop.NpcModel = "models/gman_high.mdl"

-- Health config:
opium.ahshop.PackerHealth = 100
opium.ahshop.BarrelHealth = 100
opium.ahshop.BottleHealth = 100
opium.ahshop.CodeineHealth = 100
opium.ahshop.PackedHealth = 100
opium.ahshop.PapaverineHealth = 100
opium.ahshop.SulfateHealth = 100
opium.ahshop.GasHealth = 100
opium.ahshop.WaterHealth = 100
opium.ahshop.HeaterHealth = 100

-- Heater config:
opium.ahshop.HeaterColor = Color(33,33,33)
opium.ahshop.HeaterSound = "ambient/machines/electric_machine.wav"
opium.ahshop.HeaterCookTime = 100 -- The higher number, more time it will take to cook.