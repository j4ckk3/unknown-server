local function SaveSimpleOpium(ply, cmd, args)
    
    if ply:IsSuperAdmin() then
   
        local opium = {}
       
        for k,v in pairs( ents.FindByClass("the_opium_buyer") ) do
            opium[k] = { type = v:GetClass(), pos = v:GetPos(), ang = v:GetAngles() }
        end	
       
        local convert_data = util.TableToJSON( opium )		
   
        file.Write( "opium/simple.txt", convert_data )
   
    end
end
concommand.Add("save_opium", SaveSimpleOpium)

local function DeleteSimpleOpium(ply, cmd, args)
   
    if ply:IsSuperAdmin() then
       
        file.Delete( "opium/simple.txt" )
        
    end    
end
concommand.Add("delete_opium", DeleteSimpleOpium)

local function SpawnSimpleOpium(ply, cmd, args)
    if ply:IsSuperAdmin() then
	
        local spawnopium = ents.Create( "the_opium_buyer" )
        if ( !IsValid( spawnopium ) ) then return end
        spawnopium:SetPos( ply:GetPos() + (ply:GetForward() * 100) )
        spawnopium:Spawn()
		
    end    
end
concommand.Add("spawn_opium", SpawnSimpleOpium)

local function RespawnSimpleOpium()
 
    if not file.IsDir( "opium", "DATA" ) then
 
        file.CreateDir( "opium", "DATA" )
   
    end
	
	if not file.Exists("opium/simple.txt","DATA") then return end
 
    local ImportData = util.JSONToTable(file.Read("opium/simple.txt","DATA"))
   
    for k, v in pairs(ImportData) do
       
        local npc = ents.Create( v.type )
        npc:SetPos( v.pos )
        npc:SetAngles( v.ang )
        npc:Spawn()
     
	end
end
hook.Add( "InitPostEntity", "simple_opium", RespawnSimpleOpium )