AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel(self.Model)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetCollisionGroup(COLLISION_GROUP_NONE)
	self.CanHurt = true
	local phys = self:GetPhysicsObject()

	if phys and phys:IsValid() then
		phys:Wake()
	end

	self.dt.ammoCharge = self.AmmoCapacity

	self.HP = self.HealthAmount
end

local dmg, wep, am, cl, ammo, ED, pos, mag, amountToGive, maxAmmo

function ENT:OnTakeDamage(dmginfo)
	if self.Exploded then
		return
	end

	self:GetPhysicsObject():AddVelocity(dmginfo:GetDamageForce() * 0.02)

	dmg = dmginfo:GetDamage()
	self.HP = self.HP - dmg

	if self.HP <= 0 then
		self.Exploded = true

		pos = self:GetPos()
		util.BlastDamage(dmginfo:GetInflictor(), dmginfo:GetAttacker(), pos + Vector(0, 0, 32), self.ExplodeRadius, self.ExplodeDamage)

		ED = EffectData()
		ED:SetOrigin(pos)
		ED:SetScale(1)

		util.Effect("Explosion", ED)
		SafeRemoveEntity(self)
	end
end


function ENT:Use(activator, caller)
	--print("hi")
	if self.CaliberSpecific then
		aprint("1")

		if self.dt.ammoCharge <= 0 then
			SafeRemoveEntity(self)
		end


		if activator:IsPlayer() and activator:Alive() then
			if not activator.AmmoGiveDelay or CurTime() > activator.AmmoGiveDelay then
				activator:GiveAmmo(self.ResupplyAmount, self.Caliber)

				activator.AmmoGiveDelay = CurTime() + self.ResupplyTime+30
				self.dt.ammoCharge = self.dt.ammoCharge - self.ResupplyAmount
			end
		end
	else

		if activator:IsPlayer() and activator:Alive() then

			--if self.AmmoCharge > 0 then
				wep = activator:GetActiveWeapon()
				cl = wep:GetClass()
				
				if !string.find(cl, "fas2") then return end

				if IsValid(wep) then

					am = wep:GetPrimaryAmmoType()

					if am != -1 then

						amc = activator:GetAmmoCount(am)

						if wep.Primary and wep.Primary.ClipSize then

							mag = wep:Clip1()

							if math.Round(wep.Primary.ClipSize * 12 * (wep.MaxAmmoMod and wep.MaxAmmoMod or 1)) + math.Clamp(wep.Primary.ClipSize - mag, 0, wep.Primary.ClipSize) > amc then

								--self.AmmoCharge = self.AmmoCharge - 1
								self.dt.ammoCharge = self.dt.ammoCharge - 1
								activator:EmitSound("items/ammo_pickup.wav", 60, 100)

								ammo = math.Clamp(amc + (wep.Primary.ClipSize > 50 and wep.Primary.ClipSize / 2 or wep.Primary.ClipSize) * (wep.GiveAmmoMod and wep.GiveAmmoMod or 1), 0, math.Round(wep.Primary.ClipSize * 12 * (wep.MaxAmmoMod and wep.MaxAmmoMod or 1)) + math.Clamp(wep.Primary.ClipSize - mag, 0, wep.Primary.ClipSize))
								activator:SetAmmo(ammo, am)
							end
						end
					end

					
					for k2, v2 in ipairs(activator:GetWeapons()) do
						am = v2:GetPrimaryAmmoType()
						amc = activator:GetAmmoCount(am)

						if amc == 0 and v2:Clip1() == 0 and cl != v2:GetClass() then
							if v2.Primary and v2.Primary.ClipSize then
								activator:SetAmmo(v2.Primary.ClipSize * 0.5, am)
							else
								activator:SetAmmo(15, am)
							end
						end
					end

					if wep.Secondary and wep.Secondary.Ammo != "none" and activator:GetAmmoCount(wep.Secondary.Ammo) < 12 then
						activator:GiveAmmo(1, wep.Secondary.Ammo)
						self.dt.ammoCharge = self.dt.ammoCharge - 1
					end
				end
			--else
				--SafeRemoveEntity(self)
			--end
		end
	end

	if self.dt.ammoCharge <= 0 then
		self:Remove()
	end

end
