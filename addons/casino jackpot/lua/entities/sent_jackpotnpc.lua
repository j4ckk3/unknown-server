AddCSLuaFile()
ENT.Type = "anim"
ENT.PrintName = "Casino Jackpot NPC"
ENT.Author = "KappaJ"
ENT.Category = "Fun + Games"
ENT.Editable = true
ENT.Spawnable = true
ENT.AdminOnly = true
ENT.RenderGroup = RENDERGROUP_BOTH
ENT.AutomaticFrameAdvance = true

--[[---------------------------------------------------------
	Name: Initialize
-----------------------------------------------------------]]
function ENT:Initialize()
	if (CLIENT) then return end
	self:SetModel("models/player/group01/male_0" .. math.random(1, 9) .. ".mdl")
	self:SetUseType(SIMPLE_USE)
	self:SetMoveType(MOVETYPE_NONE)
	self:SetSolid(SOLID_BBOX)
	self:PhysicsInit(SOLID_BBOX)
	local physObj = self:GetPhysicsObject()

	if (IsValid(physObj)) then
		physObj:EnableMotion(false)
		physObj:Sleep()
	end

	self:DropToFloor()
	self:ResetSequence(2)
	self:SetFlexScale(1)
	self.Key = #ents.FindByClass("sent_jackpotnpc")

	if SERVER then
		file.CreateDir'csgo_jackpot'

		if not file.Exists('csgo_jackpot/' .. string.lower(game.GetMap()) .. '.txt', 'DATA') then
			local __vector = self:GetPos()
			local __angle = self:GetAngles()

			local __table = {
				[0] = {
					["vector"] = __vector,
					["angle"] = __angle
				}
			}

			file.Write("csgo_jackpot/" .. string.lower(game.GetMap()) .. ".txt", util.TableToJSON(__table))
		else
			local __locations = file.Read("csgo_jackpot/" .. string.lower(game.GetMap()) .. ".txt", "DATA")
			local __table = util.JSONToTable(__locations)
			local __vector = self:GetPos()
			local __angle = self:GetAngles()

			__table[table.Count(__table)] = {
				["vector"] = __vector,
				["angle"] = __angle
			}

			file.Write("csgo_jackpot/" .. string.lower(game.GetMap()) .. ".txt", util.TableToJSON(__table))
		end
	end
end

function ENT:Use(_Player)
	net.Start("CSGO.Jackpot.OpenMenu")
	net.Send(_Player)
end

function ENT:Draw()
	self:DrawModel()
end

if (SERVER) then return end