if not Casino_Jackpot then --Prevents users from breaking things when saving this!
	Casino_Jackpot = {}
end

Casino_Jackpot.Config = {}
Casino_Jackpot.Config.RequiredPlayers = 2 --Amount of players required to start game
Casino_Jackpot.Config.MaxJackpot = 125000 --Maximum value of the entire pot
Casino_Jackpot.Config.MaxCashBet = 25000 --Maximum multiplier allowed to bet on
Casino_Jackpot.Config.MinCashBet = 10 --Minimum multiplier allowed to bet on
Casino_Jackpot.Config.CountdownTimer = 30 --Seconds after jackpot limit is reached to begin drawing
Casino_Jackpot.Config.Language = "English" --Currently supports English and French. May/can be expanded by adding a third table (Instructions below)
Casino_Jackpot.Config.CurrencySymbol = HyplexCurrency --Symbol used to represent the currency (£, €, etc)
Casino_Jackpot.Config.TimeBeforeClear = 15 --Amount of time (in seconds) after a game is over to reset the screen
Casino_Jackpot.Config.MinPlayerCountdown = 0 --Amount of betters until timer automatically starts counting down -- use 0 to disable this!
Casino_Jackpot.Config.MinBetCountdown = 55000 --Cash until timer automatically starts -- use 0 to disable this!
Casino_Jackpot.Config.CashType = "DarkRP" --Available types are "DarkRP", "Pointshop", "Pointshop2_Standard", "Pointshop2_Premium" Anything else is defaulted to DarkRP. You can manually edit currency functions on line 92 of the server.lua file...
Casino_Jackpot.Config.AnnouceWins = true --Announce in chat when a player has won a jackpot
Casino_Jackpot.Config.AnnounceToPlayersOnly = false --Annouce only to people who have bet/put money into the pot
Casino_Jackpot.Config.CommandsEnabled = true --Whether the /jackpot and !jackpot commands should activate the menu. (If you only want people to use the NPC, I suggest setting this to false)
Casino_Jackpot.Config.UsergroupRestrictions = {} --Leaving this blank disables this feature. Usage: {"owner", "superadmin"} etc

Casino_Jackpot.Config.Translations = {}
Casino_Jackpot.Config.Translations["English"] = {
	["vgui.casinojackpot.notenough"] = " You don't have enough money!",
	["vgui.casinojackpot.placebet"] = " PLACE BET",
	["vgui.casinojackpot.larger"] = "You cannot bet amounts larger than",
	["vgui.casinojackpot.lower"] = "or lower than",
	["vgui.casinojackpot.placedbet"] = "Placed a bet valued at:",
	["vgui.casinojackpot.betamount"] = "Bet Amount:",
	["vgui.casinojackpot.circleavatars"] = "Rounded avatars",
	["vgui.casinojackpot.wonof"] = "has won the jackpot of",
	["vgui.casinojackpot.witha"] = "with a",
	["vgui.casinojackpot.percentchange"] = "% chance!",
	["vgui.casinojackpot.congrats"] = "Congrats! You've won the jackpot of",
	["vgui.casinojackpot.maximumbet"] = "The maximum bet is %s per player!",
	["vgui.casinojackpot.potoverflow"] = "The jackpot has a maximum of %s!",
	["vgui.casinojackpot.notallowed"]  = "Sorry! Your usergroup isn't whitelisted for this!",
}

Casino_Jackpot.Config.Translations["French"] = {
    ["vgui.casinojackpot.notenough"] = "Vous n'avez pas assez d'argent!",
    ["vgui.casinojackpot.placebet"] = "PLACER UN PARI",
    ["vgui.casinojackpot.larger"] = "Vous ne pouvez pas parier plus que",
    ["vgui.casinojackpot.lower"] = "ou moins de",
    ["vgui.casinojackpot.placedbet"] = "A placé un pari à :",
    ["vgui.casinojackpot.betamount"] = "Somme du pari :",
    ["vgui.casinojackpot.circleavatars"] = "Avatars arrondis",
    ["vgui.casinojackpot.wonof"] = "a gagné le jackpot de",
    ["vgui.casinojackpot.witha"] = "avec une chance de",
    ["vgui.casinojackpot.percentchange"] = "% !",
    ["vgui.casinojackpot.congrats"] = "Félicitations ! Vous avez gagné le jackpot de",
    ["vgui.casinojackpot.maximumbet"] = "Le pari maximum est de %s par joueur !",
    ["vgui.casinojackpot.potoverflow"] = "Le jackpot maximal est %s !",
    ["vgui.casinojackpot.notallowed"]  = "Désolé ! Votre groupe n'est pas autorisé à jouer !",
}

-- Translating is as simple as changing these values.
-- I'd recommend keeping the English and creating a new table instead of overwriting!
-- Wherever "%s" appears, must be kept regardless of the language. Otherwise, the strings won't work correctly. (%s represents a value that will be imported later)

--[[

Casino_Jackpot.Config.Translations["English"] = {
	["vgui.casinojackpot.notenough"] = " You don't have enough money!",
	["vgui.casinojackpot.placebet"] = " PLACE BET",
	["vgui.casinojackpot.larger"] = "You cannot bet amounts larger than",
	["vgui.casinojackpot.lower"] = "or lower than",
	["vgui.casinojackpot.placedbet"] = "Placed a bet valued at:",
	["vgui.casinojackpot.betamount"] = "Bet Amount:",
	["vgui.casinojackpot.circleavatars"] = "Rounded avatars",
	["vgui.casinojackpot.wonof"] = "has won the jackpot of",
	["vgui.casinojackpot.witha"] = "with a",
	["vgui.casinojackpot.percentchange"] = "% chance!",
	["vgui.casinojackpot.congrats"] = "Congrats! You've won the jackpot of",
	["vgui.casinojackpot.maximumbet"] = "The maximum bet is %s per player!",
	["vgui.casinojackpot.potoverflow"] = "The jackpot has a maximum of %s!",
	["vgui.casinojackpot.notallowed"]  = "Sorry! Your usergroup isn't whitelisted for this!",
}

]]--