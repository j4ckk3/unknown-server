if not CSGO_Jackpot then
	CSGO_Jackpot = {}
end

Casino_Jackpot.BetTable = {}

net.Receive("CSGO.Jackpot.ReceivePlayers", function()
	timer.Remove("CSGO_Jackpot.WaitBuffer")
	Casino_Jackpot.BetTable = net.ReadTable()

	if CSGO_Jackpot["Main"] and CSGO_Jackpot["Main"]:IsValid() then
		if table.Count(Casino_Jackpot.BetTable) <= 0 then
			if CSGO_Jackpot["Main"].SCLR then
				CSGO_Jackpot.CreateScroller()
			end

			CSGO_Jackpot["Main"].SCLR.DrawWinner = nil
			CSGO_Jackpot["Main"].betterLog:Clear()
			CSGO_Jackpot["Main"].playersPanel:Clear()
			CSGO_Jackpot["NumWang"]:SetValue(0)
		end

		if CSGO_Jackpot["Main"].moneyBar then
			CSGO_Jackpot["Main"].moneyBar.CurVal = 0
		end

		for k, v in SortedPairsByValue(Casino_Jackpot.BetTable, true) do
			CSGO_Jackpot.AddPlayer(k, v)
		end

		CSGO_Jackpot["Main"].betterLog.evnt = 0

		for k, v in pairs(CSGO_Jackpot["Main"].events) do
			v:Remove()
			v = nil
		end

		for k, v in SortedPairsByValue(Casino_Jackpot.BetTable) do
			local get_info = steamworks.RequestPlayerInfo(util.SteamIDTo64(k))

			if CSGO_Jackpot["Main"].events[k] and CSGO_Jackpot["Main"].events[k]:IsValid() then
				CSGO_Jackpot["Main"].events[k].Amount = v
				continue
			end

			CSGO_Jackpot["Main"].events[k] = vgui.Create("DPanel", CSGO_Jackpot["Main"].betterLog)
			CSGO_Jackpot["Main"].events[k]:SetSize(496, 65)
			CSGO_Jackpot["Main"].events[k]:SetPos(2, 75 * CSGO_Jackpot["Main"].betterLog.evnt)
			CSGO_Jackpot["Main"].events[k].Amount = v or 0

			CSGO_Jackpot["Main"].events[k].Paint = function(self, w, h)
				draw.RoundedBox(0, 0, 0, w, h, Color(32, 40, 50))
				draw.SimpleText(steamworks.GetPlayerName(util.SteamIDTo64(k)), "Jackpot.Fonts.20", w / 2, 20, Color(255, 172, 39), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
				draw.SimpleText(language.GetPhrase("vgui.casinojackpot.placedbet") .. " " .. Casino_Jackpot.Config.CurrencySymbol .. string.Comma(CSGO_Jackpot["Main"].events[k].Amount), "Jackpot.Fonts.20", w / 2 - 16, 50, Color(255, 172, 39), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			end

			CSGO_Jackpot["Main"].betterLog:ScrollToChild(CSGO_Jackpot["Main"].events[k])
			CSGO_Jackpot["Main"].betterLog.evnt = CSGO_Jackpot["Main"].betterLog.evnt + 1
		end
	end

	local time_left = net.ReadDouble()
	timer.Create("CSGO_Jackpot.WaitBuffer", time_left, 1, function() end)
end)

net.Receive("CSGO.Jackpot.WinnerWinnerChickenDinner", function()
	Casino_Jackpot.BetTable = net.ReadTable()
	CSGO_Jackpot.LoadScroller()
	CSGO_Jackpot.NotifyOnce = false

	if CSGO_Jackpot["Main"] and CSGO_Jackpot["Main"]:IsValid() then
		CSGO_Jackpot["Main"].SCLR.Rand = math.random(10, 50)
		CSGO_Jackpot.ScrollToWinner(net.ReadString(), net.ReadInt(32))
	end
end)

function CSGO_Jackpot.GetPercentage(_Table, _Key)
	local int = 0

	for k, v in pairs(_Table) do
		int = int + v
	end

	if not _Table[_Key] then return 0 end

	return _Table[_Key] / int
end

function CSGO_Jackpot.GetPercentage(_Table, _Key)
	local int = 0

	for k, v in pairs(_Table) do
		int = int + v
	end

	if not _Table[_Key] then return 0 end

	return _Table[_Key] / int
end

function CSGO_Jackpot.CreateScroller()
	if CSGO_Jackpot["Main"].SCLR then
		CSGO_Jackpot["Main"].SCLR:Remove()
	end

	CSGO_Jackpot["Main"].SCLR = vgui.Create("Jackpot.HorizontalScroller", CSGO_Jackpot["Main"])
	CSGO_Jackpot["Main"].SCLR:SetSize(1000, 64)
	CSGO_Jackpot["Main"].SCLR:SetPos(CSGO_Jackpot["Main"]:GetWide() / 2 - 500, 520)
	CSGO_Jackpot["Main"].SCLR:SetOverlap(-20)
	CSGO_Jackpot["Main"].SCLR.Rand = math.random(10, 50)
	CSGO_Jackpot["Main"].SCLR.DrawWinner = false
	CSGO_Jackpot["Main"].SCLR.btnLeft.Paint = function() return end
	CSGO_Jackpot["Main"].SCLR.btnRight.Paint = function() return end
	CSGO_Jackpot["Main"].SCLR.DOT = vgui.Create("DPanel", CSGO_Jackpot["Main"])
	CSGO_Jackpot["Main"].SCLR.DOT:SetSize(2, 64)
	CSGO_Jackpot["Main"].SCLR.DOT:SetPos(CSGO_Jackpot["Main"]:GetWide() / 2 - 1, 520)

	CSGO_Jackpot["Main"].SCLR.DOT.Paint = function(self, w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(255, 172, 39))
	end
end

DImage = {}

function CSGO_Jackpot.LoadScroller()
	if not CSGO_Jackpot["Main"] or not CSGO_Jackpot["Main"]:IsValid() or not CSGO_Jackpot["Main"].SCLR then return end

	for i = 0, 30 do
		for k, v in RandomPairs(Casino_Jackpot.BetTable) do
			DImage[i .. "-" .. k] = vgui.Create("Jackpot.AvatarSquare", CSGO_Jackpot["Main"].SCLR)
			DImage[i .. "-" .. k]:SetSize(64, 64)
			DImage[i .. "-" .. k]:SetSteamID(util.SteamIDTo64(k))
			DImage[i .. "-" .. k].SteamID = k
			local func = DImage[i .. "-" .. k].Paint
			steamworks.RequestPlayerInfo(util.SteamIDTo64(k))

			DImage[i .. "-" .. k].Paint = function(self, w, h)
				local __check = CSGO_Jackpot["Main"].SCLR.OffsetX - self:GetPos()
				if __check < -999 then return end
				func(self, w, h)
			end

			CSGO_Jackpot["Main"].SCLR:AddPanel(DImage[i .. "-" .. k])
		end
	end
end

local AvatarPanels = {}

function CSGO_Jackpot.AddPlayer(ID, BetAmnt)

	if not CSGO_Jackpot["Main"] or not CSGO_Jackpot["Main"]:IsValid() then return end

	if AvatarPanels[ID] then
		AvatarPanels[ID]:Remove()
	end

	CSGO_Jackpot["Main"].moneyBar.CurVal = CSGO_Jackpot["Main"].moneyBar.CurVal + BetAmnt
	if table.Count(Casino_Jackpot.BetTable) >= Casino_Jackpot.Config.RequiredPlayers then end
	AvatarPanels[ID] = vgui.Create("Jackpot.AvatarSquare", CSGO_Jackpot["Main"].playersPanel)
	AvatarPanels[ID]:SetSize(64, 64)
	AvatarPanels[ID]:SetSteamID(util.SteamIDTo64(ID), 64)

	AvatarPanels[ID].CustomPaint = function(self, w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 230))
		draw.SimpleText(math.Round(CSGO_Jackpot.GetPercentage(Casino_Jackpot.BetTable, ID) * 100, 2) .. "%", "Jackpot.Fonts.20", w / 2, h / 2, Color(255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
end

function CSGO_Jackpot.Notify(_Message)
	if CSGO_Jackpot.NotifyOnce then return end
	notification.AddLegacy(_Message or "", NOTIFY_GENERIC, 10)
	CSGO_Jackpot.NotifyOnce = true
end

function CSGO_Jackpot.ScrollToWinner(SteamID, AmountWon)
	if not CSGO_Jackpot["Main"] or not CSGO_Jackpot["Main"]:IsValid() or not CSGO_Jackpot["Main"].SCLR then return end
	
	if table.Count(Casino_Jackpot.BetTable) <= 1 then return end
	CSGO_Jackpot["Main"].SCLR.OffsetX = 0
	lerpo = 0

	CSGO_Jackpot["Main"].SCLR.Paint = function(self)
		local pnl = DImage[math.Round(30 / 2) .. "-" .. SteamID]
		if not pnl or not pnl:IsValid() then return end
		lerpo = Lerp(FrameTime(), lerpo, 1)
		self.OffsetX = math.Round((pnl:GetPos() - (CSGO_Jackpot["Main"].SCLR:GetWide() / 2) + self.Rand) * lerpo)
		self:InvalidateLayout(true)
		local chance = CSGO_Jackpot.GetPercentage(Casino_Jackpot.BetTable, SteamID)

		if lerpo > .9995 then
			if SteamID == LocalPlayer():SteamID() then
				CSGO_Jackpot.Notify(language.GetPhrase("vgui.casinojackpot.congrats") .. " " .. Casino_Jackpot.Config.CurrencySymbol .. string.Comma(AmountWon or 0))
			end

			CSGO_Jackpot["Main"].SCLR.DrawWinner = {SteamID, AmountWon, chance}
		end
	end
end

function CSGO_Jackpot.CreateMenu()
	if CSGO_Jackpot["Main"] then
		CSGO_Jackpot["Main"]:Remove()
	end

	if CSGO_Jackpot["Help"] and CSGO_Jackpot["Help"]["Main"] then
		CSGO_Jackpot["Help"]["Main"]:Remove()
	end

	CSGO_Jackpot.NotifyOnce = false
	CSGO_Jackpot["Main"] = vgui.Create("DFrame")
	CSGO_Jackpot["Main"]:SetSize(1000, 600)
	CSGO_Jackpot["Main"]:Center()
	CSGO_Jackpot["Main"]:MakePopup()
	CSGO_Jackpot["Main"]:SetTitle("")
	CSGO_Jackpot["Main"]:ShowCloseButton(false)
	CSGO_Jackpot["Main"].fade = 0
	CSGO_Jackpot["Main"].events = {}

	CSGO_Jackpot["Main"].Paint = function(self, w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(22, 30, 40))
		local winner = self.SCLR.DrawWinner or nil

		if winner then
			self.fade = Lerp(FrameTime() * 2, self.fade, 255)
			local to64 = util.SteamIDTo64(winner[1])
			steamworks.RequestPlayerInfo(to64)
			draw.SimpleText(steamworks.GetPlayerName(to64) .. " " .. language.GetPhrase("vgui.casinojackpot.wonof") .. " " .. Casino_Jackpot.Config.CurrencySymbol .. string.Comma(math.Round(winner[2])) .. " " .. language.GetPhrase("vgui.casinojackpot.witha") .. " " .. math.Round(winner[3] * 100, 2) .. language.GetPhrase("vgui.casinojackpot.percentchange"), "Jackpot.Fonts.20", w / 2, 500, Color(255, 172, 39, self.fade), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		else
			self.fade = 0
		end

		--requirements
		local __num_players = table.Count(Casino_Jackpot.BetTable)
		local __required = Casino_Jackpot.Config.RequiredPlayers
		local __pot_total = 0
		local __str = ""

		if CSGO_Jackpot["Main"].moneyBar then
			__pot_total = CSGO_Jackpot["Main"].moneyBar.CurVal
		end

		if __required > __num_players then
			__str = __str .. tostring(__required - __num_players) .. " player(s)"
		end

		if Casino_Jackpot.Config.MinBetCountdown > __pot_total then
			__str = __str .. " and " .. Casino_Jackpot.Config.CurrencySymbol .. string.Comma(Casino_Jackpot.Config.MinBetCountdown - __pot_total)
		end

		if __str ~= "" then
			draw.SimpleText(__str .. " more required to begin game!", "Jackpot.Fonts.20", w / 2, 500, Color(255, 172, 38), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		end
	end

	CSGO_Jackpot["Close"] = vgui.Create("DButton", CSGO_Jackpot["Main"])
	CSGO_Jackpot["Close"]:SetSize(32, 32)
	CSGO_Jackpot["Close"]:SetPos(CSGO_Jackpot["Main"]:GetWide() - 32, 0)
	CSGO_Jackpot["Close"]:SetText("")

	CSGO_Jackpot["Close"].Paint = function(self, w, h)
		draw.SimpleText("X", "Jackpot.Fonts.20", w / 2, h / 2, Color(255, 172, 39), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end

	CSGO_Jackpot["Close"].DoClick = function()
		if CSGO_Jackpot["Help"]["Main"] then
			CSGO_Jackpot["Help"]["Main"]:Remove()
		end

		if CSGO_Jackpot["Main"] then
			CSGO_Jackpot["Main"]:Remove()
		end
	end

	CSGO_Jackpot["Help"] = vgui.Create("DButton", CSGO_Jackpot["Main"])
	CSGO_Jackpot["Help"]:SetSize(32, 32)
	CSGO_Jackpot["Help"]:SetPos(CSGO_Jackpot["Main"]:GetWide() - 64, 0)
	CSGO_Jackpot["Help"]:SetText("")

	CSGO_Jackpot["Help"].Paint = function(self, w, h)
		draw.SimpleText("?", "Jackpot.Fonts.20", w / 2, h / 2, Color(255, 172, 39), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end

	CSGO_Jackpot["Help"].DoClick = function()
		if CSGO_Jackpot["Help"]["Main"] then
			CSGO_Jackpot["Help"]["Main"]:Remove()
		end

		CSGO_Jackpot["Help"]["Main"] = vgui.Create("DFrame")
		CSGO_Jackpot["Help"]["Main"]:SetSize(256, 100)
		CSGO_Jackpot["Help"]["Main"]:MakePopup()
		CSGO_Jackpot["Help"]["Main"]:Center()
		CSGO_Jackpot["Help"]["Main"]:SetTitle("")
		CSGO_Jackpot["Help"]["Main"]:ShowCloseButton(false)

		CSGO_Jackpot["Help"]["Main"].Paint = function(self, w, h)
			draw.RoundedBox(0, 0, 0, w, h, Color(255, 172, 38))
			draw.RoundedBox(0, 1, 1, w - 2, h - 2, Color(22, 30, 40))
			draw.SimpleText("Maximum bet: " .. Casino_Jackpot.Config.CurrencySymbol .. string.Comma(Casino_Jackpot.Config.MaxCashBet), "Jackpot.Fonts.20", w / 2, 50, Color(255, 172, 39), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			draw.SimpleText("Maximum jackpot: " .. Casino_Jackpot.Config.CurrencySymbol .. string.Comma(Casino_Jackpot.Config.MaxJackpot), "Jackpot.Fonts.20", w / 2, 70, Color(255, 172, 39), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		end

		CSGO_Jackpot["Help"]["CircleAvatars"] = vgui.Create("DCheckBoxLabel", CSGO_Jackpot["Help"]["Main"])
		CSGO_Jackpot["Help"]["CircleAvatars"]:SetValue(JACKPOT_CIRCLE)
		CSGO_Jackpot["Help"]["CircleAvatars"]:SetPos(128 - 60, 20)
		CSGO_Jackpot["Help"]["CircleAvatars"]:SetText("#vgui.casinojackpot.circleavatars")

		CSGO_Jackpot["Help"]["CircleAvatars"].OnChange = function(self, val)
			JACKPOT_CIRCLE = (val and 1) or 0
		end

		CSGO_Jackpot["Help"]["Close"] = vgui.Create("DButton", CSGO_Jackpot["Help"]["Main"])
		CSGO_Jackpot["Help"]["Close"]:SetSize(32, 32)
		CSGO_Jackpot["Help"]["Close"]:SetPos(256 - 32, 0)
		CSGO_Jackpot["Help"]["Close"]:SetText("")

		CSGO_Jackpot["Help"]["Close"].Paint = function(self, w, h)
			draw.SimpleText("X", "Jackpot.Fonts.20", w / 2, h / 2, Color(255, 172, 39), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		end

		CSGO_Jackpot["Help"]["Close"].DoClick = function()
			CSGO_Jackpot["Help"]["Main"]:Remove()
		end
	end

	CSGO_Jackpot["Main"].moneyBar = vgui.Create("DPanel", CSGO_Jackpot["Main"])
	CSGO_Jackpot["Main"].moneyBar:SetPos(50, 20)
	CSGO_Jackpot["Main"].moneyBar:SetSize(360, 360)
	CSGO_Jackpot["Main"].moneyBar.CurVal = 0
	CSGO_Jackpot["Main"].moneyBar.curval_lerp = 0

	CSGO_Jackpot["Main"].moneyBar.Paint = function(self, w, h)
		self.curval_lerp = Lerp(FrameTime() * 2, self.curval_lerp, self.CurVal)
		surface.SetFont("Jackpot.Fonts.45")
		local t = Casino_Jackpot.Config.CurrencySymbol .. string.Comma(math.Round(self.curval_lerp))
		local t_size = surface.GetTextSize(t)
		draw.NoTexture()
		draw.Arc(180, 180, 180, 50, 0, 360, 9, Color(255, 172, 38), true)
		draw.Arc(180, 180, 180, 50, 90, (math.Round(Casino_Jackpot.Config.MaxJackpot - self.curval_lerp) / Casino_Jackpot.Config.MaxJackpot) * 360 + 90, 9, Color(32, 40, 50), true)
		draw.SimpleText(t, "Jackpot.Fonts.45", w / 2, h / 2, Color(255, 172, 39), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		--timer
		local t = timer.TimeLeft("CSGO_Jackpot.WaitBuffer") or 0
		draw.Arc(180, 180, 128, 4, 0, 360, 9, Color(255, 172, 38), true)
		draw.Arc(180, 180, 128, 4, 90, (Casino_Jackpot.Config.CountdownTimer - t) / Casino_Jackpot.Config.CountdownTimer * 360 + 90, 9, Color(32, 40, 50), true)
		self:InvalidateLayout(true)
	end

	CSGO_Jackpot["NumWang"] = vgui.Create("DNumberWang", CSGO_Jackpot["Main"])
	CSGO_Jackpot["NumWang"]:SetSize(128, 32)
	CSGO_Jackpot["NumWang"]:SetPos(165, 410)
	CSGO_Jackpot["NumWang"]:SetText("")
	CSGO_Jackpot["NumWang"]:SetFont("Jackpot.Fonts.20")
	CSGO_Jackpot["NumWang"]:SetMinMax(Casino_Jackpot.Config.MinCashBet, Casino_Jackpot.Config.MaxCashBet)

	CSGO_Jackpot["NumWang"].Paint = function(self, w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(255, 172, 38))
		draw.RoundedBox(0, 1, 1, w - 2, h - 2, Color(32, 40, 50))
		self:DrawTextEntryText(Color(220, 220, 220), Color(30, 130, 255), Color(255, 255, 255))
	end

	local lbl = vgui.Create("DLabel", CSGO_Jackpot["Main"])
	lbl:SetPos(60, 415)
	lbl:SetFont("Jackpot.Fonts.20")
	lbl:SetColor(Color(255, 255, 255))
	lbl:SetText("#vgui.casinojackpot.betamount")
	lbl:SizeToContents()
	local btn = vgui.Create("DButton", CSGO_Jackpot["Main"])
	btn:SetSize(128, 32)
	btn:SetPos(292, 410)
	btn:SetText("")

	btn.Paint = function(self, w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(255, 172, 38))
		draw.SimpleText(Casino_Jackpot.Config.CurrencySymbol, "Jackpot.Fonts.20", w / 2, h / 2, Color(32, 40, 50), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end

	local btn2 = vgui.Create("DButton", CSGO_Jackpot["Main"])
	btn2:SetSize(360, 32)
	btn2:SetPos(60, 445)
	btn2:SetText("")

	btn2.Paint = function(self, w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(255, 172, 38))
		draw.SimpleText("#vgui.casinojackpot.placebet", "Jackpot.Fonts.20", w / 2, h / 2, Color(32, 40, 50), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end

	btn2.DoClick = function()
		local val = CSGO_Jackpot["NumWang"]:GetValue()

		if val > Casino_Jackpot.Config.MaxCashBet or val < Casino_Jackpot.Config.MinCashBet then
			notification.AddLegacy(language.GetPhrase("vgui.casinojackpot.larger") .. " " .. Casino_Jackpot.Config.MaxCashBet .. " " .. language.GetPhrase("vgui.casinojackpot.lower") .. " " .. Casino_Jackpot.Config.MinCashBet, NOTIFY_ERROR, 5)

			return
		end

		net.Start("CSGO.Jackpot.PlaceBet")
		net.WriteInt(val, 32)
		net.SendToServer()
	end

	local b = vgui.Create("DScrollPanel", CSGO_Jackpot["Main"])
	b:SetSize(500, 75)
	b:SetPos(475, 20)
	b.VBar:SetSize(4, 75)

	b.VBar.Paint = function(self, w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(22, 30, 40))
	end

	b.VBar.btnDown.Paint = function(self, w, h) end
	b.VBar.btnUp.Paint = function(self, w, h) end

	b.VBar.btnGrip.Paint = function(self, w, h)
		draw.RoundedBox(4, 0, 0, w, h, Color(255, 172, 38))
	end

	CSGO_Jackpot["Main"].playersPanel = vgui.Create("DIconLayout", b)
	CSGO_Jackpot["Main"].playersPanel:SetSize(500, 75)
	CSGO_Jackpot["Main"].playersPanel:SetPos(0, 0)
	CSGO_Jackpot["Main"].playersPanel:SetSpaceY(11)
	CSGO_Jackpot["Main"].playersPanel:SetSpaceX(7)
	CSGO_Jackpot["Main"].betterLog = vgui.Create("DScrollPanel", CSGO_Jackpot["Main"])
	CSGO_Jackpot["Main"].betterLog:SetSize(500, 380)
	CSGO_Jackpot["Main"].betterLog:SetPos(475, 100)
	local w, h = CSGO_Jackpot["Main"].betterLog:GetSize()
	CSGO_Jackpot["Main"].betterLog.VBar:SetSize(4, h)

	CSGO_Jackpot["Main"].betterLog.VBar.Paint = function(self, w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(22, 30, 40))
	end

	CSGO_Jackpot["Main"].betterLog.VBar.btnDown.Paint = function(self, w, h) end
	CSGO_Jackpot["Main"].betterLog.VBar.btnUp.Paint = function(self, w, h) end

	CSGO_Jackpot["Main"].betterLog.VBar.btnGrip.Paint = function(self, w, h)
		draw.RoundedBox(4, 0, 0, w, h, Color(255, 172, 38))
	end

	CSGO_Jackpot["Main"].betterLog.Paint = function(self, w, h)
		draw.RoundedBox(1, 1, 1, w, h, Color(22, 30, 40))
	end

	CSGO_Jackpot["Main"].betterLog.evnt = 0
	local lerpo = 0
	local dP = vgui.Create("DPanel", CSGO_Jackpot["Main"])
	dP:SetSize(1000, 64)
	dP:SetPos(CSGO_Jackpot["Main"]:GetWide() / 2 - (1000 / 2), 520)

	dP.Paint = function(self, w, h)
		draw.RoundedBox(0, 0, 0, 1000, h, Color(32, 40, 50))
	end

	CSGO_Jackpot.CreateScroller()
	net.Start("CSGO.Jackpot.RequestPlayers")
	net.SendToServer()
end

net.Receive("CSGO.Jackpot.OpenMenu", function()
	if #Casino_Jackpot.Config.UsergroupRestrictions > 0 then
		local __allow

		for k, v in pairs(Casino_Jackpot.Config.UsergroupRestrictions) do
			if LocalPlayer():GetUserGroup() == v then
				__allow = true
			end
		end

		if not __allow then
			notification.AddLegacy(language.GetPhrase("vgui.casinojackpot.notallowed"), NOTIFY_ERROR, 10)

			return
		end
	end

	CSGO_Jackpot.CreateMenu()
end)

net.Receive("CSGO.Jackpot.Notify", function()
	notification.AddLegacy(net.ReadString() or "", NOTIFY_GENERIC, 10)
end)