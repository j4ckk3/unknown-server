util.AddNetworkString( "CSGO.Jackpot.WinnerWinnerChickenDinner" )
util.AddNetworkString( "CSGO.Jackpot.ReceivePlayers" )
util.AddNetworkString( "CSGO.Jackpot.RequestPlayers" )
util.AddNetworkString( "CSGO.Jackpot.PlaceBet" )
util.AddNetworkString( "CSGO.Jackpot.OpenMenu" )
util.AddNetworkString( "CSGO.Jackpot.Notify" )

Casino_Jackpot.players = {}

if timer.Exists( "CSGO.Jackpot.Draw" ) then
	timer.Remove( "CSGO.Jackpot.Draw" )
end

function Casino_Jackpot.GetTotal()
	local total = 0

	for k, v in pairs( Casino_Jackpot.players ) do
		total = total + v
	end

	return total
end

function Casino_Jackpot.UsergroupCheck( _Player )

	local __allow = true

	if #Casino_Jackpot.Config.UsergroupRestrictions > 0 then

		__allow = false

		for k, v in pairs( Casino_Jackpot.Config.UsergroupRestrictions ) do
			if _Player:GetUserGroup() == v then
				__allow = true
			end
		end

	end

	return __allow
end

net.Receive( "CSGO.Jackpot.PlaceBet", function( _Length, _Player )

	if !Casino_Jackpot.UsergroupCheck( _Player ) then
		return
	end

	if timer.Exists( "ClearAll" ) then
		return
	end

	local amnt = net.ReadInt( 32 )
	local newAmnt

	local total = Casino_Jackpot.GetTotal()

	if total + amnt > Casino_Jackpot.Config.MaxJackpot then
		net.Start( "CSGO.Jackpot.Notify" )
		net.WriteString( string.format( Casino_Jackpot.Config.Translations[Casino_Jackpot.Config.Language]["vgui.casinojackpot.potoverflow"], Casino_Jackpot.Config.CurrencySymbol .. string.Comma( Casino_Jackpot.Config.MaxJackpot ) ) )
		net.Send( _Player )

		return
	end

	if Casino_Jackpot.players[ _Player:SteamID() ] then
		newAmnt = Casino_Jackpot.players[ _Player:SteamID() ] + amnt
	else
		newAmnt = amnt
	end

	if ( newAmnt < Casino_Jackpot.Config.MinCashBet ) || ( newAmnt > Casino_Jackpot.Config.MaxCashBet ) then
		
		net.Start( "CSGO.Jackpot.Notify" )
		net.WriteString( string.format( Casino_Jackpot.Config.Translations[Casino_Jackpot.Config.Language]["vgui.casinojackpot.maximumbet"], Casino_Jackpot.Config.CurrencySymbol .. string.Comma( Casino_Jackpot.Config.MaxCashBet ) ) )
		net.Send( _Player )

		return
	end

	if not Casino_Jackpot.CanAfford( _Player, amnt ) then
		net.Start( "CSGO.Jackpot.Notify" )
		net.WriteString( Casino_Jackpot.Config.Translations[Casino_Jackpot.Config.Language]["vgui.casinojackpot.notenough"] )
		net.Send( _Player )

		return
	end

	Casino_Jackpot.AddCash( _Player, -amnt )

	Casino_Jackpot.players[ _Player:SteamID() ] = newAmnt

	total = Casino_Jackpot.GetTotal()

	if Casino_Jackpot.Config.MinPlayerCountdown != 0 && table.Count( Casino_Jackpot.players ) >= Casino_Jackpot.Config.MinPlayerCountdown then
		Casino_Jackpot.Draw()
		Casino_Jackpot.SendDataToAll( )

		return
	elseif Casino_Jackpot.Config.MinBetCountdown != 0 && total >= Casino_Jackpot.Config.MinBetCountdown then
		Casino_Jackpot.Draw()
		Casino_Jackpot.SendDataToAll( )

		return
	end

	Casino_Jackpot.SendDataToAll( )
end)


function Casino_Jackpot.SendDataToAll( )
	net.Start( "CSGO.Jackpot.ReceivePlayers" )
	net.WriteTable( Casino_Jackpot.players )
	net.WriteDouble( timer.TimeLeft( "CSGO.Jackpot.Draw" ) or 0 )
	net.Broadcast( )
end

function Casino_Jackpot.SendDataPlayer( _Player )
	net.Start( "CSGO.Jackpot.ReceivePlayers" )
	net.WriteTable( Casino_Jackpot.players )
	net.WriteDouble( timer.TimeLeft( "CSGO.Jackpot.Draw" ) or 0 )
	net.Send( _Player )
end

net.Receive( "CSGO.Jackpot.RequestPlayers", function( _Length, _Player )
	Casino_Jackpot.SendDataPlayer( _Player )
end)

function Casino_Jackpot.IsFinite(num)
    return not (num ~= num or num == math.huge or num == -math.huge)
end

function Casino_Jackpot.AddCash(_Player, _Amount)
	local __type = string.lower(Casino_Jackpot.Config.CashType)
	local __amount = math.Round(tonumber(_Amount))
	if not Casino_Jackpot.IsFinite(__amount) then return end

	if __type == "pointshop" then
		_Player:PS_GivePoints(__amount)

		return
	elseif __type == "santosrp" then
		_Player:AddMoney(__amount)

		return
	elseif __type == "pointshop2_standard" then
		_Player:PS2_AddStandardPoints(__amount)

		return
	elseif __type == "pointshop2_premium" then
		_Player:PS2_AddPremiumPoints(__amount)

		return
	else
		_Player:addMoney(__amount) --DarkRP default

		return
	end
end

function Casino_Jackpot.CanAfford(_Player, _Amount)
	if not _Player or not _Amount then return false end

    local __type = string.lower(Casino_Jackpot.Config.CashType)
        
    if __type == "pointshop" then
        return ( _Player:PS_GetPoints() >= _Amount )
    elseif __type == "santosrp" then
    	return _Player:CanAfford( _Amount )
    elseif __type == "pointshop2_standard" then
    	return ( _Player.PS2_Wallet.points >= _Amount )
    elseif __type == "pointshop2_premium" then
    	return ( _Player.PS2_Wallet.premiumPoints >= _Amount )
    else
    	return _Player:canAfford( _Amount )
    end
end

function Casino_Jackpot.GetPercentage( _Table, _Key )

	local int = 0

	for k, v in pairs( _Table ) do
		int = int + v
	end

	return _Table[_Key] / int
end

function Casino_Jackpot.GetWinner( _Table )
   local sum = 0
   for _, chance in pairs( _Table ) do
      sum = sum + chance
   end

   local rand = math.random(sum)
   local winningKey
   for key, chance in pairs( _Table ) do
      winningKey = key
      rand = rand - chance
      if rand <= 0 then break end
   end

   return winningKey
end

function Casino_Jackpot.Draw()

	if table.Count( Casino_Jackpot.players ) <= 1 then
		return
	end

	local __winner_steamID = Casino_Jackpot.GetWinner( Casino_Jackpot.players )
	
	if timer.Exists( "CSGO.Jackpot.Draw" ) then
		return
	end

	timer.Create( "CSGO.Jackpot.Draw", Casino_Jackpot.Config.CountdownTimer, 1, function( )
		Casino_Jackpot.SendDataToAll()

		net.Start( "CSGO.Jackpot.WinnerWinnerChickenDinner" )
		net.WriteTable( Casino_Jackpot.players )
		net.WriteString( __winner_steamID )

		local __total_win = 0

		for k, v in pairs( Casino_Jackpot.players ) do
			__total_win = __total_win + v
		end

		net.WriteInt( __total_win, 32 )
		net.Broadcast( )

		timer.Create( "ClearAll", Casino_Jackpot.Config.TimeBeforeClear, 1, function()
			
			local __winner = player.GetBySteamID( __winner_steamID )
			local __percent_win = Casino_Jackpot.GetPercentage( Casino_Jackpot.players, __winner_steamID )
			local __players_temp = Casino_Jackpot.players

			Casino_Jackpot.players = {}
			Casino_Jackpot.SendDataToAll( )

			if __winner && __winner:IsValid() then
				Casino_Jackpot.AddCash( __winner, __total_win )

				local __find_lang = Casino_Jackpot.Config.Translations[ Casino_Jackpot.Config.Language ]
				local __str = "[JACKPOT] " .. __winner:Nick() .. " " .. __find_lang[ "vgui.casinojackpot.wonof" ] .. " " .. Casino_Jackpot.Config.CurrencySymbol .. string.Comma( __total_win or 0 ) .. " " .. __find_lang[ "vgui.casinojackpot.witha" ] .. " " .. math.Round( __percent_win or 0, 2 ) * 100 .. __find_lang[ "vgui.casinojackpot.percentchange" ]
				
				if Casino_Jackpot.Config.AnnouceWins then
					if Casino_Jackpot.Config.AnnounceToPlayersOnly then
						for k, v in pairs( __players_temp ) do
							local __player = player.GetBySteamID( k )

							if __player && __player:IsValid() then
								__player:PrintMessage( HUD_PRINTTALK, __str )
							end
						end
					else
						for k, v in pairs( player.GetAll() ) do
							v:PrintMessage( HUD_PRINTTALK, __str )
						end
					end
				end
			end
		end)
	end)

end

hook.Add( "PlayerSay", "Casino_Jackpot.PlayerSay.OpenMenu", function( _Player, _Text )
	local __text = string.lower( _Text )

	if Casino_Jackpot.Config.CommandsEnabled && __text == "/jackpot" || __text == "!jackpot" then
		net.Start( "CSGO.Jackpot.OpenMenu" )
		net.Send( _Player )

		return ""
	end
end )

hook.Add( "OnPhysgunFreeze", "Casino_Jackpot.OnPhysgunFreeze.SetLocation", function( _Weapon, _Physics, _Entity, _Player )

	if _Player:IsSuperAdmin() and _Entity.Key and _Entity:IsValid() and _Entity:GetClass() == "sent_jackpotnpc" then

		local __table = util.JSONToTable(file.Read("csgo_jackpot/" .. string.lower(game.GetMap()) .. ".txt") or "[]")

		if __table[_Entity.Key] then
			__table[_Entity.Key] = {
				["vector"] = _Entity:GetPos(),
				["angle"] = _Entity:GetAngles() - Angle(90, 0, 0)
			}
		end

		file.Write("csgo_jackpot/" .. string.lower(game.GetMap()) .. ".txt", util.TableToJSON(__table or "[]"))

		return
	end
end)

hook.Add( "EntityRemoved", "Casino_Jackpot.EntityRemoved.RemoveNPC", function( _Entity )
	if _Entity.Key and _Entity:IsValid() and _Entity:GetClass() == "sent_jackpotnpc" then
		local __table = util.JSONToTable(file.Read("csgo_jackpot/" .. string.lower(game.GetMap()) .. ".txt") or "[]")

		if __table[_Entity.Key] then
			__table[_Entity.Key] = {}
		end

		file.Write("csgo_jackpot/" .. string.lower(game.GetMap()) .. ".txt", util.TableToJSON(__table or "[]"))

		return
	end
end)

--[[hook.Add("InitPostEntity", "Casino_Jackpot.InitPostEntity.SpawnNPC", function()
	local __table = util.JSONToTable(file.Read("csgo_jackpot/" .. string.lower(game.GetMap()) .. ".txt") or "[]")

	for k, v in pairs(__table) do
		if not v.vector or not v.angle then continue end

		local npc = ents.Create("sent_jackpotnpc")
		npc:SetPos(v.vector)
		npc:SetAngles(v.angle)
		npc:Spawn()
		npc:Activate()
		npc.Key = k
	end

end)]]--
