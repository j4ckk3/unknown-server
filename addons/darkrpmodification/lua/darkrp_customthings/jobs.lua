--[[---------------------------------------------------------------------------
DarkRP custom jobs
---------------------------------------------------------------------------
This file contains your custom jobs.
This file should also contain jobs from DarkRP that you edited.

Note: If you want to edit a default DarkRP job, first disable it in darkrp_config/disabled_defaults.lua
      Once you've done that, copy and paste the job to this file and edit it.

The default jobs can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/jobrelated.lua

For examples and explanation please visit this wiki page:
http://wiki.darkrp.com/index.php/DarkRP:CustomJobFields

Add your custom jobs under the following line:
---------------------------------------------------------------------------]]


-----------------------------------CITIZENS----------------------------------------
TEAM_CITIZEN = DarkRP.createJob("Citizen", {
    color = Color(20, 150, 20, 255),
    model = {
        "models/player/Group01/Female_01.mdl",
        "models/player/Group01/Female_02.mdl",
        "models/player/Group01/Female_03.mdl",
        "models/player/Group01/Female_04.mdl",
        "models/player/Group01/Female_06.mdl",
        "models/player/group01/male_01.mdl",
        "models/player/Group01/Male_02.mdl",
        "models/player/Group01/male_03.mdl",
        "models/player/Group01/Male_04.mdl",
        "models/player/Group01/Male_05.mdl",
        "models/player/Group01/Male_06.mdl",
        "models/player/Group01/Male_07.mdl",
        "models/player/Group01/Male_08.mdl",
        "models/player/Group01/Male_09.mdl"
    },
    description = [[The Citizen is the most basic level of society you can hold besides being a hobo. You have no specific role in city life.]],
    weapons = {},
    command = "citizen",
    max = 0,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Citizens",
})

TEAM_ARMS = DarkRP.createJob("Arms Dealer", {
    color = Color(204, 132, 0, 255),
    model = {"models/player/monk.mdl"},
    description = [[As a Arms Dealer you sell weaponry to the city's people for relative prices. You decide!]],
    weapons = {},
    command = "armsdealer",
    max = 2,
    salary = 500,
    admin = 0,
    vote = false,
    hasLicense = true,
    candemote = true,
    category = "Citizens"
})
------------------------------------GOVERNMENT------------------------------------------


------------------------------------CRIMINALS-------------------------------------------
TEAM_GANGSTER = DarkRP.createJob("Gangster", {
    color = Color(110, 110, 110, 255),
    model = {"models/player/Suits/male_01_open_tie.mdl",
     "models/player/Suits/male_02_open_tie.mdl",
      "models/player/Suits/male_03_open_tie.mdl",
       "models/player/Suits/male_04_open_tie.mdl",
        "models/player/Suits/male_05_open_tie.mdl",
         "models/player/Suits/male_06_open_tie.mdl",
          "models/player/Suits/male_07_open_tie.mdl",
           "models/player/Suits/male_08_open_tie.mdl",
            "models/player/Suits/male_09_open_tie.mdl"},
    description = [[You're a Gangster, You do whatever you want to!]],
    weapons = {""},
    command = "gangster",
    max = 10,
    salary = 160,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Criminals"
})

TEAM_THIEF = DarkRP.createJob("Thief", {
    color = Color(156, 0, 0, 255),
    model = {
        "models/player/Suits/robber_open.mdl",
        "models/player/Suits/robber_shirt.mdl",
        "models/player/Suits/robber_shirt_2.mdl",
        "models/player/Suits/robber_tie.mdl",
        "models/player/Suits/robber_tuckedtie.mdl"
    },
    description = [[You're a Thief, You break into people's homes for fun.]],
    weapons = {"lockpick"},
    command = "thief",
    max = 6,
    salary = 100,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Criminals"
})

--[[---------------------------------------------------------------------------
Define which team joining players spawn into and what team you change to if demoted
---------------------------------------------------------------------------]]
GAMEMODE.DefaultTeam = TEAM_CITIZEN
--[[---------------------------------------------------------------------------
Define which teams belong to civil protection
Civil protection can set warrants, make people wanted and do some other police related things
---------------------------------------------------------------------------]]
GAMEMODE.CivilProtection = {
    [TEAM_POLICE] = true,
    [TEAM_CHIEF] = true,
    [TEAM_MAYOR] = true,
}
--[[---------------------------------------------------------------------------
Jobs that are hitmen (enables the hitman menu)
---------------------------------------------------------------------------]]
DarkRP.addHitmanTeam(TEAM_MOB)
