local discordspawns = {}
local teamspeakspawns = {}
local discordpos
local teamspeakpos
resource.AddWorkshop("812594909")

local LoadSpawns = function()
    --------------------------------------------------------
    --------------------------Discord-----------------------
    --------------------------------------------------------
    if not file.IsDir("wall_widgets/discord", "DATA") then
        file.CreateDir("wall_widgets/discord")
    end

    if file.Exists("wall_widgets/discord/" .. string.lower(game.GetMap()) .. ".txt", "DATA") then
		discordpos = ""
		discordpos = file.Read("wall_widgets/discord/" .. string.lower(game.GetMap()) .. ".txt", "DATA")
    else
        discordpos = ""
    end

    discordspawns = {}

    if string.len(discordpos) >= 5 then
        local DiscordPostab = string.Explode("\n", discordpos)

        for k, v in pairs(DiscordPostab) do
            local spaw = string.Explode(";", v)
            local pos = util.StringToType(spaw[1], "Vector")
            local angle = util.StringToType(spaw[2], "Angle")

            if discordpos ~= nil then
                discordspawns[pos] = angle
			end
		end
	end
end

local Widget_Spawn = function()
	LoadSpawns()
    --------------------------------------------------------
    --------------------------Discord-----------------------
    --------------------------------------------------------
    for v, k in pairs(ents.FindByClass("widget_discord")) do
        k:Remove()
    end

    for v, k in pairs(discordspawns) do
        if util.StringToType(tostring(v), "Vector").x ~= 0 and util.StringToType(tostring(v), "Vector").y ~= 0 and util.StringToType(tostring(v), "Vector").z ~= 0 then
            local ent = ents.Create("widget_discord")
            ent:SetPos(util.StringToType(tostring(v), "Vector"))
            ent:SetAngles(util.StringToType(tostring(k), "Angle"))
            ent:Spawn()
        end
    end
end

hook.Add("PostCleanupMap", "PostcleanupDiscordSpawn", Widget_Spawn)
hook.Add("InitPostEntity", "initialspawndiscord", Widget_Spawn)

timer.Simple(5, function()
    Widget_Spawn()
end)

function SaveWidgets(ply)
    --------------------------------------------------------
    --------------------------Discord-----------------------
    --------------------------------------------------------
    if IsValid(ply) and ply:IsSuperAdmin() then
        local discordcurrentspawns = {}
        local NewSpawns2

        for v, k in pairs(ents.FindByClass("widget_discord")) do
            local stringtoadd = (tostring(math.Round(k:GetPos().x)) .. " " .. tostring(math.Round(k:GetPos().y)) .. " " .. tostring(math.Round(k:GetPos().z)) .. ";" .. tostring(math.Round(k:GetAngles().p)) .. " " .. tostring(math.Round(k:GetAngles().y)) .. " " .. tostring(math.Round(k:GetAngles().r)))
            table.insert(discordcurrentspawns, stringtoadd)
            local discordsavepos = table.concat(discordcurrentspawns, "\n")

            if (discordsavepos == "") then
                NewSpawns2 = tostring(math.Round(k:GetPos().x)) .. " " .. tostring(math.Round(k:GetPos().y)) .. " " .. tostring(math.Round(k:GetPos().z)) .. ";" .. tostring(math.Round(k:GetAngles().p)) .. " " .. tostring(math.Round(k:GetAngles().y)) .. " " .. tostring(math.Round(k:GetAngles().r))
            else
                NewSpawns2 = discordsavepos .. "\n"
            end

            file.Write("wall_widgets/discord/" .. string.lower(game.GetMap()) .. ".txt", NewSpawns2)
        end

        LoadSpawns()
    else
        if IsValid(ply) then
            ply:ChatPrint("You are not a superadmin")
        end
    end
end

concommand.Add("widget_save", function(ply)
    SaveWidgets(ply)
end)