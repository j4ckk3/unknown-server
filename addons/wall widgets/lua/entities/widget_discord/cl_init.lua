local discordid = "311937293227130891"

--Translate here
local voice = "Voice"
local chat = "Chat"
local usetocopy = "Use to Copy Link"
local members = "Members"
local online = "Online"



-----------------VV DONT EDIT BELOW THIS LINE VV-----------------------
include("shared.lua")
AddCSLuaFile()

local discordapp = {}
local GetDiscordClients = function()
	local url = "https://discordapp.com/api/guilds/"..discordid.."/widget.json"
	http.Fetch(url, function(body, length, headers, code )
	discordapp.discordtable = util.JSONToTable( body )
	end)
end
GetDiscordClients()
function ENT:Initialize()
	GetDiscordClients()
	timer.Create( "Discordinfo", 15, 0, function () GetDiscordClients() end )
	
end

local rot = Vector(-90, 90, 0)
local font = "DebugFixedSmall"
local discordmat = Material("discordlogo.png")

net.Receive( "UsedDiscord", function( len, ply )
	SetClipboardText(discordapp.discordtable.instant_invite)
	LocalPlayer():ChatPrint("Address Copied")
end )



function ENT:Draw()
	local pos = self.Entity:GetPos() + (self.Entity:GetForward() )
	local ang = self.Entity:GetAngles()
		ang:RotateAroundAxis(ang:Right(), 	rot.x)
	ang:RotateAroundAxis(ang:Up(), 		rot.y)
	ang:RotateAroundAxis(ang:Forward(), rot.z)
	if discordapp.discordtable then		
		cam.Start3D2D( pos, ang, .25 )
			surface.SetDrawColor( 50, 50, 50, 255 ) 
			surface.DrawRect( -120, -200, 240, 400 ) 
			surface.SetDrawColor( 255, 255, 255, 255 ) 
			surface.DrawRect( 0, -200, 1, 400 ) 
			surface.SetDrawColor( 114, 137, 218 ) 
			surface.DrawRect( -120, -240, 240, 60  ) 
			surface.SetDrawColor(  255, 255, 255, 255 ) 
			surface.SetMaterial( discordmat	) 
			surface.DrawTexturedRect( -120, -240, 150, 60 ) 
			local totalonline = 0
			if discordapp.discordtable.members != nil then
				for v,k in pairs (discordapp.discordtable.members) do
					if !k.bot then
					totalonline = totalonline + 1
					end
				end
			end
			draw.SimpleText( totalonline, font, 80, -225, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
			draw.SimpleText( members, font, 80, -215, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
			draw.SimpleText( online , font, 80, -205, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
			surface.SetDrawColor( 114, 137, 218 ) 
			surface.DrawRect( -120, 180, 240, 30  ) 
			draw.SimpleText( (discordapp.discordtable.name), "DebugFixed", 0, 185, Color( 255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
			draw.SimpleText( (discordapp.discordtable.instant_invite), "HudHintTextSmall", 0, 195, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
			draw.SimpleText( usetocopy, "HudHintTextSmall", 0, 205, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
			surface.SetDrawColor( 114, 137, 218 ) 
			surface.DrawRect( 1, -180, 119, 15  ) 
			draw.SimpleText( voice, font, 60, -172.5, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
		
			surface.SetDrawColor( 114, 137, 218 ) 
			surface.DrawRect( -120, -180, 120, 15  ) 
			surface.SetDrawColor( 255, 255, 255 ) 
			surface.DrawRect( -120, -180, 240, 1  ) 
			draw.SimpleText( chat , font, -60, -172.5, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
			local playernameposchat = -160
			if discordapp.discordtable.members != nil then
				for v,k in pairs(discordapp.discordtable.members) do
					if !k.bot then
						if playernameposchat <= 175 then
							if k.nick then
								draw.SimpleText( string.Left(k.nick,16), font, -118, playernameposchat, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
							else
								draw.SimpleText( string.Left(k.username,16), font, -118, playernameposchat, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
							end
							
						end
					playernameposchat = playernameposchat + 10
					
					end
				end
				local playernameposvoice = -160
				for v,k in pairs(discordapp.discordtable.members) do
					if !k.bot and k.channel_id then
						if playernameposvoice <= 175 then
							if k.nick then
								draw.SimpleText( string.Left(k.nick,16), font, 3, playernameposvoice, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
							else
								draw.SimpleText( string.Left(k.username,16), font, 3, playernameposvoice, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, Color( 0, 0, 0 ) )
							end
							
						end
					playernameposvoice = playernameposvoice + 10
					
					end
				end
			end
		cam.End3D2D()
	end
end