AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

util.AddNetworkString( "UsedDiscord" )

function ENT:Use( activator, caller )
	net.Start( "UsedDiscord" )
	net.Send( activator )
end

function ENT:Initialize()
	self:SetUseType(SIMPLE_USE)
	self:SetModel("models/props_building_details/Storefront_Template001a_Bars.mdl")
	self:SetMoveType( MOVETYPE_NOCLIP )
	self:DrawShadow(false)
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetCollisionGroup(COLLISION_GROUP_WEAPON)
	self:GetPhysicsObject():EnableMotion( false )
end
