_PArmory.Weapons = {} 

_PArmory.AddWeapon(
	"Pistols", 				--- CATEGORY NAME.
	"fas2_ots33", 			--- CLASS NAME.
	Vector(-2, 0, 5), 		--- POSITION OFFSET.
	Angle(0, 15, 20) 			--- ANGLE OFFSET.
)
_PArmory.AddWeapon(
	"Pistols",
	"fas2_glock20",
	Vector(-5, 0, 0),
	Angle(0, 20, 15)
)
_PArmory.AddWeapon(
	"Rifles",
	"fas2_mp5a5",
	Vector(-8, 0, -3),
	Angle(5, 5, 0)
)
_PArmory.AddWeapon(
	"Rifles",
	"fas2_g36c",
	Vector(0, 0, -2),
	Angle(5, 0, 10)
)