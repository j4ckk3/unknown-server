_PArmory.Weapons = {}
_PArmory.Categories = {}

resource.AddWorkshop("1165526642")

function _PArmory.CreateCategory(category)
	if table.HasValue(_PArmory.Categories, category) then
		Error(
			"[ERROR] Category '", category, "' already exists!\n",
				"Make sure the category is not added twice!\n"
		)
		
		return
	end
	
	table.insert(_PArmory.Categories, category)
end

function _PArmory.AddWeapon(category, class, offset, angoffset)
	if !table.HasValue(_PArmory.Categories, category) then
		Error(
			"[ERROR] Category '", category, "' doesn't exist!\n",
				"Make sure the category is added in armory_categories.lua!\n"
		)
		
		return
	end
	
	table.insert(_PArmory.Weapons, {
		["category"] = category,
		["class"] = class,
		["offset"] = offset,
		["angoffset"] = angoffset
	})
end