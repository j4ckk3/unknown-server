local IsValid = IsValid
local table = table
local util = util
local file = file
local ents = ents
local game = game

local function spawnThem()
	local content = file.Read("_PArmory/spawns.txt", "DATA")
	
	if !content then return end
	
	local tbl = util.JSONToTable(content)
	local co = table.Count(tbl)
	local count = 0
	
	if co > 0 then
		for k, v in pairs(tbl) do
			if v["map"] == game.GetMap() then
				local entity = ents.Create("armory")
				entity:SetPos(v["position"])
				entity:SetAngles(v["angles"])
				entity:Spawn()
				entity:Activate()
				
				count = count + 1
			end
		end
		
		print("Created "..count.." Armories!")
	end
end
timer.Remove("_PArmoryCreateArmories")
timer.Create("_PArmoryCreateArmories", 5, 1, spawnThem)

local function CreateArmory(ply)
	if IsValid(ply) then
		if !ply:IsSuperAdmin() then 
			ply:ChatPrint("You must be a superadmin to use this!")
			return 
		end
		
		local tr = ply:GetEyeTrace()
		
		if !tr then 
			ply:ChatPrint("Invalid trace?? (Shouldn't happen)")
			return 
		end
		
		local pos = tr.HitPos
		local ang = ply:EyeAngles()
		ang.x = 0
		ang.z = 0
		
		if ply:GetPos():Distance(pos) > 200 then
			ply:ChatPrint("You're trying to spawn an Armory too far from you!")
			return
		end
		
		local entity = ents.Create("Armory")
		entity:SetPos(pos + Vector(0, 0, 36))
		entity:SetAngles(ang + Angle(0, 180, 0))
		entity:Spawn()
		entity:Activate()
		
		if IsValid(entity) then
			ply:ChatPrint("Armory created!")
			ply:ChatPrint([[Don't forget to run the "_PArmorySave" console command to save all armory positions!]])
		end
	end
end
concommand.Add("_PArmoryCreate", CreateArmory)

local function RemoveArmory(ply)
	if IsValid(ply) then
		if !ply:IsSuperAdmin() then 
			ply:ChatPrint("You must be a superadmin to use this!")
			return 
		end
		
		local tr = ply:GetEyeTrace()
		
		if !tr then 
			ply:ChatPrint("Invalid trace?? (Shouldn't happen)")
			return 
		end
		
		if !tr.Entity then 
			ply:ChatPrint("Invalid entity?? (Shouldn't happen)")
			return 
		end
		
		local entity = tr.Entity
		
		if entity:GetClass() != "armory" then 
			ply:ChatPrint("You're not looking at an Armory!")
			return 
		end
		
		local pos = tr.HitPos
		local ang = ply:EyeAngles()
		
		if ply:GetPos():Distance(pos) > 200 then
			ply:ChatPrint("You're trying to remove an Armory too far from you!")
			return
		end
		
		if IsValid(entity) then
			entity:Remove()
			
			ply:ChatPrint("Armory removed!")
			ply:ChatPrint([[Don't forget to run the "_PArmorySave" console command to save all armory positions!]])
		end
	end
end
concommand.Add("_PArmoryRemove", RemoveArmory)

local function SaveArmories(ply)
	if IsValid(ply) then
		if !ply:IsSuperAdmin() then 
			ply:ChatPrint("You must be a superadmin to use this!")
			return 
		end
		
		local arm = _PArmory
		
		local armoryTable = arm.Armories
		local count = table.Count(armoryTable)
		
		local tbl = {}
		
		if !file.IsDir("_PArmory", "DATA") then
			file.CreateDir("_PArmory")
		end
		
		if !file.Exists("_PArmory/spawns.txt", "DATA") then
			file.Write("_PArmory/spawns.txt")
		end
		
		local co = 0
		
		if count > 0 then
			for k, v in pairs(armoryTable) do
				if IsValid(v) then
					if v:GetClass() == "armory" then
						tbl[k] = {position = v:GetPos(), angles = v:GetAngles(), map = game.GetMap()}
						co = co + 1
					end
				end
			end
			
			if co > 0 then
				file.Write("_PArmory/spawns.txt", util.TableToJSON(tbl))
				
				ply:ChatPrint("Saved "..co.." Armories!")
			end
		else
			file.Write("_PArmory/spawns.txt", util.TableToJSON({}))
			ply:ChatPrint("All Armories removed from the config!")
		end
	end
end
concommand.Add("_PArmorySave", SaveArmories)