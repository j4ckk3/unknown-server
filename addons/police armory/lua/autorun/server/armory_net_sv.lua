if CLIENT then return end

util.AddNetworkString("_PArmoryCop")
util.AddNetworkString("_PArmoryWeapon")

local net = net
local table = table
local IsValid = IsValid

net.Receive("_PArmoryWeapon", function(len, ply)
	local armory = net.ReadEntity()
	local id = net.ReadString()
	
	local arm = _PArmory
	
	local copTeams = arm.Cops
	local armoryWeapons = arm.Weapons
	local weaponTimer = arm.WeaponTimer
	
	if !IsValid(ply) then 
		return 
	end
	
	if !IsValid(armory) then
		return
	end
	
	if !ply:Alive() then 
		return 
	end
	
	if armory:GetClass() != "armory" then
		return
	end
	
	if armory:GetPos():Distance(ply:GetPos()) > 200 then /* See if the player is not taking the weapon out of any armory miles away */
		return
	end
	
	if !table.HasValue(copTeams, ply:Team()) then
		return
	end
	
	if armory:GetWeps() <= 0 then
		DarkRP.notify(ply, 1, 5, "There are no weapons in the Armory!")
		return
	end
	
	for k,v in pairs(armoryWeapons) do
		if v["class"] == id then
			if ply.CanRetreiveWeapon == true then
				ply.CanRetreiveWeapon = false
				
				armory:SetWeps( armory:GetWeps() - 1 )
				
				local wep = ply:Give(v["class"])
				
				if arm.AllowDropping == false then
					wep.nodrop = true
				end
				
				DarkRP.notify(ply, 1, 5, "Retreived Weapon!")
				
				timer.Simple(weaponTimer, function()
					if IsValid(ply) then
						ply.CanRetreiveWeapon = true
					end
				end)
			else
				DarkRP.notify(ply, 1, 5, "You can't take another Weapon yet!")
			end
			
			return
		end
	end
end)