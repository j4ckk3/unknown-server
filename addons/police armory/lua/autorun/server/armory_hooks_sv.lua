hook.Add("OnArmoryStartRobbery", "external", function(self, robber)
	/* Add your own stuff here */
end)

hook.Add("OnArmoryEndRobbery", "external", function(self, robber, reason)
	/* Add your own stuff here */
end)

local pairs = pairs
local IsValid = IsValid
local math = math

hook.Add("PlayerSpawn", "armoryPlayer", function(ply)
	ply.CanRetreiveWeapon = true
end)

hook.Add("OnArmoryEndRobbery", "main", function(self, robber, reason)
	self:SetIsBeingRobbed(false)
	self:SetProgress(0)
	
	if reason == 5 then
		if IsValid(self:GetRobber()) then
			self:GetRobber():addMoney(self:GetMoney())
		end
		
		DarkRP.notifyAll(1, 5, "The Police Armory was robbed!")
		self:GetRobber():wanted(nil, "Police Armory Robbery!", 360)
		
		local recoveryTime = _PArmory.RecoveryTimer
		
		self:SetIsBroken(true)
		
		timer.Simple(recoveryTime, function()
			if IsValid(self) then
				self:SetIsBroken(false)
			end
		end)
		
		local darkrpShipments = CustomShipments
		
		for i = 1, self:GetShipments() do
			timer.Simple(i - 1, function()
				for k,v in pairs(darkrpShipments) do
					shipmentNumber = math.random( table.Count(darkrpShipments) )
				end
				
				if shipmentNumber > 0 then
					local shipment = ents.Create("spawned_shipment")
					shipment:SetContents(shipmentNumber, math.random(3, 8))
					shipment:Spawn()
					shipment:SetModel("models/Items/item_item_crate.mdl")
					shipment:PhysicsInit(SOLID_VPHYSICS)
					shipment:SetMoveType(MOVETYPE_VPHYSICS)
					shipment:SetSolid(SOLID_VPHYSICS)
					local pos = self:GetPos()
					local ang = self:GetAngles()
					
					shipment:SetPos( pos + (ang:Forward() * 50) + (ang:Up() * math.random(5, 30)) )
					
					local phys = shipment:GetPhysicsObject()
					
					if IsValid(phys) then
						phys:Wake()
					end
				end
			end)
		end
		
		self:SetWeps(0)
		self:SetShipments(0)
		self:SetMoney(0)
		self:SetIsBroken(true)
		
	elseif reason == 3 then
		DarkRP.notifyAll(1, 5, "Armory robber disconnected!")
		DarkRP.notifyAll(1, 5, "Armory robbery failed!")
	elseif reason == 2 then
		DarkRP.notifyAll(1, 5, "Armory robber died!")
		DarkRP.notifyAll(1, 5, "Armory robbery failed!")
	elseif reason == 4 then
		DarkRP.notifyAll(1, 5, "Armory robber ran away!")
		DarkRP.notifyAll(1, 5, "Armory robbery failed!")
	elseif reason == 1 then
		DarkRP.notifyAll(1, 5, "Armory robber got arrested!")
		DarkRP.notifyAll(1, 5, "Armory robbery failed!")
	end
	
	self:SetRobber(nil)
end)

hook.Add("PlayerDeath", "armoryMain", function(ply)
	for k,v in pairs(_PArmory.Armories) do
		if IsValid(v) then
			if v:GetClass() != "armory" then return end
			
			if v:GetRobber() != nil then
				if v:GetRobber() == ply then
					hook.Run("OnArmoryEndRobbery", v, ply, 2)
					v:SetRobber(nil)
				end
			end
		end
	end
end)

hook.Add("PlayerDisconnected", "armoryMain", function(ply)
	for k,v in pairs(_PArmory.Armories) do
		if IsValid(v) then
			if v:GetClass() != "armory" then return end
			
			if v:GetRobber() != nil then
				if v:GetRobber() == ply then
					hook.Run("OnArmoryEndRobbery", v, ply, 3)
					v:SetRobber(nil)
				end
			end
		end
	end
end)

hook.Add("playerArrested", "armoryMain", function(ply)
	for k,v in pairs(_PArmory.Armories) do
		if IsValid(v) then
			if v:GetClass() != "armory" then return end
			
			if v:GetRobber() != nil then
				if v:GetRobber() == ply then
					hook.Run("OnArmoryEndRobbery", v, ply, 1)
					v:SetRobber(nil)
				end
			end
		end
	end
end)

hook.Add("canDropWeapon", "armory.canDropWeapon", function(ply, wep)
	if wep.nodrop then
		return false
	end
end)