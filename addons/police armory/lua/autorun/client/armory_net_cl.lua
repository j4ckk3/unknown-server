if SERVER then return end

local net = net
local draw = draw
local vgui = vgui
local pairs = pairs
local Color = Color
local Vector = Vector

net.Receive("_PArmoryCop", function()
	local _P = net.ReadTable()
	local armory = net.ReadEntity()
	if !IsValid(armory) then return end
	
	local cats = _P["cats"]
	local setupWeps = _P["weps"]
	
	local weps = armory:GetWeps()
	local count = table.Count(setupWeps)
	
	local borderColor = _P["clr"]
	local buttonColor = Color(0, 0, 0, 100)
	
	if count <= 0 then
		chat.AddText(Color(255, 255, 255, 255), "No weapons set in the armory!")
		return
	end
	
	local W, H = ScrW(), ScrH()
	
	local m = vgui.Create("DFrame")
	m:SetSize(W / 2, H / 2)
	m:SetTitle("")
	m:SetDraggable(false)
	m:ShowCloseButton(false)
	m:Center()
	m:MakePopup()
	
	m.Paint = function(self, w, h)
		draw.RoundedBox(2, 0, 0, w, h, borderColor)
		
		draw.RoundedBox(0, 10, 52, w - 20, h - 62, Color(0, 0, 0, 200))
		
		draw.RoundedBox(0, 0, 0, w, 25, Color(0, 0, 0, 200))
		
		draw.RoundedBox(0, 5, 30, w - 10, h - 35, Color(0, 0, 0, 200))
		
		draw.SimpleText("Police Armory", "Trebuchet24", w / 2, 12, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	
	local c = vgui.Create("DButton", m)
	c:SetSize(50, 15)
	c:SetPos(m:GetWide() - c:GetWide() - 5, 5)
	c:SetText("")
	
	c.Paint = function(self, w, h)
		if c:IsHovered() then
			draw.RoundedBox(3, 0, 0, w, h, Color(255, 150, 150, 255))
		else
			draw.RoundedBox(3, 0, 0, w, h, Color(255, 0, 0, 255))
		end
		
		draw.SimpleText("x", "Trebuchet24", w / 2, h / 2 - 2, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	
	c.DoClick = function()
		surface.PlaySound("buttons/button14.wav")
		m:Remove()
	end
	
	local s = vgui.Create("DPropertySheet", m)
	s:SetSize(m:GetWide() - 14, m:GetTall() - 39)
	s:SetPos(7, 32)
	s:SetText("")
	
	s.Paint = function(self, w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(55, 55, 55, 100))
	end
	
	local items = {}
	
	for k,v in pairs(cats) do
		local p = vgui.Create("DPanel", s)
		
		p.Paint = function( self, w, h ) 
			draw.RoundedBox(1, 0, 0, w, h, Color(0, 0, 0, 150)) 
		end
		
		local sb = vgui.Create("DScrollPanel", p)
		sb:SetSize(s:GetWide() - 16, s:GetTall() - 36)
		sb.Paint = function(self, w, h) end
		
		local sc = sb:GetVBar()
		
		sc.Paint = function( self, w, h )
			draw.RoundedBox(1, 0, 0, w, h, Color(0, 0, 0, 150)) 
		end
		
		sc.btnUp.Paint = function( self, w, h ) 
			draw.RoundedBox(1, 0, 0, w, h, Color(0, 0, 0, 150)) 
		end
		
		sc.btnDown.Paint = function( self, w, h )
			draw.RoundedBox(1, 0, 0, w, h, Color(0, 0, 0, 150)) 
		end
		
		sc.btnGrip.Paint = function( self, w, h ) 
			draw.RoundedBox(1, 0, 0, w, h, Color(0, 0, 0, 150)) 
		end
		
		items[v] = {}
		
		for id, item in pairs(setupWeps) do
			if item["category"] == v then
				table.insert(items[v], item)
			end
		end
		
		for ide, itemm in pairs(items[v]) do
			local x = 0
			local y = (ide * sb:GetTall() / 2) - (sb:GetTall() / 2)
			
			local b = vgui.Create("DButton", sb)
			b:SetSize(sb:GetWide() - 4, sb:GetTall() / 2)
			b:SetPos(2.5, y)
			b:SetText("")
			
			b.Paint = function(self, w, h)
			end
			
			local i = vgui.Create("DModelPanel", b)
			i:SetText("")
			i:SetPos(2.5, 2.5)
			i:SetSize(b:GetWide() - 5, b:GetTall() - 5)
			i.Paint = function( self, w, h )
				draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 75))
			end
			
			local model = "models/error.mdl"
			
			if weapons.GetStored(itemm["class"]) then
				model = weapons.GetStored(itemm["class"])["WorldModel"]
			end
			
			local w = vgui.Create("DModelPanel", i)
			w:SetSize(i:GetWide(), i:GetTall())
			w:SetModel(model)
			w:SetCamPos(Vector( 10, 15, 5 ))
			w:SetLookAt(Vector( 0, 0, 0 ))
			
			w.LayoutEntity = function(ENT)
				ENT.Entity:SetAngles(Angle(0, -30, 0) + itemm["angoffset"])
				ENT.Entity:SetPos(Vector(-10, -20, -10) + itemm["offset"])
			end
			
			w.DoClick = function()
				net.Start("_PArmoryWeapon")
					net.WriteEntity(armory)
					net.WriteString(itemm["class"], 8)
				net.SendToServer()
				
				m:Remove()
			end
		end
		
		s:AddSheet(" " .. v .. " ", p)
	end
	
	for k,v in pairs(s.Items) do
		v.Tab:SetTextColor(Color(255, 255, 255, 255))
		
		v.Tab.Paint = function(self, w, h)
			if !v.Tab:IsHovered() then
				if !v.Tab:IsActive() then
					draw.RoundedBox(0, 5, 0, w - 10, h, Color(0, 0, 0, 155))
				else
					draw.RoundedBox(0, 5, 0, w - 10, h - 8, Color(155, 155, 155, 155))
				end
			else
				draw.RoundedBox(0, 5, 0, w - 10, h, Color(155, 155, 155, 150))
			end
		end
	end
end)