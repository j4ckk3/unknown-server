print("--------------------------------------")     
print("Loading 2155X's Police Armory\n")

if SERVER then
	print("Loading: armory_config.lua")
	include("armory_config.lua")
	
	print("Loading: armory_functions_sv.lua")
	include("server/armory_functions_sv.lua")
	
	timer.Simple(0.1, function()
		
		print("Loading: armory_categories.lua")
		include("armory_categories.lua")
		
		print("Loading: armory_weapons.lua")
		include("armory_weapons.lua")
		
	end)
	
	print("Loading: armory_cmd_sv.lua")
	include("server/armory_cmd_sv.lua")
	
	print("Loading: armory_hooks_sv.lua")
	include("server/armory_hooks_sv.lua")
	
	print("Loading: armory_net_sv.lua")
	include("server/armory_net_sv.lua")
else
	AddCSLuaFile("client/armory_net_cl.lua")
	
	print("Loading: armory_net_cl.lua")
	include("client/armory_net_cl.lua")
end

print("\nFinished loading 2155X's Police Armory")
print("--------------------------------------")