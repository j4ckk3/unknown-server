_PArmory = _PArmory or {}
_PArmory.Armories = _PArmory.Armories or {}

/* Configuration */
_PArmory.MaxMoney = 20000 			/* Maximum money that the armory can hold.  DO NOT USE ANYTHING OTHER THAN THOUSANDS (500, 20, 1) */
_PArmory.MaxShipments = 5 			/* Maximum shipments that the armory can hold. */ 
_PArmory.MaxWeapons = 20 			/* Maximum weapons that the armory can hold. */ 

_PArmory.ArmorGive = 80 			/* How much armor to give cops once they open the armory */
_PArmory.WeaponTimer = 20 			/* How long to wait before taking another weapon from the armory */
_PArmory.RecoveryTimer = 3*(60*10) 	/* How long to wait before robbing the armory again (after a success) */

_PArmory.RobberySpeed = 0.15 		/* Higher values indicate faster robbery */
_PArmory.RobberySound = true 		/* Should the robbery sound play (small explosions) */

_PArmory.PlayersCops = 3 			/* Cops needed to start robbing the armory. */ 
_PArmory.Players = 6 				/* Players needed to start robbing the armory. */ 

_PArmory.AllowDropping = false 		/* Should players be allowed to drop the weapon from the armory. */ 
  
_PArmory.BorderColor = Color(150, 150, 150, 150)

hook.Add("loadCustomDarkRPItems", "loadArmoryStuff", function()
-------------------------------------------------------------
-- Example: _PArmory.Cops = {TEAM_COP, TEAM_CP, TEAM_SWAT} --
-- Example: _PArmory.Robbers = {TEAM_THIEF, TEAM_GANGSTER} --
-------------------------------------------------------------

/* Which TEAMS count as cops */
_PArmory.Cops = {TEAM_MAYOR, TEAM_POLICE, TEAM_CHIEF, TEAM_SWAT}

/* Which TEAMS count as robbers */
_PArmory.Robbers = {TEAM_EGANG, TEAM_EMOB, TEAM_WGANG, TEAM_WMOB, TEAM_PROMARAUDER, TEAM_MARAUDER}
end)

-------------------------------------------------------------
-- To add new categories:
-- Edit armory_categories.lua

-- To add new weapons:
-- Edit armory_weapons.lua
-------------------------------------------------------------

-------------------------------------------------------------
-- For developers:

-- SV Hooks:
---- OnArmoryStartRobbery -> self, robber
---- OnArmoryEndRobbery -> self, robber, reason
-- Valid end reasons: 1 = arrested, 2 = death, 3 = disconnected, 4 = ran away, 5 = success
-------------------------------------------------------------


