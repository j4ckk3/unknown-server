include('shared.lua')

local draw = draw
local surface = surface
local Color = Color

surface.CreateFont( "armoryFont1", {
	font = "Coolvetica",
	extended = false,
	size = 128,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )

surface.CreateFont( "armoryFont2", {
	font = "Coolvetica",
	extended = false,
	size = 100,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )

surface.CreateFont( "armoryFont3", {
	font = "Coolvetica",
	extended = false,
	size = 75,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )

function ENT:Draw()
	self:DrawModel()
	
	if LocalPlayer():GetPos():Distance(self:GetPos()) > 500 then
		return
	end
	
	local pos = self:GetPos()
	local ang = self:GetAngles()

	ang:RotateAroundAxis(ang:Up(), 90)
	ang:RotateAroundAxis(ang:Forward(), 90)
	
	local progress = 0
	local ships = self:GetShipments()
	local weps = self:GetWeps()
	local aships = ""
	local aweps = ""
	
	if ships == 0 then aships = "No" else aships = ships end
	if weps == 0 then aweps = "No" else aweps = weps end
	
	progress = math.Remap( self:GetProgress(), 0, 100, 0, 520 )
	
	local maxweps, maxships, maxmoney, sw, ss, sm = self:GetMaxWeapons(), self:GetMaxShipments(), self:GetMaxMoney(), 0, 0, 0
	
	sw = math.Remap( self:GetWeps(), maxweps, 0, 880 - 36, 0 )
	ss = math.Remap( self:GetShipments(), maxships, 0, 880 - 36, 0 )
	sm = math.Remap( self:GetMoney(), maxmoney, 0, 880 - 36, 0 )
	
	cam.Start3D2D(pos + ang:Up() * 8.415 + ang:Right() * -5.95 + ang:Forward() * 0.9, ang, 0.05)
		draw.RoundedBox(1, -475, -555, 950 - 36, 1172 + 147, Color(155, 155, 155, 225))
		draw.RoundedBox(1, -465, -545, 930 - 36, 175, Color(0, 0, 0, 200))
		draw.RoundedBox(1, -465, -360, 930 - 36, 970 + 147, Color(0, 0, 0, 200))
		
		draw.SimpleText("|< Police Armory >|", "armoryFont1", -18, -450, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		draw.SimpleText("| Armory Contents |", "armoryFont2", -18, -300, Color(225, 225, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		
		draw.RoundedBox(1, -445, -225, 890 - 36, 75, Color(0, 0, 0, 200))
		draw.RoundedBox(1, -445, -225 + 80, 890 - 36, 75, Color(0, 0, 0, 200))
		draw.RoundedBox(1, -445, -225 + 160, 890 - 36, 75, Color(0, 0, 0, 200))
		
		draw.RoundedBox(1, -440, -220, sm, 65, Color(175, 0, 0, 200))
		draw.RoundedBox(1, -440, -220 + 80, sw, 65, Color(175, 0, 0, 200))
		draw.RoundedBox(1, -440, -220 + 160, ss, 65, Color(175, 0, 0, 200))
		
		draw.SimpleText("$" .. self:GetMoney() .. " / $" .. maxmoney, "armoryFont3", -18, -185, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		draw.SimpleText(weps .. " / " .. maxweps .. " Weapons", "armoryFont3", -18, -105, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		draw.SimpleText(ships .. " / " .. maxships .. " Shipments", "armoryFont3", -18, -25, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		
		draw.RoundedBox(1, -445, 30 + 75, 890 - 36, 560, Color(0, 0, 0, 200))
		
		draw.RoundedBox(1, -265, 45 + 75, 530 - 36, 530, Color(0, 0, 0, 200))
		
		local id = surface.GetTextureID("vgui/entities/weapon_ak472.vtf")
		surface.SetDrawColor( 255, 255, 255 )
		surface.SetTexture( id )
		surface.DrawTexturedRect( -260, 50 + 75, 520 - 36, 520 )
		
		draw.RoundedBox(1, -425, 50 + 75, 145, progress, Color(175, 0, 0, 200))
		draw.RoundedBox(1, 280 - 35, 50 + 75, 145, progress, Color(175, 0, 0, 200))
	cam.End3D2D()
end