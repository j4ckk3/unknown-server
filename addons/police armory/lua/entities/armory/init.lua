AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
 
include("shared.lua")

local IsValid = IsValid
local net = net
local table = table
local team = team
local pairs = pairs
local player = player
local timer = timer
local tostring = tostring

function ENT:Refill()
	if !IsValid(self) then return end
	
	if self:GetIsBeingRobbed() == true then return end
	
	local arm = _PArmory
	
	local maxWeapons, maxShipments, maxMoney = arm.MaxWeapons, arm.MaxShipments, arm.MaxMoney
	
	local currWeapons, currShipments, currMoney = self:GetWeps(), self:GetShipments(), self:GetMoney()
	
	if currWeapons < maxWeapons then
		self:SetWeps( currWeapons + 1 )
	end
	
	if currShipments < maxShipments then
		self:SetShipments( currShipments + 1 )
	end
	
	if currMoney < maxMoney then
		self:SetMoney( currMoney + 1000 )
	end
end

function ENT:PoliceClicked(ply)
	if !IsValid(ply) then 
		return
	end
	
	if !ply:Alive() then 
		return 
	end
	
	local arm = _PArmory
	
	local armor = arm.ArmorGive
	
	ply:SetArmor(armor)
	
	net.Start("_PArmoryCop")
		net.WriteTable({["cats"] = arm.Categories, ["weps"] = arm.Weapons, ["clr"] = arm.BorderColor})
		net.WriteEntity(self)
	net.Send(ply)
end

function ENT:CanRob(ply)
	local arm = _PArmory
	
	local copTeams = arm.Cops
	local robberTeams = arm.Robbers
	local players = arm.Players
	local playersCops = arm.PlayersCops
	local players, cops = 0, 0
	
	if !IsValid(ply) then
		return false
	end
	
	if !ply:Alive() then 
		return false
	end
	
	if table.HasValue(copTeams, ply:Team()) then
		self:PoliceClicked(ply)
		return false
	end
	
	if !table.HasValue(robberTeams, ply:Team()) then
		DarkRP.notify(ply, 1, 5, "Police Armory.")
		return false
	end
	
	if self:GetShipments() <= 0 and self:GetMoney() <= 0 then
		DarkRP.notify(ply, 1, 5, "The Armory is empty!")
		return false
	end
	
	if self:GetIsBeingRobbed() == true then
		DarkRP.notify(ply, 1, 5, "The Armory is already being robbed!")
		return false
	end
	
	if self:GetIsBroken() == true then
		DarkRP.notify(ply, 1, 5, "The Armory is restocking after the last robbery!")
		return false
	end
	
	for k,v in pairs(player.GetAll()) do
		if IsValid(v) then
			if v:Alive() then
				if table.HasValue(copTeams, v:Team()) then
					cops = cops + 1
				end
			end
		end
	end
	
	if cops < playersCops then
		DarkRP.notify(ply, 1, 5, "Not enough Cops for the robbery!")
		return false
	end
	
	if player.GetCount() < players then
		DarkRP.notify(ply, 1, 5, "Not enough Players for the robbery!")
		return false
	end
	
	return true
end

function ENT:Initialize()
	self:SetModel("models/custom_models/sterling/2155x_locker.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
	
	local phys = self:GetPhysicsObject()
	
	if IsValid(phys) then
		phys:Wake()
		phys:EnableMotion(false)
	end
	
	local arm = _PArmory
	
	self:SetMoney(0)
	self:SetShipments(0)
	self:SetWeps(0)
	self:SetProgress(0)
	
	self:SetMaxMoney(arm.MaxMoney)
	self:SetMaxShipments(arm.MaxShipments)
	self:SetMaxWeapons(arm.MaxWeapons)
	
	self:SetRobber(nil)
	self:SetIsBeingRobbed(false)
	self:SetIsBroken(false)
	
	timer.Create("_armoryRefill_"..tostring(self:EntIndex()), 30, 0, function() 
		if IsValid(self) then 
			self:Refill() 
		end 
	end)
	
	table.insert(_PArmory.Armories, self)
end

function ENT:Use(activator, ply)
	if !IsValid(ply) then return end
	if !ply:Alive() then return end
	
	if ply:GetPos():Distance(self:GetPos()) > 200 then return end
	
	if self:CanRob(ply) == true then
		DarkRP.notify(ply, 1, 5, "Don't get arrested, killed, disconnect or run away from the armory!")
		DarkRP.notify(ply, 1, 5, "Stay close to the armory to progress!")
		DarkRP.notify(ply, 1, 5, "You have started the robbery!")
		
		self:SetRobber(ply)
		self:SetIsBeingRobbed(true)
		self:SetIsBroken(false)
		
		hook.Run("OnArmoryStartRobbery", self, ply)
	end
end

function ENT:Think()
	if self:GetIsBeingRobbed() == true then
		if self:GetProgress() >= 100 then
			self:SetProgress(0)
			hook.Run("OnArmoryEndRobbery", self, self:GetRobber(), 5)
			return
		end
		local arm = _PArmory
		
		local robberTeams = arm.Robbers
		local timeOffset = arm.RobberySpeed
		local shouldPlay = arm.RobberySound
		
		for k,v in pairs(player.GetAll()) do
			if v:GetPos():Distance(self:GetPos()) < 100 then
				if table.HasValue(robberTeams, v:Team()) then
					if shouldPlay == true then
						self:EmitSound("ambient/levels/labs/electric_explosion3.wav", 50, 100, 0.4)
					end
					self:SetProgress( self:GetProgress() + timeOffset)
				end
			end
			
			if v == self:GetRobber() then
				if v:GetPos():Distance(self:GetPos()) > 500 then
					self:SetProgress(0)
					hook.Run("OnArmoryEndRobbery", self, self:GetRobber(), 4)
					return
				end
			end
		end
		
	end
	
	self:NextThink(CurTime() + 0.5)
end

function ENT:OnRemove()
	timer.Remove("_armoryRefill_"..tostring(self:EntIndex()))
	if self:GetIsBeingRobbed() == true then
		hook.Run("OnArmoryEndRobbery", self, self:GetRobber(), 4)
	end
	
	self:SetRobber(nil)
	self:SetIsBeingRobbed(false)
	self:SetMoney(0)
	self:SetShipments(0)
	self:SetWeps(0)
	self:SetProgress(0)
	
	table.RemoveByValue(_PArmory.Armories, self)
end