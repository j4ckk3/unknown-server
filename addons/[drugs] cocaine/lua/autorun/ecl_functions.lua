if SERVER then
	resource.AddWorkshop("1090674274");

	function ECL:Log(text)
		local prefix = "[ECL]: ";

		local time = os.time()
		local filename = os.date("%d_%m_%Y.txt" , time)
		local date = os.date("[%H:%M:%S]" , time)

		file.Append( "jb/ecl_logs/"..filename, date..prefix..text.."\n" )

		if ECL.Logs.Type == 2 then
			MsgC(Color(255,255,0), prefix, Color(255,255,255), text.."\n")
		end;
	end

	if !ECL.StartedLogs then
		if !file.IsDir("jb", "DATA") then
			file.CreateDir("jb", "DATA");
		end;

		if !file.IsDir("jb/ecl_logs", "DATA") then
			file.CreateDir("jb/ecl_logs", "DATA");
		end;

		local time = os.time()
		local date = os.date("%d_%m_%Y" , time)

		if !file.Exists("jb/ecl_logs/"..date..".txt", "DATA") then
			file.Write("jb/ecl_logs/"..date..".txt", "", "DATA");
		end;

		file.Append( "jb/ecl_logs/"..date..".txt", "\n" )
		ECL:Log("Starting logging.")
		ECL.StartedLogs = true;
	end;

	ECL.PotThink = true;

	function ECL:PotThink(ent)
		local cleaned = ent:GetNWInt("cleaned");
		local ingnited = ent:GetNWBool("ingnited");
		local maxAmount = ent:GetNWInt("max_amount");
		if ent.nextTick < CurTime() then
			local temp = ent:GetNWInt("temperature")

			if ingnited and cleaned == maxAmount then
				ent:SetNWInt("temperature", temp+math.random(1,2))
				ent:PlaySound();
			end;
			ent.nextTick = CurTime() + 1;
			ent:SetNWBool("ingnited", false)
			if !ingnited then
				ent:StopPlay();
			end

			if !ingnited and temp > ECL.Pot.Temperature then
				ent:SetNWInt("temperature", temp-math.random(0,1));
			end;

			if temp > ECL.Pot.ExplodeTemperature then
				ent:StopPlay();
				ent:Explode();
			end;
		end;
	end;

	ECL.StoveThink = true;

	function ECL:StoveThink(ent)
	    local Ang = ent:GetAngles();
		Ang:RotateAroundAxis(Ang:Forward(), 90);

	    local poses;
		if (ECL.CustomModels) and (ECL.CustomModels.Stove) then
		    poses = {[1] = ent:GetPos()+Ang:Right()*-3.8+Ang:Forward()*-0.255+Ang:Up()*1.82};
	    else
	        poses = {
		    	[1] = ent:GetPos()+Ang:Right()*-19.8+Ang:Forward()*-9.75+Ang:Up()*11.5,
		    	[3] = ent:GetPos()+Ang:Right()*-19.8+Ang:Forward()*2.75+Ang:Up()*11.5,
		    	[2] = ent:GetPos()+Ang:Right()*-19.8+Ang:Forward()*-9.75+Ang:Up()*-11.2,
		    	[4] = ent:GetPos()+Ang:Right()*-19.8+Ang:Forward()*2.75+Ang:Up()*-11.2
	    	};
	    end;

		local plates = {
			[1] = ent:GetNWBool("left-top"),
			[2] = ent:GetNWBool("right-top"),
			[3] = ent:GetNWBool("left-bottom"),
			[4] = ent:GetNWBool("right-bottom")
		};
		local unabled = 0
		for k, v in pairs(plates) do
			if v then
				unabled = unabled + 1;
				local pos = poses[k];
				local entities = ents.FindInSphere(pos,2);
				for k2, ent in pairs(entities) do
					local class = ent:GetClass();

					if class == "ecl_pot" then
						ent:SetNWBool("ingnited", true);
					end
				end
			end
		end

		if unabled > 0 then
			local gas = ent:GetNWInt("gas");
			if gas > 0 then
				ent:SetNWInt("gas", math.Round(gas - 1.5*unabled));
			else
				ent:SetNWBool("left-top", false);
				ent:SetNWBool("left-bottom", false);
				ent:SetNWBool("right-top", false);
				ent:SetNWBool("right-bottom", false);
				if (ECL.CustomModels) and (ECL.CustomModels.Stove) then
				    ent:SetBodygroup(2, 1);
				end;
			end;
		end;
	end;

    timer.Simple(10, function()
		local filepathes = {
				"models/srcocainelab/portablestove.mdl",
				"models/srcocainelab/gascan.mdl",
				"models/srcocainelab/cocainebrick.mdl",
				"models/srcocainelab/cocaplant.mdl",
				"models/srcocainelab/cocaplant_nopot.mdl"
		}

		for k, v in pairs(filepathes) do
			if !util.IsValidModel(v) then
				if id then
					ECL:Log("Warning! Model '"..v.." isn't loaded.")
				end;
			end;
		end;
	end);
end;

function ECL:SetLanguage(name)
	if SERVER then
		ECL:Log("'ecl_config.lua' has been successfully loaded.")
		ECL:Log("'ecl_functions.lua' has been successfully loaded.")
	end

	local lang = self:GetLanguage(name);

	if lang then
		if CLIENT then
			ECL.Language = lang;
		end

		if SERVER then
			ECL:Log("Language '"..name.."' has been successfully set.")
		end
	end
end

function ECL:CreateLanguage(name, tbl)
	ECL.Languages[name] = tbl;
end

function ECL:GetLanguage(name)
	AddCSLuaFile("languages/ecl_language_"..name..".lua")
	include("languages/ecl_language_"..name..".lua")

	local lang = ECL.Languages[name]
	if lang then
		return lang;
	else
		ECL:Log("Cannot find '"..name.."' language.")
	end;
end
