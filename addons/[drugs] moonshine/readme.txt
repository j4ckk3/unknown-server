The Moonshining

You drop the extracted folder in to your addons folder, then you open lua/moonshine/cf/config/config.lua.
Edit what you need in the config, after save and restart your server.

Console commands:
-- When you have spawned the npc/stand, placed how you want it, then run this command. (after restart server)
dist_save
-- If you want to place your npc's different ways, then run this command, it delete the current spawn positions.
dist_delete
-- If you want to spawn the barrel buyer npc.
dist_spawn_npc
-- If you want to spawn the stand that purchase moonshine bottles.
dist_spawn_stand

Api functions:

Serverside:
dist_msg(ply, "Place your msg here.")
dist_effect(ply, 50) // 1/100 how drunk the person will become.
dist_sound(value, pos) // value is the sound string format, and the vector position where to play it.
dist_math(value) // place a number in the math, if you place 50 it will be 1/50 chance.
dist_pass(self, degree, ran, grain, corn, water) // checks if the dist got current value and returns true/false

Shared:
dist_3d2d(ply, self, vector1, vector2) // if you want to create a new 3d2d button, require you run the function clientside too.


Why is my content purple/error?
You need to force download on the materials/models, for people to see them.
You do it by adding the content to your workshop collection, look it up on google if you have some issues.

Workshop content link:
http://steamcommunity.com/sharedfiles/filedetails/?id=1086143799

