if SERVER then
	util.AddNetworkString("dist_chat_api")
	util.AddNetworkString("dist_effect_api")
end

function dist_msg(ply, value)
	net.Start("dist_chat_api")
		net.WriteString(value)
	net.Send(ply)
end

function dist_effect(ply, value)
	net.Start("dist_effect_api")
		net.WriteInt(value, 16)
	net.Send(ply)
end

function dist_sound(value, pos)
	sound.Play( value, pos )
end

function dist_math(value)
	return math.random(1, value)
end

function dist_pass(self, degree, ran, grain, corn, water)
	return self:Getdist_degree() >= degree
	&& self:Getdist_ran() >= ran
	&& self:Getdist_grain() >= grain
	&& self:Getdist_corn() >= corn
	&& self:Getdist_water() >= water
end