AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize( )
	self:SetModel( dist.ahshop.NpcModel )
	self:SetHullType(HULL_HUMAN)
	self:SetHullSizeNormal()
	self:SetNPCState(NPC_STATE_SCRIPT)
	self:SetSolid( SOLID_BBOX)
	self:CapabilitiesAdd(CAP_ANIMATEDFACE)
	self:CapabilitiesAdd(CAP_TURN_HEAD)
	self:DropToFloor()
	self:SetMaxYawSpeed(90)
	self:SetCollisionGroup( 1 )
end

function ENT:AcceptInput( key, ply )
	local EI = self:EntIndex()
	if ( ( self.lastUsed or CurTime() ) <= CurTime() ) and ( key == "Use" && ply:IsPlayer() && IsValid( ply ) ) then
		self.lastUsed = CurTime() + 0.25
		for k,v in pairs(ents.FindByClass("mn_barrel")) do 
			if ( v.Spawned ) then
				if self:GetPos():Distance(v:GetPos()) <= dist.ahshop.NpcPurchaseDistance then 	
					local val = 1
					if ( v:Getdist_alcohol() >= dist.ahshop.NpcPurchaseBonusAlcoholAmount ) then
						local val = dist.ahshop.NpcPurchaseBonus
					end
					local mon = (dist.ahshop.NpcBarrelPrice * val)
					ply:addMoney( mon )
					dist_msg(ply, "You just sold your moonshine barrel for: $"..mon)
					v:Remove()	
				end
			else
				dist_msg(ply, "This barrel need to be cooled down in the oxidation pool.")	
			end
		end	
	end	
end


