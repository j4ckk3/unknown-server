ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.PrintName = "Bottle"
ENT.Author = "Mikael"
ENT.Category = "moonshine"
ENT.Spawnable = true
ENT.AdminSpawnable = false

function ENT:SetupDataTables()
	self:NetworkVar("Bool", 0, "dist_full")
	self:NetworkVar("Bool", 1, "dist_bought")
	self:NetworkVar("Int", 0, "dist_moonshine")
	self:NetworkVar("Int", 1, "dist_alcohol")
end
