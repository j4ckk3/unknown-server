AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/wood_barrel/wood_barrel.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:GetPhysicsObject():Wake()
	timer.Simple(550, function() if self:IsValid() then self:Remove() end end )
	self:SetCustomCollisionCheck( true )
	self.dist_count = 0
end

function ENT:StartTouch( ent )
	if (ent:GetClass() == "mn_bottle") && (self.Spawned) then 
		if ( !ent:Getdist_full() ) && !( self.dist_count >= 10 ) then
			self.dist_count = ((self.dist_count or 0) + 1)
			if ( self.dist_count >= 10 ) then
				self.dist_count = 0
				self.Spawned = false
				self:Setdist_full(false)
				self:Setdist_alcohol(0)
				self:Setdist_moonshine(0)
				self:SetColor(Color(255,255,255))
			end
			ent:Setdist_full(true)
			ent:SetColor(Color(0,255,0))
			ent:Setdist_alcohol(self:Getdist_alcohol())
		end
	end
end


