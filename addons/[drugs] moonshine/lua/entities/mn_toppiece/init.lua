AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/big_pipe/big_pipe.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:GetPhysicsObject():Wake()
	timer.Simple(550, function() if self:IsValid() then self:Remove() end end )
	self:SetCustomCollisionCheck( true )
end

function ENT:OnRemove()
end

