AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/pot/pot_nodirt.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:GetPhysicsObject():Wake()
end

function ENT:Use( ply )
	if ( ( self.lastUsed or CurTime() ) <= CurTime() ) then
		self.lastUsed = CurTime() + 0.25
		if ( self:Getdist_harvest() ) then
			self.plant:Remove()
			self:Setdist_harvest(false)
			dist_msg(ply, "You succesfully harvested the crop.")
			if ( self:Getdist_crop() == 1 ) then
				local dist_corn = ents.Create( "mn_ccorn" )
				if ( !IsValid( dist_corn ) ) then return end 
				dist_corn:SetPos( self:GetPos() + self:GetUp() * 12 )
				dist_corn:SetPos( self.plant:GetPos() + self.plant:GetUp() * 8 )
				dist_corn:Spawn()
				self:Setdist_crop(0)	
			elseif ( self:Getdist_crop() == 2 ) then
				local dist_grain = ents.Create( "mn_ggrain" )
				if ( !IsValid( dist_grain ) ) then return end 
				dist_grain:SetPos( self.plant:GetPos() + self.plant:GetUp() * 8 )
				dist_grain:Spawn()
				self:Setdist_crop(0)				
			end
		end
	end
end

function ENT:StartTouch( ent )
	if (ent:GetClass() == "mn_dirt") then 
		if ( !self.dist_dirt ) then
			ent:Remove()
			self.dist_dirt = true
			self:SetModel("models/pot/pot_dirt.mdl")
		end
	elseif (ent:GetClass() == "mn_gseed") then
		if !IsValid( self.plant ) && ( self.dist_dirt ) then
			self.plant = ents.Create( "prop_dynamic" )
			if ( !IsValid( self.plant ) ) then return end
			self.plant:SetModel( "models/plants/wheatplant.mdl" )
			self.plant:PhysicsInit( SOLID_VPHYSICS )
			self.plant:SetParent( self )
			self.plant:SetPos( Vector(0, 0, 9) )
			self.plant:SetAngles( self:GetAngles() )
			self.plant:SetModelScale(self.plant:GetModelScale() * 0.10, 0)
			self.plant:Spawn()
			self:GrowTimer(dist.ahshop.GrainGrowTime)
			self:Setdist_crop(2)
			ent:Remove()
		end
	elseif (ent:GetClass() == "mn_cseed") then
		if !IsValid( self.plant ) && ( self.dist_dirt ) then
			self.plant = ents.Create( "prop_dynamic" )
			if ( !IsValid( self.plant ) ) then return end
			self.plant:SetModel( "models/plants/cornplant.mdl" )
			self.plant:PhysicsInit( SOLID_VPHYSICS )
			self.plant:SetParent( self )
			self.plant:SetPos( Vector(0, 0, 9) )
			self.plant:SetAngles( self:GetAngles() )
			self.plant:SetModelScale(self.plant:GetModelScale() * 0.10, 0)
			self.plant:Spawn()
			self:GrowTimer(dist.ahshop.CornGrowTime)
			self:Setdist_crop(1)
			ent:Remove()
		end
	end
end

function ENT:GrowTimer(second)
	self.timeran = 0
	timer.Create( "growtimer"..self:EntIndex(), 6, 0, function()
		if self.timeran == second then
			self:Setdist_harvest(true)
			timer.Remove( "growtimer"..self:EntIndex() )
			return
		end
		self.timeran = ((self.timeran || 0) + 1)
		self.plant:SetModelScale( self.plant:GetModelScale() * 1.10, 1 )
	end)
end

function ENT:OnRemove()
	timer.Remove( "growtimer"..self:EntIndex() )
end