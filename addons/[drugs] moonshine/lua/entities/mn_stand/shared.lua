ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.PrintName = "Sell Stand"
ENT.Author = "Mikael"
ENT.Category = "moonshine"
ENT.Spawnable = true
ENT.AdminSpawnable = false

function ENT:SetupDataTables()
	self:NetworkVar("Int", 0, "dist_count")
	self:NetworkVar("Int", 1, "dist_money")
end
