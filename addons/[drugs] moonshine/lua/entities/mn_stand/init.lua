AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/stand/stand.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:GetPhysicsObject():Wake()
	self.dist_amount = 0
	self:Setdist_count(0)
	self.dist_side = 0
end

function ENT:Use( ply )
	local index = self.dist_index
	local dist_beep = dist.ahshop.DistilleryButtonSound
	
	if ( ( self.lastUsed || CurTime() ) <= CurTime() ) then
	
		self.lastUsed = CurTime() + 0.25
		
		if dist_3d2d(ply, self, distl_.dist_pos5, distl_.dist_pos6) then
		
			if !( self:Getdist_count() <= 0 ) then
				ply:addMoney( -dist.ahshop.StandBottlePrice )
				self.dist_bottle[self:Getdist_count()]:Remove()
				self:Setdist_count(self:Getdist_count() - 1)
				local dist_moonshine = ents.Create( "mn_bottle" )
				if ( !IsValid( dist_moonshine ) ) then return end 
				dist_moonshine:SetPos( self:GetPos() + self:GetUp() * 47 + self:GetForward() * 30 )
				dist_moonshine:Setdist_bought(true)
				dist_moonshine:SetColor(Color(0,255,0))
				dist_moonshine:Spawn()
			end

		elseif dist_3d2d(ply, self, distl_.dist_pos7, distl_.dist_pos8) then

			if self:Getdist_money() <= 0 then
				dist_msg(ply, "You havent placed any moonshine bottles on the stand.")
			else
				ply:addMoney( self:Getdist_money() )
				dist_msg(ply, "You just sold your moonshine bottles for: " .. HyplexCurrency ..self:Getdist_money())
				self:Setdist_money(0)
			end		
		
		end
	end
end

function ENT:StartTouch( ent )	
	if (ent:GetClass() == "mn_bottle") && !(self:Getdist_count() > 12) && !ent:Getdist_bought() && ent:Getdist_full() then 
	
		self.dist_amount = ((self.dist_amount || 0) - 8)
		self:Setdist_count(self:Getdist_count() + 1)
		self:Setdist_money(self:Getdist_money() + dist.ahshop.StandBottleSellPrice)
			
		if (self:Getdist_count() == 6) then
			self.dist_side = ((self.dist_side || 0) - 8)
			self.dist_amount = 0
		end
		
		if !self.dist_bottle then
			self.dist_bottle = {}
		end
		
		self.dist_bottle[self:Getdist_count()] = ents.Create("prop_dynamic")
		if ( !IsValid( self.dist_bottle[self:Getdist_count()] ) ) then return end
		self.dist_bottle[self:Getdist_count()]:SetModel("models/props_junk/glassjug01.mdl")
		self.dist_bottle[self:Getdist_count()]:PhysicsInit(SOLID_VPHYSICS)
		self.dist_bottle[self:Getdist_count()]:SetParent(self)
		self.dist_bottle[self:Getdist_count()]:SetPos(Vector(14 + self.dist_amount, 8 + self.dist_side, 35.7))
		self.dist_bottle[self:Getdist_count()]:SetAngles(self:GetAngles())
		self.dist_bottle[self:Getdist_count()]:Spawn()
		ent:Remove()

	end
end