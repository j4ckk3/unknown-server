AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/wood_bucket/wood_bucket.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:GetPhysicsObject():Wake()
	self.dist_index = self:EntIndex()
end

function ENT:StartTouch( ent )
	if (ent:GetClass() == "mn_barrel") && (ent:Getdist_full()) then 
		if !( self:Getdist_barrel() >= 4 ) && ( !ent.Spawned ) && ( !timer.Exists( "coolwater"..self.dist_index ) )then
		
			if !self.barrel then
				self.barrel = {}
				self.barrel[1] = { alcohol = ent:Getdist_alcohol() }
			else
				table.insert( self.barrel, { alcohol = ent:Getdist_alcohol() } )
			end
			
			if ( !self.dist_barrel ) then
				self.dist_barrel = {}
			end
			
			if ( !self.dist_side ) then
				self.dist_side = 0
			end
			
			if ( self:Getdist_barrel() == 2 ) then
				self.dist_side = ( ( self.dist_side or 0  ) - 40 )
				self.dist_amount = 0
			end
			
			self:Setdist_barrel( self:Getdist_barrel() + 1 )	
			self.dist_amount = (( self.dist_amount or 0) - 35 )
			
			if ( self:Getdist_barrel() == 4 ) then
				timer.Create( "coolwater"..self.dist_index, dist.ahshop.OxidationSingleBarrelTime, 4, function() 
					local dist_cold = ents.Create( "mn_barrel" )
					if ( !IsValid( dist_cold ) ) then return end 
					dist_cold:SetPos( self.dist_barrel[self:Getdist_barrel()]:GetPos() )
					dist_cold:SetAngles( self.dist_barrel[self:Getdist_barrel()]:GetAngles() )
					dist_cold:Setdist_alcohol( self.barrel[self:Getdist_barrel()].alcohol )
					dist_cold:SetColor(Color(0,255,0))
					dist_cold.Spawned = true
					dist_cold:Spawn()
					self.dist_barrel[self:Getdist_barrel()]:Remove()
					self:Setdist_barrel( self:Getdist_barrel() - 1 )	
					self.dist_side = 0
					self.dist_amount = 0	
					table.remove( self.barrel, self:Getdist_barrel() ) 					
				end)
			end
			
			self.dist_barrel[self:Getdist_barrel()] = ents.Create( "prop_dynamic" )
			if ( !IsValid( self.dist_barrel[self:Getdist_barrel()] ) ) then return end
			self.dist_barrel[self:Getdist_barrel()]:SetModel( "models/wood_barrel/wood_barrel.mdl" )
			self.dist_barrel[self:Getdist_barrel()]:PhysicsInit( SOLID_VPHYSICS )
			self.dist_barrel[self:Getdist_barrel()]:SetParent( self )
			self.dist_barrel[self:Getdist_barrel()]:SetPos( Vector(54 + self.dist_amount, 20 + self.dist_side, 4) )
			self.dist_barrel[self:Getdist_barrel()]:SetAngles( self:GetAngles() )
			self.dist_barrel[self:Getdist_barrel()]:SetColor( Color(255,0,0) )
			self.dist_barrel[self:Getdist_barrel()]:Spawn()
			ent:Remove()
			
		end	
	end
end

function ENT:OnRemove()
	timer.Remove( "coolwater"..self.dist_index )
end 
