ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.PrintName = "Oxidation Pool"
ENT.Author = "Mikael"
ENT.Category = "moonshine"
ENT.Spawnable = true
ENT.AdminSpawnable = false

function ENT:SetupDataTables()
	self:NetworkVar("Bool", 0, "dist_full")
	self:NetworkVar("Int", 0, "dist_moonshine")
	self:NetworkVar("Int", 1, "dist_alcohol")
	self:NetworkVar("Int", 2, "dist_barrel")
end
