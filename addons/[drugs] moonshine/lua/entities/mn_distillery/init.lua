AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT:Initialize()
	self:SetModel( "models/bronze_barrel/bronze_barrel.mdl" )
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:GetPhysicsObject():Wake()
	self:GetPhysicsObject():SetMass(39)
	self:Setdist_health( dist.ahshop.DistilleryHealth )
	self.dist_index = self:EntIndex()
	self.SteamSound = CreateSound(self,dist.ahshop.DistilleryBoilSound)
end

function ENT:OnTakeDamage( dmg )
	self:Setdist_health( ( self:Getdist_health() || 100 ) - dmg:GetDamage() )
	if ( self:Getdist_health() <= 0 ) then                  
		self:Destruct()
        self:Remove()     
    end
end

function ENT:Use( ply )
	-- Start/stop cooking & cooling function.
	local index = self.dist_index
	local dist_beep = dist.ahshop.DistilleryButtonSound
	if ( ( self.lastUsed || CurTime() ) <= CurTime() ) then
	
		self.lastUsed = CurTime() + 0.25
		
		if !( IsValid( self.dist_barrel ) ) && !( IsValid( self.dist_top ) ) then return end
		
		if dist_3d2d(ply, self, distl_.dist_pos1, distl_.dist_pos2)  then
		
			if !(self.dist_run) then
			
				sound.Play( dist_beep, self:GetPos() )
				self.SteamSound:Play()
				self.dist_run = true
				self:Setdist_start(true)

				timer.Create("boiltimer"..index, 2, 0, function()

					self:Setdist_ran(self:Getdist_ran() + 20)
		
					if self:Getdist_moonshine() >= 100 then
						self.dist_run = false
						self.dist_vec = self.dist_barrel:GetPos()
						self.dist_barrel:Remove()
						timer.Remove("boiltimer"..index)
						local barrel = ents.Create( "mn_barrel" )
						if ( !IsValid( barrel ) ) then return end 
						barrel:SetPos( self.dist_vec )
						barrel:Setdist_alcohol( barrel:Getdist_alcohol() + dist_math( 100 ) )
						barrel:Setdist_moonshine( barrel:Getdist_moonshine() + self:Getdist_moonshine() )
						barrel:Setdist_full( true )	
						barrel:SetColor( Color(255,0,0) )
						barrel:Spawn()
						self:Setdist_moonshine( 0 )	
						return
					end
		
					if self:Getdist_degree() >= 200 && !timer.Exists( "overheat"..index ) then
						timer.Create( "overheat"..index, dist.ahshop.DistilleryOverheatTime, 1, function() 
							self:Destruct()
							self:Remove() 
						end)
					else
						self:Setdist_degree( self:Getdist_degree() + 4 )	
					end

					if dist_pass( self, 99, 199, 0, 0, 0 ) then 
						self:Setdist_moonshine( self:Getdist_moonshine() + dist_math(dist.ahshop.DistilleryMoonshineAmount) )
						self:Setdist_degree( self:Getdist_degree() + dist_math(dist.ahshop.DistilleryHeatAmount) )
						self:Setdist_grain( self:Getdist_grain() - dist_math(dist.ahshop.DistilleryGrainUsage) )			
						self:Setdist_corn( self:Getdist_corn() - dist_math(dist.ahshop.DistilleryCornUsage) )
						self:Setdist_water( self:Getdist_water() - dist_math(dist.ahshop.DistilleryWaterUsage) )	
					end
				end)
			
			else
			
				self.dist_run = false
				self.SteamSound:Stop()
				self:Setdist_start( false )
				sound.Play( dist_beep, self:GetPos() )
				timer.Remove( "boiltimer"..index )
				
				timer.Create( "rantimer"..index, 4, 0, function() 
					if ( self:Getdist_ran()) && (self:Getdist_degree() ) then
						timer.Remove( "rantimer"..index )	
					end
					if !( self:Getdist_ran() <= 0 ) then
						self:Setdist_ran(self:Getdist_ran() - 20)
					end
					if !( self:Getdist_degree() <= 0 ) then
						self:Setdist_degree(self:Getdist_degree() - dist.ahshop.DistilleryUnHeatAmount)
					end
				end)	
			end
			
		elseif dist_3d2d( ply, self, distl_.dist_pos3, distl_.dist_pos4 ) && !( self:Getdist_degree() <= 0 ) then
		
			self:Setdist_cooling( true )
			sound.Play( dist_beep, self:GetPos() )	
			
			timer.Create( "cooldown2"..index, 1, 1, function() 
				self:Setdist_cooling( false )
			end)
			
			timer.Create( "cooldown"..index, 0.1, dist.ahshop.DistilleryCoolRunTimes, function() 
				if self:Getdist_degree() <= 0 then
					timer.Remove( "cooldown"..index )		
					return
				end
				if timer.Exists( "overheat"..index ) then
					timer.Remove( "overheat"..index )
				end
			self:Setdist_degree( self:Getdist_degree() - dist_math(dist.ahshop.DistilleryHeatCoolingAmount) )
			end)
		end
	end
end

function ENT:StartTouch( ent )
	if ( ent:GetClass() == "mn_grain" ) then 
	
		if !( self:Getdist_grain() >= 61 ) then
			self:Setdist_grain( self:Getdist_grain() + dist.ahshop.DistilleryGrainAmount )
			ent:Remove()
		end
		
	elseif ( ent:GetClass() == "mn_corn" ) then 
	
		if !( self:Getdist_corn() >= 61 ) then
			self:Setdist_corn( self:Getdist_corn() + dist.ahshop.DistilleryCornAmount )	
			ent:Remove()
		end
		
	elseif ( ent:GetClass() == "mn_water" ) then 
	
		if !( self:Getdist_water() >= 61 ) then	
			self:Setdist_water( self:Getdist_water() + dist.ahshop.DistilleryWaterAmount )	
			ent:Remove()
		end
		
	elseif ( ent:GetClass() == "mn_ggrain" ) then 
	
		if !( self:Getdist_grain() >= 61 ) then
			self:Setdist_grain( self:Getdist_grain() + dist.ahshop.DistilleryGrainAmount )
			ent:Remove()
		end
		
	elseif ( ent:GetClass() == "mn_ccorn" ) then 
	
		if !( self:Getdist_corn() >= 61 ) then
			self:Setdist_corn( self:Getdist_corn() + dist.ahshop.DistilleryCornAmount )	
			ent:Remove()
		end

	elseif ( ent:GetClass() == "mn_toppiece" ) then 
	
		if ( !IsValid( self.dist_top ) ) then
			self.dist_top = ents.Create( "prop_dynamic" )
			if ( !IsValid( self.dist_top ) ) then return end
			self.dist_top:SetModel( "models/big_pipe/big_pipe.mdl" )
			self.dist_top:PhysicsInit( SOLID_VPHYSICS )
			self.dist_top:SetParent( self )
			self.dist_top:SetPos( Vector(31, -3, 42) )
			self.dist_top:SetAngles( self:GetAngles() )
			self.dist_top:Spawn()
			ent:Remove()
		end
		
	elseif ( ent:GetClass() == "mn_barrel" ) then 
	
		if ( !IsValid( self.dist_barrel ) ) && ( !ent:Getdist_full() ) && ( !ent.Spawned  ) then	
			self.dist_barrel = ents.Create( "prop_dynamic" )
			if ( !IsValid( self.dist_barrel ) ) then return end
			self.dist_barrel:SetModel( "models/wood_barrel/wood_barrel.mdl" )
			self.dist_barrel:PhysicsInit( SOLID_VPHYSICS )
			self.dist_barrel:SetParent( self )
			self.dist_barrel:SetPos( Vector(66, -5, -12) )
			self.dist_barrel:SetAngles( self:GetAngles() )
			self.dist_barrel:Spawn()
			ent:Remove()
		end
	end
end

function ENT:Destruct()
    local vPoint = self:GetPos()
    local effectdata = EffectData()
    effectdata:SetStart(vPoint)
    effectdata:SetOrigin(vPoint)
    effectdata:SetScale(1)
    util.Effect("Explosion", effectdata) 
end

function ENT:OnRemove()
	timer.Remove( "fillbucket"..self.dist_index )
	timer.Remove( "boiltimer"..self.dist_index )
	timer.Remove( "overheat"..self.dist_index )
	timer.Remove( "cooldown2"..self.dist_index )
	timer.Remove( "cooldown"..self.dist_index )
	if self.SteamSound then self.SteamSound:Stop() end
end 