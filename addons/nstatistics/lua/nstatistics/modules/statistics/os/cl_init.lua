NSTATISTICS.AddStatistic({
	Title = "OperatingSystem",
	Name = "system",
	Beautifier = nil,
	RawDataModifier = nil,
	Modifier = nil,
	ForPlayers = true,
	Display = "%.2f%%",
	Legend = nil,
	ShowKey = true,
	MinChartY = 100,
	ModifyFilterRawData = nil,
})

//2da1bfd3a41fca3accf4355554e449ff16e8e33cb37e0168c9195dfe66ad041a
NSTATISTICS.AddInitialSpawnCallback(function()
	local cases = {
		"Windows",
		"Linux",
		"OSX",
		"BSD",
		"POSIX",
		"Other"
	}
	
	local system = jit.os
	
	if system and !table.HasValue(cases, system) then
		system = "Other"
	end
	
	NSTATISTICS.SendStatisticToServer("system", { system })
end)