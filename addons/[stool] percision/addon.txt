// Extracted with MulleDK19's GMAD Extractor
"AddonInfo"
{
	"name" "Precision Tool"
	"author_name" "Author Name"
	"info" " { 	"description": "garrysmod addon to allow exact movement of objects for accurate building. Push/pull, rotate, and snap props together at specific distances.", 	"type": "tool", 	"tags":  	[ 		"build", 		"fun" 	] }"
}